var initAjaxDatatables = function (datas) {

    //init date pickers
    /*$('.date-picker').datepicker({
        rtl: App.isRTL(),
        autoclose: true
    });*/

    var grid = new Datatable();

    var column = datas.column;
    var columns = [];
    var hideColumns = [];
    for(var i in column){
        var splitColumn = column[i].split(":");

        if(splitColumn[0] == "btn"){
            tmp = {
                title: '', 
                "sClass": "v-middle",
                render: function (data, type, row) {
                    var renderButton = "";
                    if(datas.fancybox == 1){
                        renderButton = "<a onclick='openFancybox("+data+")' class='margin-right-10 text-success'><i class='fa fa-lg fa-image'></i></a>";
                    }
                    
                    var splitButton = splitColumn[1].split("-");
                    var tableButton = ['view', 'edit', 'delete'];

                    for(var j in splitButton){
                        if(tableButton.indexOf(splitButton[j]) != -1){
                            var confirmAction = "";
                            var iconButton = "";
                            var btnClass = "";

                            if(splitButton[j] == "edit"){
                                btnClass = 'text-primary';
                                iconButton = 'edit';
                            }
                            else if(splitButton[j] == "delete"){
                                btnClass = 'text-danger';
                                iconButton = 'trash';
                                confirmAction = 'onclick="return confirm(\'Are you sure?\')"';
                            }
                            else if(splitButton[j] == "view"){
                                btnClass = 'text-muted';
                                iconButton = 'eye';
                            }

                            //last row for id
                            renderButton += "<a href='" + $.globalVar('base_url') + datas.controller + "/" + splitButton[j] + "/" + row[row.length - 1] + "' title='"+ splitButton[j] +"' class='margin-right-10 " + btnClass + "' "+confirmAction+"><i class='fa fa-lg fa-" + iconButton + "'></i></a>";
                        }
                    }

                    return renderButton;
                },
                sortable: false
            }
        }
        else{
            var sClass = "v-middle";
            if(column[i] == "price" || column[i] == "qty" || column[i] == "total" || column[i] == "not_sellable") sClass = "v-middle text text-right";

            if(column[i] == "image" || column[i] == "photo"){
                tmp = {
                    title: column[i].replace(/_/g, " ").toUpperCase(),
                    sortable: false,
                    "sClass": sClass + " text-center"
                }
            }
            else{
                tmp = {
                    title: column[i].replace(/_/g, " ").toUpperCase(),
                    "sClass": sClass
                }
            }

            if(column[i] == "product_data_id"){
                hideTmp = {
                    "targets": [ parseInt(i) ],
                    "visible": false
                }

                hideColumns.push(hideTmp);
            }
        }

        columns.push(tmp);
    }

     if(datas.role.indexOf("create") != -1) {
        var createButton = '<a href="' + $.globalVar('base_url') + datas.controller + '/create" id="datatable-create" class="btn blue btn-outline btn-circle">'+
            '<i class="fa fa-plus"></i> Create'+
        '</a>';

        $("#tools-column").append(createButton);
    }

    var searchButton = '<a onclick="tableFilter()" class="btn grey-salsa btn-outline btn-circle">'+
        '<i class="fa fa-search"></i> Search'+
    '</a>';
    $("#tools-column").append(searchButton);

    var toolsButton = '';
    if(datas.role.indexOf("export") != -1) {
        toolsButton = [
            { extend: 'print', className: 'btn default' },
            { extend: 'copy', className: 'btn default' },
            { extend: 'pdf', className: 'btn default' },
            { extend: 'excel', className: 'btn default' },
            { extend: 'csv', className: 'btn default' },
            {
                text: 'Reload',
                className: 'btn default',
                action: function ( e, dt, node, config ) {
                    dt.ajax.reload();
                    alert('Datatable reloaded!');
                }
            },
        ];


        var toolsColumn = '<a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">'+
            '<i class="fa fa-share"></i>'+
            '<span class="hidden-xs"> Tools </span>'+
            '<i class="fa fa-angle-down"></i>'+
        '</a>'+
        '<ul class="dropdown-menu pull-right" id="datatable_ajax_tools">'+
            '<li>'+
                '<a href="javascript:;" data-action="5" class="tool-action">'+
                    '<i class="icon-refresh"></i> Reload</a>'+
            '</li>'+
            '<li>'+
                '<a href="javascript:;" data-action="0" class="tool-action">'+
                    '<i class="icon-printer"></i> Print</a>'+
            '</li>'+
            '<li>'+
                '<a href="javascript:;" data-action="2" class="tool-action">'+
                    '<i class="icon-doc"></i> PDF</a>'+
            '</li>'+
            '<li>'+
                '<a href="javascript:;" data-action="3" class="tool-action">'+
                    '<i class="icon-paper-clip"></i> Excel</a>'+
            '</li>'+
       '</ul>';

        $("#tools-column").append(toolsColumn);
    }

    var order = [];
    order[0] = [datas.order, datas.orderDir];

    if(datas.order2 != null){
        order[1] = [datas.order2, datas.orderDir2];
    }

    grid.init({
        src: $("#" + datas.tableId),
        onSuccess: function (grid, response) {
            // grid:        grid object
            // response:    json object of server side ajax response
            // execute some code after table records loaded
        },
        onError: function (grid) {
            // execute some code on network or other general error  
        },
        onDataLoad: function(grid) {
            // execute some code on ajax data load
        },
        loadingMessage: 'Loading...',
        dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
            // So when dropdowns used the scrollable div should be removed. 
            
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
            "columns": columns,
            
            "lengthMenu": [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            "columnDefs": hideColumns,
            "pageLength": 10, // default record count per page
            "ajax": {
                "url": $.globalVar('base_url') + datas.controller + "/" + datas.source, // ajax source
                "type": "POST"
            },
            "drawCallback": function(oSettings) { // run some code on table redraw
                var api = this.api();
 
                // Output the data for the visible rows to the browser's console
                $("#bulk-data").val(JSON.stringify(api.ajax.json()));

                if(datas.fancybox == 2) $(".profile-pic").fancybox();
            },
            "order": order,// set first column as a default sort by asc
        
            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            buttons: toolsButton,

        }
    });

    //append filter
    var searchColumn = datas.searchColumn;
    var searchClause = datas.searchClause;
    var searchDateFlag = 0;
    var searchTimeFlag = 0;

    if(datas.searchClause == undefined) searchClause = [];

    var lastSearchColumn = searchColumn.length - 1;

    if(datas.filterDisplay == 1){
        var filterColumn = '<tr role="row" id="table-filter" class="filter">';
    }
    else{
        var filterColumn = '<tr role="row" id="table-filter" class="filter" style="display:none;">';
    }

    for(var i in searchColumn){
        if(i == lastSearchColumn && searchColumn[i] == ""){
            filterColumn += '<td>'+ 
                '<button class="btn btn-sm green btn-outline md-skip filter-submit">'+
                    '<i class="fa fa-search"></i> Search</button>'+
                '<button class="btn btn-sm red btn-outline md-skip filter-cancel">'+
                    '<i class="fa fa-times"></i> Reset</button>'+
            '</td>';
        }
        else if(searchColumn[i] == ""){
            filterColumn += "<td></td>";
        }
        else if(searchColumn[i] == "status" || searchColumn[i] == "approve_hierarchy"){
            filterColumn += '<td>'+
                '<select class="form-control form-filter input-sm" name="search_' + searchColumn[i].replace(".", "_") + '">'+
                    '<option value="y">Active</option>'+
                    '<option value="n">Inactive</option>'+
                '</select>'+
            '</td>';
        } else if(searchColumn[i] == "status_approval"){
            filterColumn += '<td>'+
                '<select class="form-control form-filter input-sm" name="search_' + searchColumn[i].replace(".", "_") + '">'+
                    '<option value="w">Waiting</option>'+
                    '<option value="y">Approve</option>'+
                    '<option value="n">Reject</option>'+
                '</select>'+
            '</td>';
        } else if(searchColumn[i][0] == "select"){
            filterColumn += '<td>'
                +'<select class="form-control form-filter input-sm" name="search_' + searchColumn[i][1].replace(".", "_") + '">';
                filterColumn += '<option value="">--Select One--</option>';
                $.each(searchColumn[i][2],function(v,q){
                   filterColumn += '<option value="'+v+'">'+q+'</option>';     
                });
            filterColumn += '</select>'
                +'</td>';
        }
        else if(searchClause[i] == "range_number"){
            filterColumn += '<td>'+
                '<div class="margin-bottom-5">'+
                    '<input type="text" class="form-control form-filter input-sm" name="search_' + searchColumn[i].replace(".", "_") + '_min" placeholder="Min..">'+
                '</div>'+
                '<input type="text" class="form-control form-filter input-sm" name="search_' + searchColumn[i].replace(".", "_") + '_max" placeholder="Max..">'+
            '</td>';
        }
        else if(searchClause[i] == "range_date"){
            filterColumn += '<td width="200">'+
                '<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">'+
                    '<input type="text" class="form-control form-filter input-sm" readonly name="search_' + searchColumn[i].replace(".", "_") + '_from" placeholder="From">'+
                    '<span class="input-group-btn">'+
                        '<button class="btn btn-sm default" type="button">'+
                            '<i class="fa fa-calendar"></i>'+
                        '</button>'+
                    '</span>'+
                '</div>'+
                '<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">'+
                    '<input type="text" class="form-control form-filter input-sm" readonly name="search_' + searchColumn[i].replace(".", "_") + '_to" placeholder="To">'+
                    '<span class="input-group-btn">'+
                        '<button class="btn btn-sm default" type="button">'+
                            '<i class="fa fa-calendar"></i>'+
                        '</button>'+
                    '</span>'+
                '</div>'+

            '</td>';

            searchDateFlag = 1;
        }
        else if(searchClause[i] == "range_time"){
            filterColumn += '<td>'+
                '<div class="bootstrap-timepicker">'+
                    '<div class="form-group">'+
                        '<div class="input-group margin-bottom-5">'+
                            '<input type="text" class="form-control form-filter input-sm timepicker" readonly name="search_' + searchColumn[i].replace(".", "_") + '_from" placeholder="From">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn btn-sm default" type="button">'+
                                    '<i class="fa fa-clock-o"></i>'+
                                '</button>'+
                            '</span>'+
                        '</div>'+
                        '<div class="input-group">'+
                            '<input type="text" class="form-control form-filter input-sm timepicker" readonly name="search_' + searchColumn[i].replace(".", "_") + '_to" placeholder="To">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn btn-sm default" type="button">'+
                                    '<i class="fa fa-clock-o"></i>'+
                                '</button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</td>';

            searchTimeFlag = 1;
        }
        else{
            filterColumn += '<td><input type="text" class="form-control form-filter input-sm" name="search_' + searchColumn[i].replace(".", "_") + '" placeholder="Search ' + column[i].replace("_", " ") + '.."></td>';
        }
    }

    filterColumn += '</tr>';

    $("#" + datas.tableId + " thead").append(filterColumn);
    //end append filter

    // handle group actionsubmit button click
    grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
        e.preventDefault();
        var action = $(".table-group-action-input", grid.getTableWrapper());
        if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
            grid.setAjaxParam("customActionType", "group_action");
            grid.setAjaxParam("customActionName", action.val());
            grid.setAjaxParam("id", grid.getSelectedRows());
            grid.getDataTable().ajax.reload();
            grid.clearAjaxParams();
        } else if (action.val() == "") {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'Please select an action',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        } else if (grid.getSelectedRowsCount() === 0) {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'No record selected',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        }
    });

    grid.setAjaxParam("customActionType", "group_action");
    grid.getDataTable().ajax.reload();
    grid.clearAjaxParams();


    // handle datatable custom tools
    $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
        var action = $(this).attr('data-action');
        grid.getDataTable().button(action).trigger();
    });

    //init datepicker
    if(searchDateFlag == 1){
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    if(searchTimeFlag == 1){
         $('.timepicker').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: false,
            showMeridian: false,
            defaultTime: '08:00'
        });

        // handle input group button click
        $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
            e.preventDefault();
            $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
        });
    }

    $(".form-filter").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);

        if (code == 13) {
            $(".filter-submit").click();
        }
    });
}

function tableFilter(){
    if($('#table-filter').css('display') == 'none'){ 
       $('#table-filter').show('slow'); 
    } else { 
       $('#table-filter').hide('slow'); 
    }
}

function openFancybox(id){
    $.fancybox([
        { href : $("#pp-"+id).val() }
    ]);
}