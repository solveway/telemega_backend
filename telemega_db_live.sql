/*
Navicat MySQL Data Transfer

Source Server         : telemega
Source Server Version : 50638
Source Host           : 132.148.26.11:3306
Source Database       : telemega_db

Target Server Type    : MYSQL
Target Server Version : 50638
File Encoding         : 65001

Date: 2018-02-20 10:28:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `te_authmenu`
-- ----------------------------
DROP TABLE IF EXISTS `te_authmenu`;
CREATE TABLE `te_authmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `upd_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2089 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_authmenu
-- ----------------------------
INSERT INTO `te_authmenu` VALUES ('2088', '5', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2087', '2', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2086', '132', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2085', '131', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2084', '8', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2083', '4', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2082', '3', '1', 'y', 'y', 'y', 'y', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2081', '12', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2080', '134', '1', 'y', 'y', 'n', 'n', 'y', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2079', '133', '1', 'y', 'y', 'n', 'n', 'y', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2078', '129', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2077', '130', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2076', '111', '1', 'n', 'y', 'y', 'n', 'y', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2075', '7', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2074', '112', '1', 'y', 'y', 'y', 'n', 'y', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2053', '1', '3', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-02 13:13:58', '2018-01-02 13:13:58');
INSERT INTO `te_authmenu` VALUES ('2052', '1', '10', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-12-14 13:57:49', '2017-12-14 13:57:49');
INSERT INTO `te_authmenu` VALUES ('2073', '6', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');
INSERT INTO `te_authmenu` VALUES ('2072', '1', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2018-01-03 17:47:08', '2018-01-03 17:47:08');

-- ----------------------------
-- Table structure for `te_category_product`
-- ----------------------------
DROP TABLE IF EXISTS `te_category_product`;
CREATE TABLE `te_category_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_product_name` varchar(100) DEFAULT NULL,
  `description` text,
  `status` char(2) DEFAULT NULL,
  `image` varchar(100) DEFAULT '0',
  `upd_by` int(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_category_product
-- ----------------------------
INSERT INTO `te_category_product` VALUES ('1', 'Kartu Perdana', null, 'y', '0', null, '0000-00-00 00:00:00', '2018-01-09 15:43:00');
INSERT INTO `te_category_product` VALUES ('3', 'Dompet Pulsa', null, 'y', '0', null, '0000-00-00 00:00:00', '2018-01-09 15:42:52');

-- ----------------------------
-- Table structure for `te_city`
-- ----------------------------
DROP TABLE IF EXISTS `te_city`;
CREATE TABLE `te_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_city
-- ----------------------------
INSERT INTO `te_city` VALUES ('1', '21', '1', 'Aceh Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('2', '21', '2', 'Aceh Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('3', '21', '3', 'Aceh Besar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('4', '21', '4', 'Aceh Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('5', '21', '5', 'Aceh Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('6', '21', '6', 'Aceh Singkil', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('7', '21', '7', 'Aceh Tamiang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('8', '21', '8', 'Aceh Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('9', '21', '9', 'Aceh Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('10', '21', '10', 'Aceh Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('11', '21', '11', 'Aceh Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('12', '32', '12', 'Agam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('13', '23', '13', 'Alor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('14', '19', '14', 'Ambon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('15', '34', '15', 'Asahan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('16', '24', '16', 'Asmat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('17', '1', '17', 'Badung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('18', '13', '18', 'Balangan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('19', '15', '19', 'Balikpapan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('20', '21', '20', 'Banda Aceh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('21', '18', '21', 'Bandar Lampung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('22', '9', '22', 'Bandung Kabupaten', '2016-06-02 11:12:54', '2017-09-27 14:50:53');
INSERT INTO `te_city` VALUES ('23', '9', '23', 'Bandung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('24', '9', '24', 'Bandung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('25', '29', '25', 'Banggai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('26', '29', '26', 'Banggai Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('27', '2', '27', 'Bangka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('28', '2', '28', 'Bangka Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('29', '2', '29', 'Bangka Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('30', '2', '30', 'Bangka Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('31', '11', '31', 'Bangkalan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('32', '1', '32', 'Bangli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('33', '13', '33', 'Banjar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('34', '9', '34', 'Banjar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('35', '13', '35', 'Banjarbaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('36', '13', '36', 'Banjarmasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('37', '10', '37', 'Banjarnegara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('38', '28', '38', 'Bantaeng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('39', '5', '39', 'Bantul', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('40', '33', '40', 'Banyuasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('41', '10', '41', 'Banyumas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('42', '11', '42', 'Banyuwangi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('43', '13', '43', 'Barito Kuala', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('44', '14', '44', 'Barito Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('45', '14', '45', 'Barito Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('46', '14', '46', 'Barito Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('47', '28', '47', 'Barru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('48', '17', '48', 'Batam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('49', '10', '49', 'Batang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('50', '8', '50', 'Batang Hari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('51', '11', '51', 'Batu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('52', '34', '52', 'Batu Bara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('53', '30', '53', 'Bau-Bau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('54', '9', '54', 'Bekasi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('55', '9', '55', 'Bekasi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('56', '2', '56', 'Belitung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('57', '2', '57', 'Belitung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('58', '23', '58', 'Belu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('59', '21', '59', 'Bener Meriah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('60', '26', '60', 'Bengkalis', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('61', '12', '61', 'Bengkayang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('62', '4', '62', 'Bengkulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('63', '4', '63', 'Bengkulu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('64', '4', '64', 'Bengkulu Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('65', '4', '65', 'Bengkulu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('66', '15', '66', 'Berau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('67', '24', '67', 'Biak Numfor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('68', '22', '68', 'Bima', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('69', '22', '69', 'Bima', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('70', '34', '70', 'Binjai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('71', '17', '71', 'Bintan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('72', '21', '72', 'Bireuen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('73', '31', '73', 'Bitung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('74', '11', '74', 'Blitar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('75', '11', '75', 'Blitar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('76', '10', '76', 'Blora', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('77', '7', '77', 'Boalemo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('78', '9', '78', 'Bogor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('79', '9', '79', 'Bogor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('80', '11', '80', 'Bojonegoro', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('81', '31', '81', 'Bolaang Mongondow (Bolmong)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('82', '31', '82', 'Bolaang Mongondow Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('83', '31', '83', 'Bolaang Mongondow Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('84', '31', '84', 'Bolaang Mongondow Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('85', '30', '85', 'Bombana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('86', '11', '86', 'Bondowoso', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('87', '28', '87', 'Bone', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('88', '7', '88', 'Bone Bolango', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('89', '15', '89', 'Bontang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('90', '24', '90', 'Boven Digoel', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('91', '10', '91', 'Boyolali', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('92', '10', '92', 'Brebes', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('93', '32', '93', 'Bukittinggi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('94', '1', '94', 'Buleleng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('95', '28', '95', 'Bulukumba', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('96', '16', '96', 'Bulungan (Bulongan)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('97', '8', '97', 'Bungo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('98', '29', '98', 'Buol', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('99', '19', '99', 'Buru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('100', '19', '100', 'Buru Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('101', '30', '101', 'Buton', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('102', '30', '102', 'Buton Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('103', '9', '103', 'Ciamis', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('104', '9', '104', 'Cianjur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('105', '10', '105', 'Cilacap', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('106', '3', '106', 'Cilegon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('107', '9', '107', 'Cimahi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('108', '9', '108', 'Cirebon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('109', '9', '109', 'Cirebon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('110', '34', '110', 'Dairi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('111', '24', '111', 'Deiyai (Deliyai)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('112', '34', '112', 'Deli Serdang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('113', '10', '113', 'Demak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('114', '1', '114', 'Denpasar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('115', '9', '115', 'Depok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('116', '32', '116', 'Dharmasraya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('117', '24', '117', 'Dogiyai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('118', '22', '118', 'Dompu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('119', '29', '119', 'Donggala', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('120', '26', '120', 'Dumai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('121', '33', '121', 'Empat Lawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('122', '23', '122', 'Ende', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('123', '28', '123', 'Enrekang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('124', '25', '124', 'Fakfak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('125', '23', '125', 'Flores Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('126', '9', '126', 'Garut', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('127', '21', '127', 'Gayo Lues', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('128', '1', '128', 'Gianyar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('129', '7', '129', 'Gorontalo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('130', '7', '130', 'Gorontalo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('131', '7', '131', 'Gorontalo Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('132', '28', '132', 'Gowa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('133', '11', '133', 'Gresik', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('134', '10', '134', 'Grobogan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('135', '5', '135', 'Gunung Kidul', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('136', '14', '136', 'Gunung Mas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('137', '34', '137', 'Gunungsitoli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('138', '20', '138', 'Halmahera Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('139', '20', '139', 'Halmahera Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('140', '20', '140', 'Halmahera Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('141', '20', '141', 'Halmahera Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('142', '20', '142', 'Halmahera Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('143', '13', '143', 'Hulu Sungai Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('144', '13', '144', 'Hulu Sungai Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('145', '13', '145', 'Hulu Sungai Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('146', '34', '146', 'Humbang Hasundutan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('147', '26', '147', 'Indragiri Hilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('148', '26', '148', 'Indragiri Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('149', '9', '149', 'Indramayu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('150', '24', '150', 'Intan Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('151', '6', '151', 'Jakarta Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('152', '6', '152', 'Jakarta Pusat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('153', '6', '153', 'Jakarta Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('154', '6', '154', 'Jakarta Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('155', '6', '155', 'Jakarta Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('156', '8', '156', 'Jambi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('157', '24', '157', 'Jayapura', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('158', '24', '158', 'Jayapura', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('159', '24', '159', 'Jayawijaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('160', '11', '160', 'Jember', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('161', '1', '161', 'Jembrana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('162', '28', '162', 'Jeneponto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('163', '10', '163', 'Jepara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('164', '11', '164', 'Jombang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('165', '25', '165', 'Kaimana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('166', '26', '166', 'Kampar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('167', '14', '167', 'Kapuas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('168', '12', '168', 'Kapuas Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('169', '10', '169', 'Karanganyar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('170', '1', '170', 'Karangasem', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('171', '9', '171', 'Karawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('172', '17', '172', 'Karimun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('173', '34', '173', 'Karo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('174', '14', '174', 'Katingan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('175', '4', '175', 'Kaur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('176', '12', '176', 'Kayong Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('177', '10', '177', 'Kebumen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('178', '11', '178', 'Kediri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('179', '11', '179', 'Kediri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('180', '24', '180', 'Keerom', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('181', '10', '181', 'Kendal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('182', '30', '182', 'Kendari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('183', '4', '183', 'Kepahiang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('184', '17', '184', 'Kepulauan Anambas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('185', '19', '185', 'Kepulauan Aru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('186', '32', '186', 'Kepulauan Mentawai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('187', '26', '187', 'Kepulauan Meranti', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('188', '31', '188', 'Kepulauan Sangihe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('189', '6', '189', 'Kepulauan Seribu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('190', '31', '190', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('191', '20', '191', 'Kepulauan Sula', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('192', '31', '192', 'Kepulauan Talaud', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('193', '24', '193', 'Kepulauan Yapen (Yapen Waropen)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('194', '8', '194', 'Kerinci', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('195', '12', '195', 'Ketapang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('196', '10', '196', 'Klaten', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('197', '1', '197', 'Klungkung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('198', '30', '198', 'Kolaka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('199', '30', '199', 'Kolaka Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('200', '30', '200', 'Konawe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('201', '30', '201', 'Konawe Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('202', '30', '202', 'Konawe Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('203', '13', '203', 'Kotabaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('204', '31', '204', 'Kotamobagu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('205', '14', '205', 'Kotawaringin Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('206', '14', '206', 'Kotawaringin Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('207', '26', '207', 'Kuantan Singingi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('208', '12', '208', 'Kubu Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('209', '10', '209', 'Kudus', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('210', '5', '210', 'Kulon Progo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('211', '9', '211', 'Kuningan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('212', '23', '212', 'Kupang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('213', '23', '213', 'Kupang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('214', '15', '214', 'Kutai Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('215', '15', '215', 'Kutai Kartanegara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('216', '15', '216', 'Kutai Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('217', '34', '217', 'Labuhan Batu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('218', '34', '218', 'Labuhan Batu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('219', '34', '219', 'Labuhan Batu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('220', '33', '220', 'Lahat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('221', '14', '221', 'Lamandau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('222', '11', '222', 'Lamongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('223', '18', '223', 'Lampung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('224', '18', '224', 'Lampung Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('225', '18', '225', 'Lampung Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('226', '18', '226', 'Lampung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('227', '18', '227', 'Lampung Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('228', '12', '228', 'Landak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('229', '34', '229', 'Langkat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('230', '21', '230', 'Langsa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('231', '24', '231', 'Lanny Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('232', '3', '232', 'Lebak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('233', '4', '233', 'Lebong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('234', '23', '234', 'Lembata', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('235', '21', '235', 'Lhokseumawe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('236', '32', '236', 'Lima Puluh Koto/Kota', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('237', '17', '237', 'Lingga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('238', '22', '238', 'Lombok Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('239', '22', '239', 'Lombok Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('240', '22', '240', 'Lombok Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('241', '22', '241', 'Lombok Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('242', '33', '242', 'Lubuk Linggau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('243', '11', '243', 'Lumajang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('244', '28', '244', 'Luwu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('245', '28', '245', 'Luwu Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('246', '28', '246', 'Luwu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('247', '11', '247', 'Madiun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('248', '11', '248', 'Madiun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('249', '10', '249', 'Magelang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('250', '10', '250', 'Magelang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('251', '11', '251', 'Magetan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('252', '9', '252', 'Majalengka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('253', '27', '253', 'Majene', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('254', '28', '254', 'Makassar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('255', '11', '255', 'Malang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('256', '11', '256', 'Malang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('257', '16', '257', 'Malinau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('258', '19', '258', 'Maluku Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('259', '19', '259', 'Maluku Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('260', '19', '260', 'Maluku Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('261', '19', '261', 'Maluku Tenggara Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('262', '27', '262', 'Mamasa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('263', '24', '263', 'Mamberamo Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('264', '24', '264', 'Mamberamo Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('265', '27', '265', 'Mamuju', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('266', '27', '266', 'Mamuju Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('267', '31', '267', 'Manado', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('268', '34', '268', 'Mandailing Natal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('269', '23', '269', 'Manggarai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('270', '23', '270', 'Manggarai Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('271', '23', '271', 'Manggarai Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('272', '25', '272', 'Manokwari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('273', '25', '273', 'Manokwari Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('274', '24', '274', 'Mappi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('275', '28', '275', 'Maros', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('276', '22', '276', 'Mataram', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('277', '25', '277', 'Maybrat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('278', '34', '278', 'Medan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('279', '12', '279', 'Melawi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('280', '8', '280', 'Merangin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('281', '24', '281', 'Merauke', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('282', '18', '282', 'Mesuji', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('283', '18', '283', 'Metro', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('284', '24', '284', 'Mimika', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('285', '31', '285', 'Minahasa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('286', '31', '286', 'Minahasa Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('287', '31', '287', 'Minahasa Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('288', '31', '288', 'Minahasa Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('289', '11', '289', 'Mojokerto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('290', '11', '290', 'Mojokerto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('291', '29', '291', 'Morowali', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('292', '33', '292', 'Muara Enim', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('293', '8', '293', 'Muaro Jambi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('294', '4', '294', 'Muko Muko', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('295', '30', '295', 'Muna', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('296', '14', '296', 'Murung Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('297', '33', '297', 'Musi Banyuasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('298', '33', '298', 'Musi Rawas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('299', '24', '299', 'Nabire', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('300', '21', '300', 'Nagan Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('301', '23', '301', 'Nagekeo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('302', '17', '302', 'Natuna', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('303', '24', '303', 'Nduga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('304', '23', '304', 'Ngada', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('305', '11', '305', 'Nganjuk', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('306', '11', '306', 'Ngawi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('307', '34', '307', 'Nias', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('308', '34', '308', 'Nias Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('309', '34', '309', 'Nias Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('310', '34', '310', 'Nias Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('311', '16', '311', 'Nunukan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('312', '33', '312', 'Ogan Ilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('313', '33', '313', 'Ogan Komering Ilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('314', '33', '314', 'Ogan Komering Ulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('315', '33', '315', 'Ogan Komering Ulu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('316', '33', '316', 'Ogan Komering Ulu Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('317', '11', '317', 'Pacitan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('318', '32', '318', 'Padang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('319', '34', '319', 'Padang Lawas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('320', '34', '320', 'Padang Lawas Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('321', '32', '321', 'Padang Panjang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('322', '32', '322', 'Padang Pariaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('323', '34', '323', 'Padang Sidempuan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('324', '33', '324', 'Pagar Alam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('325', '34', '325', 'Pakpak Bharat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('326', '14', '326', 'Palangka Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('327', '33', '327', 'Palembang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('328', '28', '328', 'Palopo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('329', '29', '329', 'Palu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('330', '11', '330', 'Pamekasan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('331', '3', '331', 'Pandeglang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('332', '9', '332', 'Pangandaran', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('333', '28', '333', 'Pangkajene Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('334', '2', '334', 'Pangkal Pinang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('335', '24', '335', 'Paniai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('336', '28', '336', 'Parepare', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('337', '32', '337', 'Pariaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('338', '29', '338', 'Parigi Moutong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('339', '32', '339', 'Pasaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('340', '32', '340', 'Pasaman Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('341', '15', '341', 'Paser', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('342', '11', '342', 'Pasuruan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('343', '11', '343', 'Pasuruan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('344', '10', '344', 'Pati', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('345', '32', '345', 'Payakumbuh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('346', '25', '346', 'Pegunungan Arfak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('347', '24', '347', 'Pegunungan Bintang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('348', '10', '348', 'Pekalongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('349', '10', '349', 'Pekalongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('350', '26', '350', 'Pekanbaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('351', '26', '351', 'Pelalawan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('352', '10', '352', 'Pemalang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('353', '34', '353', 'Pematang Siantar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('354', '15', '354', 'Penajam Paser Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('355', '18', '355', 'Pesawaran', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('356', '18', '356', 'Pesisir Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('357', '32', '357', 'Pesisir Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('358', '21', '358', 'Pidie', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('359', '21', '359', 'Pidie Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('360', '28', '360', 'Pinrang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('361', '7', '361', 'Pohuwato', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('362', '27', '362', 'Polewali Mandar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('363', '11', '363', 'Ponorogo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('364', '12', '364', 'Pontianak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('365', '12', '365', 'Pontianak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('366', '29', '366', 'Poso', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('367', '33', '367', 'Prabumulih', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('368', '18', '368', 'Pringsewu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('369', '11', '369', 'Probolinggo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('370', '11', '370', 'Probolinggo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('371', '14', '371', 'Pulang Pisau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('372', '20', '372', 'Pulau Morotai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('373', '24', '373', 'Puncak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('374', '24', '374', 'Puncak Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('375', '10', '375', 'Purbalingga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('376', '9', '376', 'Purwakarta', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('377', '10', '377', 'Purworejo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('378', '25', '378', 'Raja Ampat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('379', '4', '379', 'Rejang Lebong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('380', '10', '380', 'Rembang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('381', '26', '381', 'Rokan Hilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('382', '26', '382', 'Rokan Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('383', '23', '383', 'Rote Ndao', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('384', '21', '384', 'Sabang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('385', '23', '385', 'Sabu Raijua', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('386', '10', '386', 'Salatiga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('387', '15', '387', 'Samarinda', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('388', '12', '388', 'Sambas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('389', '34', '389', 'Samosir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('390', '11', '390', 'Sampang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('391', '12', '391', 'Sanggau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('392', '24', '392', 'Sarmi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('393', '8', '393', 'Sarolangun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('394', '32', '394', 'Sawah Lunto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('395', '12', '395', 'Sekadau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('396', '28', '396', 'Selayar (Kepulauan Selayar)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('397', '4', '397', 'Seluma', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('398', '10', '398', 'Semarang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('399', '10', '399', 'Semarang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('400', '19', '400', 'Seram Bagian Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('401', '19', '401', 'Seram Bagian Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('402', '3', '402', 'Serang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('403', '3', '403', 'Serang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('404', '34', '404', 'Serdang Bedagai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('405', '14', '405', 'Seruyan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('406', '26', '406', 'Siak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('407', '34', '407', 'Sibolga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('408', '28', '408', 'Sidenreng Rappang/Rapang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('409', '11', '409', 'Sidoarjo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('410', '29', '410', 'Sigi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('411', '32', '411', 'Sijunjung (Sawah Lunto Sijunjung)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('412', '23', '412', 'Sikka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('413', '34', '413', 'Simalungun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('414', '21', '414', 'Simeulue', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('415', '12', '415', 'Singkawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('416', '28', '416', 'Sinjai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('417', '12', '417', 'Sintang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('418', '11', '418', 'Situbondo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('419', '5', '419', 'Sleman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('420', '32', '420', 'Solok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('421', '32', '421', 'Solok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('422', '32', '422', 'Solok Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('423', '28', '423', 'Soppeng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('424', '25', '424', 'Sorong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('425', '25', '425', 'Sorong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('426', '25', '426', 'Sorong Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('427', '10', '427', 'Sragen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('428', '9', '428', 'Subang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('429', '21', '429', 'Subulussalam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('430', '9', '430', 'Sukabumi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('431', '9', '431', 'Sukabumi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('432', '14', '432', 'Sukamara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('433', '10', '433', 'Sukoharjo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('434', '23', '434', 'Sumba Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('435', '23', '435', 'Sumba Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('436', '23', '436', 'Sumba Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('437', '23', '437', 'Sumba Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('438', '22', '438', 'Sumbawa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('439', '22', '439', 'Sumbawa Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('440', '9', '440', 'Sumedang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('441', '11', '441', 'Sumenep', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('442', '8', '442', 'Sungaipenuh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('443', '24', '443', 'Supiori', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('444', '11', '444', 'Surabaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('445', '10', '445', 'Surakarta (Solo)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('446', '13', '446', 'Tabalong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('447', '1', '447', 'Tabanan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('448', '28', '448', 'Takalar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('449', '25', '449', 'Tambrauw', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('450', '16', '450', 'Tana Tidung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('451', '28', '451', 'Tana Toraja', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('452', '13', '452', 'Tanah Bumbu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('453', '32', '453', 'Tanah Datar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('454', '13', '454', 'Tanah Laut', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('455', '3', '455', 'Tangerang Kabupaten', '2016-06-02 11:12:54', '2017-09-27 14:46:50');
INSERT INTO `te_city` VALUES ('456', '3', '456', 'Tangerang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('457', '3', '457', 'Tangerang Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('458', '18', '458', 'Tanggamus', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('459', '34', '459', 'Tanjung Balai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('460', '8', '460', 'Tanjung Jabung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('461', '8', '461', 'Tanjung Jabung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('462', '17', '462', 'Tanjung Pinang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('463', '34', '463', 'Tapanuli Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('464', '34', '464', 'Tapanuli Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('465', '34', '465', 'Tapanuli Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('466', '13', '466', 'Tapin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('467', '16', '467', 'Tarakan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('468', '9', '468', 'Tasikmalaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('469', '9', '469', 'Tasikmalaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('470', '34', '470', 'Tebing Tinggi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('471', '8', '471', 'Tebo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('472', '10', '472', 'Tegal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('473', '10', '473', 'Tegal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('474', '25', '474', 'Teluk Bintuni', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('475', '25', '475', 'Teluk Wondama', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('476', '10', '476', 'Temanggung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('477', '20', '477', 'Ternate', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('478', '20', '478', 'Tidore Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('479', '23', '479', 'Timor Tengah Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('480', '23', '480', 'Timor Tengah Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('481', '34', '481', 'Toba Samosir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('482', '29', '482', 'Tojo Una-Una', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('483', '29', '483', 'Toli-Toli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('484', '24', '484', 'Tolikara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('485', '31', '485', 'Tomohon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('486', '28', '486', 'Toraja Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('487', '11', '487', 'Trenggalek', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('488', '19', '488', 'Tual', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('489', '11', '489', 'Tuban', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('490', '18', '490', 'Tulang Bawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('491', '18', '491', 'Tulang Bawang Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('492', '11', '492', 'Tulungagung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('493', '28', '493', 'Wajo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('494', '30', '494', 'Wakatobi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('495', '24', '495', 'Waropen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('496', '18', '496', 'Way Kanan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('497', '10', '497', 'Wonogiri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('498', '10', '498', 'Wonosobo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('499', '24', '499', 'Yahukimo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('500', '24', '500', 'Yalimo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `te_city` VALUES ('501', '5', '501', 'Yogyakarta', '2016-06-02 11:12:54', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `te_config`
-- ----------------------------
DROP TABLE IF EXISTS `te_config`;
CREATE TABLE `te_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` text,
  `config_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_config
-- ----------------------------
INSERT INTO `te_config` VALUES ('1', 'web_name', 'Tele Ring', 'both', null, '2018-01-03 16:15:14');
INSERT INTO `te_config` VALUES ('2', 'web_logo', 'UtW6hW_telering_logo.png', 'both', null, '2018-01-03 16:15:14');
INSERT INTO `te_config` VALUES ('3', 'web_email', 'elim@solveway.co.id', 'both', null, '2017-10-24 15:50:13');
INSERT INTO `te_config` VALUES ('4', 'favicon', 'idrhWB_telering_logo.png', 'both', null, '2018-01-03 16:15:14');
INSERT INTO `te_config` VALUES ('5', 'web_title', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `te_config` VALUES ('6', 'web_description', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `te_config` VALUES ('7', 'web_keywords', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `te_config` VALUES ('8', 'maintenance_mode', 'n', 'front', '0000-00-00 00:00:00', '2016-05-19 05:26:19');
INSERT INTO `te_config` VALUES ('9', 'background_color', '#d622cf', 'front', '0000-00-00 00:00:00', '2016-05-29 09:25:41');
INSERT INTO `te_config` VALUES ('10', 'font_color', '#2f9fd6', 'front', '0000-00-00 00:00:00', '2016-05-29 09:25:43');
INSERT INTO `te_config` VALUES ('11', 'send_email', 'n', 'both', '0000-00-00 00:00:00', '2016-11-29 10:18:35');
INSERT INTO `te_config` VALUES ('12', 'send_sms', 'n', 'both', '0000-00-00 00:00:00', '2016-11-29 09:07:11');
INSERT INTO `te_config` VALUES ('13', 'deadline_payment', '1', 'both', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `te_config` VALUES ('14', 'phone', '021-22553223', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `te_config` VALUES ('15', 'address', '<p>Perkantoran Sedayu Square Blok C No 32<br />Cengkareng, Jakarta Barat</p>', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `te_config` VALUES ('16', 'header_info', 'Happy Shopping Sista', 'front', '0000-00-00 00:00:00', '2016-12-03 03:59:47');
INSERT INTO `te_config` VALUES ('17', 'shipping_returns_product', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>\r\n<ul>\r\n<li>Any Product types that You want - Simple, Configurable</li>\r\n<li>Downloadable/Digital Products, Virtual Products</li>\r\n<li>Inventory Management with Backordered items</li>\r\n<li>Customer Personal Products - upload text for embroidery, monogramming</li>\r\n<li>Create Store-specific attributes on the fly</li>\r\n</ul>', 'front', '0000-00-00 00:00:00', '2016-12-03 15:23:01');
INSERT INTO `te_config` VALUES ('18', 'terms_condition_product', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>\r\n<ul>\r\n<li>Any Product types that You want - Simple, Configurable</li>\r\n<li>Downloadable/Digital Products, Virtual Products</li>\r\n<li>Inventory Management with Backordered items</li>\r\n<li>Customer Personal Products - upload text for embroidery, monogramming</li>\r\n<li>Create Store-specific attributes on the fly</li>\r\n</ul>', 'front', '0000-00-00 00:00:00', '2016-12-03 15:23:01');
INSERT INTO `te_config` VALUES ('19', 'gmaps', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3967.0150615360963!2d106.72692481476865!3d-6.128674995562458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d5201130ab3%3A0xe0586c3d9f378cd0!2sSolveway+Digital!5e0!3m2!1sid!2sid!4v1497410446355', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `te_config` VALUES ('20', 'city', '151', 'front', '0000-00-00 00:00:00', '2017-05-31 09:59:28');
INSERT INTO `te_config` VALUES ('21', 'fax', '', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `te_config` VALUES ('22', 'product_version', '59', 'both', '0000-00-00 00:00:00', '2017-12-18 12:03:37');
INSERT INTO `te_config` VALUES ('23', 'about', 'Nitrogen', 'both', '0000-00-00 00:00:00', '2017-10-17 09:04:15');

-- ----------------------------
-- Table structure for `te_customer`
-- ----------------------------
DROP TABLE IF EXISTS `te_customer`;
CREATE TABLE `te_customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(100) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `no_hp` varchar(100) DEFAULT '2',
  `email` varchar(50) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `address` text,
  `status` varchar(1) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `radius` varchar(255) DEFAULT NULL COMMENT 'satuan meter',
  `no_ktp` varchar(100) DEFAULT NULL,
  `no_npwp` varchar(100) DEFAULT NULL,
  `no_dompul` varchar(100) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_customer
-- ----------------------------
INSERT INTO `te_customer` VALUES ('1', 'admin', '1', '2', 'admin@admin.com', 'GhsSde_2-9670.jpg', 'cent', 'y', null, null, null, null, null, null, '1', '2015-09-30 02:02:34', '2017-12-22 17:18:01');
INSERT INTO `te_customer` VALUES ('2', 'admin_yudi', '2', '2', 'op_nitro@gmail.com', '7scb4t_suzu.jpg', 'ADMIN NITROGEN', 'y', null, null, null, null, null, null, '1', '2017-10-02 10:37:50', '2017-12-14 15:28:36');
INSERT INTO `te_customer` VALUES ('3', 'admin_indri', '2', '2', 'admin_nitro@gmail.com', 'zPzrvQ_Koala.jpg', 'admin_nitro', 'y', null, null, null, null, null, null, '1', '0000-00-00 00:00:00', '2017-12-22 10:23:08');
INSERT INTO `te_customer` VALUES ('45', 'admin_abi', '2', '2', 'abi123@gmail.com', null, 'ADMIN NITROGEN', 'y', null, null, null, null, null, null, '1', '0000-00-00 00:00:00', '2017-12-14 15:30:30');
INSERT INTO `te_customer` VALUES ('46', 'admin_khotimi', '2', '2', 'op_keppo@gmail.com', 'Hugauv_Hydrangeas.jpg', 'ADMIN NITROGEN', 'y', null, null, null, null, null, null, '1', '2017-10-23 15:52:25', '2017-12-14 15:31:29');
INSERT INTO `te_customer` VALUES ('47', 'admin_daroni', '2', '2', 'daroni@gmail.com', 'g9irrH_Koala.jpg', 'ADMIN NITROGEN', 'y', null, null, null, null, null, null, '1', '2017-10-24 14:06:40', '2017-12-19 01:43:47');
INSERT INTO `te_customer` VALUES ('48', 'dedesumardi', '3', '2', 'op_kampungsawah@gmail.com', 'V04kad_Koala.jpg', 'operator untuk kampung sawah', 'y', null, null, null, null, null, null, '1', '2017-10-27 13:25:47', '2017-12-22 06:58:57');
INSERT INTO `te_customer` VALUES ('49', 'muhamadsaputra', '3', '2', 'saputra@gmail.com', 'nznHT4_PUTRA.jpg', 'OPERATOR NITROGEN', 'y', '-6.1896776', '106.8381752', null, null, null, null, '1', '2017-12-14 15:34:31', '2018-01-30 09:58:47');
INSERT INTO `te_customer` VALUES ('50', '99 Cell', '0', '127', 'elimsuhendra@gmail.com', 'Tx9THV_Tulips.jpg', 'Jl. Pluit Karang Cantik No. 6', 'y', '-6.113280121056015', '106.77825465060732', null, '11111111', '2222222', '6281809720134', '1', '2018-01-17 14:13:07', '2018-01-29 06:30:55');
INSERT INTO `te_customer` VALUES ('51', '88 Cell', 'Elim', '08999878046', 'elimsuhendra@gmail.com', 'jfyfRt_Lighthouse.jpg', 'Jl. Kemanggisan Raya', 'y', '-6.170563260586048', '106.76951064921877', null, '11111111', '2222222', '6285921268484\r\n6285921268484\r\n6285921268484', '1', '2018-01-17 14:15:21', '2018-01-29 06:31:10');
INSERT INTO `te_customer` VALUES ('52', '77 Cell', 'Elim', '08999878046', 'elimsuhendra@gmail.com', 'uRfX9e_Tulips.jpg', 'Jl. Kemanggisan Raya', 'y', '-6.1896776', '106.8381752', null, '11111111', '2222222', '6281918731238', '1', '2018-01-17 14:33:53', '2018-01-29 06:31:15');
INSERT INTO `te_customer` VALUES ('53', '99 Cell', 'Budi', '127', 'elimsuhendra@gmail.com', null, 'Jl. Pluit Karang Cantik No. 6', 'y', '-6.113280121056015', '106.77825465060732', null, '11111111', '2222222', '6281910592807', '1', '2018-01-17 14:59:39', '2018-01-29 06:31:37');
INSERT INTO `te_customer` VALUES ('54', '99 Cell', 'Budi', '127', 'elimsuhendra@gmail.com', 'q3mm5G_Tulips.jpg', 'Jl. Pluit Karang Cantik No. 6', 'y', '-6.113280121056015', '106.77825465060732', null, '11111111', '2222222', '6287821479377', '1', '2018-01-17 15:25:50', '2018-01-29 06:31:30');

-- ----------------------------
-- Table structure for `te_dompul_reguler`
-- ----------------------------
DROP TABLE IF EXISTS `te_dompul_reguler`;
CREATE TABLE `te_dompul_reguler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nominal` int(100) DEFAULT NULL,
  `discount` float(11,1) DEFAULT '0.0',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_dompul_reguler
-- ----------------------------
INSERT INTO `te_dompul_reguler` VALUES ('5', '25000', '1.5', '0000-00-00 00:00:00', '2018-01-26 16:12:13');
INSERT INTO `te_dompul_reguler` VALUES ('6', '50000', '1.5', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `te_dompul_reguler` VALUES ('7', '100000', '1.5', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `te_dompul_reguler` VALUES ('8', '200000', '1.5', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `te_inbox`
-- ----------------------------
DROP TABLE IF EXISTS `te_inbox`;
CREATE TABLE `te_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inbox_name` text,
  `sales_id` int(11) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `read` varchar(2) DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_inbox
-- ----------------------------
INSERT INTO `te_inbox` VALUES ('1', 'Pesan 20 perdana', '50', 'Ada cust mau pesan perdana nomor cantik 20 pcs untuk minggu depan', '2018-01-25 15:36:11', '2018-01-25 15:36:11', 'n');
INSERT INTO `te_inbox` VALUES ('2', 'Pesan 12 Perdana', '50', 'ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan. ada yg mau pesan 12 perdana nomor cantik, tolong di sediakan untuk senin depan.', '2018-01-25 15:42:54', '2018-01-25 15:42:54', 'n');

-- ----------------------------
-- Table structure for `te_mslanguage`
-- ----------------------------
DROP TABLE IF EXISTS `te_mslanguage`;
CREATE TABLE `te_mslanguage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(30) DEFAULT NULL,
  `language_name_alias` varchar(30) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `order` int(10) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_mslanguage
-- ----------------------------
INSERT INTO `te_mslanguage` VALUES ('1', 'Indonesia', 'id', 'y', '1', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `te_mslanguage` VALUES ('2', 'English', 'en', 'y', '2', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `te_msmenu`
-- ----------------------------
DROP TABLE IF EXISTS `te_msmenu`;
CREATE TABLE `te_msmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(30) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_name_alias` varchar(50) DEFAULT NULL,
  `menu_id` int(10) DEFAULT NULL,
  `status_parent` varchar(1) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `order` int(2) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_msmenu
-- ----------------------------
INSERT INTO `te_msmenu` VALUES ('1', 'Control Menu', 'Dashboard', '/', null, 'y', 'fa fa-home', 'n', 'n', 'n', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-08-16 17:26:36');
INSERT INTO `te_msmenu` VALUES ('2', null, 'Preferences', 'preferences', null, 'y', 'fa fa-wrench', 'n', 'n', 'n', 'n', 'n', 'n', '25', 'y', '0000-00-00 00:00:00', '2016-11-25 07:28:06');
INSERT INTO `te_msmenu` VALUES ('3', null, 'User', 'user', '12', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2016-06-01 08:06:05');
INSERT INTO `te_msmenu` VALUES ('4', null, 'User Access', 'user-access', '12', 'n', null, 'y', 'y', 'y', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-10-24 16:55:22');
INSERT INTO `te_msmenu` VALUES ('5', null, 'General Settings', 'general-settings', '2', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '4', 'y', '0000-00-00 00:00:00', '2016-11-29 10:19:50');
INSERT INTO `te_msmenu` VALUES ('111', null, 'Adjustment', 'adjustment', '7', 'n', null, 'n', 'y', 'y', 'n', 'y', 'n', '5', 'y', '0000-00-00 00:00:00', '2018-01-03 16:52:33');
INSERT INTO `te_msmenu` VALUES ('130', null, 'Package', 'package', '7', 'n', null, 'y', 'y', 'y', 'n', 'n', 'n', '4', 'y', '0000-00-00 00:00:00', '2018-01-03 16:52:31');
INSERT INTO `te_msmenu` VALUES ('129', null, 'Manage', 'manage-product', '7', 'n', '', 'y', 'y', 'y', 'n', 'n', 'n', '3', 'y', '0000-00-00 00:00:00', '2018-01-03 16:52:29');
INSERT INTO `te_msmenu` VALUES ('12', null, 'Administration', 'administration', null, 'y', 'fa fa-cog', 'n', 'n', 'n', 'n', 'n', 'n', '12', 'y', '0000-00-00 00:00:00', '2016-06-01 09:36:11');
INSERT INTO `te_msmenu` VALUES ('6', null, 'Outlet', 'outlets', null, 'y', 'fa fa-flag', 'n', 'n', 'n', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-09-26 17:47:28');
INSERT INTO `te_msmenu` VALUES ('131', null, 'Stock Report', 'stock-report', '8', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-10-19 10:44:34');
INSERT INTO `te_msmenu` VALUES ('112', '', 'Manage', 'manage-outlet', '6', 'n', '', 'y', 'y', 'y', 'n', 'y', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-09-26 18:04:23');
INSERT INTO `te_msmenu` VALUES ('132', null, 'Outlet Income Report', 'outlet-income-report', '8', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-10-19 10:55:11');
INSERT INTO `te_msmenu` VALUES ('8', null, 'Report', 'report', null, 'y', 'fa fa-bar-chart', 'n', 'n', 'n', 'n', 'n', 'n', '12', 'y', '0000-00-00 00:00:00', '2017-09-26 18:03:40');
INSERT INTO `te_msmenu` VALUES ('7', null, 'Product', 'products', null, 'y', 'fa fa-cubes', 'n', 'n', 'y', 'n', 'n', 'n', '9', 'y', '0000-00-00 00:00:00', '2017-11-13 11:46:50');
INSERT INTO `te_msmenu` VALUES ('133', null, 'Category', 'category-product', '7', 'n', null, 'y', 'y', 'y', 'n', 'y', 'n', '1', 'y', '0000-00-00 00:00:00', '2018-01-03 16:51:30');
INSERT INTO `te_msmenu` VALUES ('134', null, 'Subcategory', 'subcategory-product', '7', 'n', null, 'y', 'y', 'y', 'n', 'y', 'n', '2', 'y', '0000-00-00 00:00:00', '2018-01-03 16:52:36');

-- ----------------------------
-- Table structure for `te_operator`
-- ----------------------------
DROP TABLE IF EXISTS `te_operator`;
CREATE TABLE `te_operator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_name` varchar(100) DEFAULT NULL,
  `no_prefix` text,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` char(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_operator
-- ----------------------------
INSERT INTO `te_operator` VALUES ('1', 'XL Axiata', ';0817;0818;0819;0859;0877;0878;', null, '2018-01-03 11:43:02', 'y');
INSERT INTO `te_operator` VALUES ('2', 'Telkomsel', '\r\n;0811;0812;0813;0821;0822;0823;0852;0853;0851;', '0000-00-00 00:00:00', '2018-01-03 11:43:03', 'n');
INSERT INTO `te_operator` VALUES ('3', 'Indosat', ';0855;0856;0857;0858;0814;0815;0816;', '0000-00-00 00:00:00', '2018-01-03 11:43:04', 'n');
INSERT INTO `te_operator` VALUES ('4', 'Axis', '\r\n0838;0831;0832;0833;', '0000-00-00 00:00:00', '2018-01-09 09:46:23', 'y');

-- ----------------------------
-- Table structure for `te_orderdt`
-- ----------------------------
DROP TABLE IF EXISTS `te_orderdt`;
CREATE TABLE `te_orderdt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderhd_id` int(15) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `price` int(20) DEFAULT NULL,
  `images` varchar(150) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT 'Type -> "Product" / "Package" menandakan dia dari table apa',
  `barcode` varchar(100) DEFAULT NULL,
  `total_item` int(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subcategory_product_id` int(15) DEFAULT NULL,
  `no_dompul` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_orderdt
-- ----------------------------
INSERT INTO `te_orderdt` VALUES ('1', '3', '1', 'DP5', 'Dompul 5000', '5000', null, 'product', null, '8', '2018-02-01 15:54:58', '2018-02-01 15:54:58', '1', null);
INSERT INTO `te_orderdt` VALUES ('2', '4', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 15:58:51', '2018-02-01 15:58:51', '2', null);
INSERT INTO `te_orderdt` VALUES ('3', '5', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:03:13', '2018-02-01 16:03:13', '2', null);
INSERT INTO `te_orderdt` VALUES ('4', '6', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:06:12', '2018-02-01 16:06:12', '2', null);
INSERT INTO `te_orderdt` VALUES ('5', '7', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:07:55', '2018-02-01 16:07:55', '2', null);
INSERT INTO `te_orderdt` VALUES ('6', '8', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:09:07', '2018-02-01 16:09:07', '2', null);
INSERT INTO `te_orderdt` VALUES ('7', '9', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:10:28', '2018-02-01 16:10:28', '2', null);
INSERT INTO `te_orderdt` VALUES ('8', '10', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-01 16:12:21', '2018-02-01 16:12:21', '2', null);
INSERT INTO `te_orderdt` VALUES ('9', '11', '2', 'DP10', 'Dompul 10000', '10000', null, 'product', '', '5', '2018-02-01 16:24:40', '2018-02-01 16:24:40', '1', null);
INSERT INTO `te_orderdt` VALUES ('10', '12', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '50000', '2018-02-02 11:34:25', '2018-02-02 11:34:25', '2', null);
INSERT INTO `te_orderdt` VALUES ('11', '13', '1', 'DP5', 'Dompul 5000', '5000', null, 'product', null, '5', '2018-02-02 11:38:01', '2018-02-02 11:38:01', '1', null);
INSERT INTO `te_orderdt` VALUES ('12', '14', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '100000', '2018-02-02 11:45:30', '2018-02-02 11:45:30', '2', null);
INSERT INTO `te_orderdt` VALUES ('13', '17', '1', 'SP0KREGOR-JKT HU Besar', 'HU Besar', '9500', null, 'package', '665544123', '1', '2018-02-02 15:09:00', '2018-02-02 15:09:00', '0', null);
INSERT INTO `te_orderdt` VALUES ('14', '18', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'product', '', '200000', '2018-02-05 10:29:31', '2018-02-05 10:29:31', '2', null);
INSERT INTO `te_orderdt` VALUES ('15', '19', '9', 'SP0KREGOR-JKT HU Kecil', 'HU Kecil', '9500', null, 'package', '445566132', '1', '2018-02-05 10:32:23', '2018-02-05 10:32:23', '0', null);
INSERT INTO `te_orderdt` VALUES ('16', '20', '9', 'SP0KREGOR-JKT HU Kecil', 'HU Kecil', '9500', null, 'package', '445566132', '1', '2018-02-05 10:37:58', '2018-02-05 10:37:58', null, null);
INSERT INTO `te_orderdt` VALUES ('17', '21', '9', 'SP0KREGOR-JKT HU Kecil', 'HU Kecil', '9500', null, 'package', '445566132', '1', '2018-02-05 10:42:26', '2018-02-05 10:42:26', null, null);
INSERT INTO `te_orderdt` VALUES ('18', '22', '1', '0814141414', 'KP XL 0K LTE 2', null, null, 'product', '99887744', '10000', '2018-02-09 14:29:46', '2018-02-09 14:29:46', '6', null);
INSERT INTO `te_orderdt` VALUES ('19', '22', '1', 'SP0KREGOR-JKT HU Besar', 'HU Besar', '19000', null, 'package', '665544123', '2', '2018-02-09 14:29:46', '2018-02-09 14:29:46', null, null);
INSERT INTO `te_orderdt` VALUES ('20', '23', '1', '0888888888', 'Kartu Perdana', null, '3', 'product', '741741741', '1000000', '2018-02-14 11:56:52', '2018-02-14 11:56:52', '9', null);
INSERT INTO `te_orderdt` VALUES ('21', '24', '9', '0888888888', 'Kartu Perdana', '1000000', null, 'product', '741741741', '1', '2018-02-14 12:01:58', '2018-02-14 12:01:58', '3', null);
INSERT INTO `te_orderdt` VALUES ('22', '25', '9', '0888888888', 'Kartu Perdana', '1000000', null, 'product', '741741741', '1', '2018-02-14 12:13:41', '2018-02-14 12:13:41', '3', null);
INSERT INTO `te_orderdt` VALUES ('23', '25', null, 'SP0KREGOR-JKT HU Kecil', 'HU kecil', '2', '19000', 'package', '112233', '6', '2018-02-14 12:13:41', '2018-02-14 12:13:41', null, null);
INSERT INTO `te_orderdt` VALUES ('24', '26', '9', '0888888888', 'Kartu Perdana', '1000000', null, 'product', '741741741', '1', '2018-02-14 12:21:31', '2018-02-14 12:21:31', '3', null);
INSERT INTO `te_orderdt` VALUES ('25', '26', '6', 'SP0KREGOR-JKT HU Kecil', 'HU kecil', '0', null, 'package', '112233', '0', '2018-02-14 12:21:31', '2018-02-14 12:21:31', null, null);
INSERT INTO `te_orderdt` VALUES ('26', '27', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 16:15:35', '2018-02-14 16:15:35', '1', null);
INSERT INTO `te_orderdt` VALUES ('27', '27', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 16:15:35', '2018-02-14 16:15:35', '1', null);
INSERT INTO `te_orderdt` VALUES ('28', '27', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 16:15:35', '2018-02-14 16:15:35', '2', null);
INSERT INTO `te_orderdt` VALUES ('29', '28', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('30', '28', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('31', '28', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '2', null);
INSERT INTO `te_orderdt` VALUES ('32', '28', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('33', '28', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('34', '28', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '2', null);
INSERT INTO `te_orderdt` VALUES ('35', '28', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('36', '28', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '1', null);
INSERT INTO `te_orderdt` VALUES ('37', '28', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:09:21', '2018-02-14 17:09:21', '2', null);
INSERT INTO `te_orderdt` VALUES ('38', '29', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('39', '29', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('40', '29', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '2', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('41', '29', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('42', '29', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('43', '29', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '2', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('44', '29', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '190', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('45', '29', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '200', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '1', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('46', '29', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '525000', '2018-02-14 17:28:41', '2018-02-14 17:28:41', '2', '081238jasjhwqdl');
INSERT INTO `te_orderdt` VALUES ('47', '34', '9', 'SP0KREGOR-JKT HU Kecil', 'HU Kecil', '19000', null, 'package', '445566132', '2', '2018-02-17 14:17:34', '2018-02-17 14:17:34', null, null);
INSERT INTO `te_orderdt` VALUES ('48', '34', '1', 'DP5', 'Dompul 5000', '5000', null, 'dompul', null, '4', '2018-02-17 14:17:34', '2018-02-17 14:17:34', '1', '0852365412');
INSERT INTO `te_orderdt` VALUES ('49', '34', '2', 'DP10', 'Dompul 10000', '10000', null, 'dompul', '', '10', '2018-02-17 14:17:34', '2018-02-17 14:17:34', '1', '0852365412');
INSERT INTO `te_orderdt` VALUES ('50', '34', '3', 'DOMPUL REGULER', 'DOMPUL REGULER', '1', null, 'dompul', '', '10000', '2018-02-17 14:17:34', '2018-02-17 14:17:34', '2', '0852365412');

-- ----------------------------
-- Table structure for `te_orderhd`
-- ----------------------------
DROP TABLE IF EXISTS `te_orderhd`;
CREATE TABLE `te_orderhd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` text,
  `email` varchar(50) DEFAULT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL COMMENT 'customer_id',
  `user_id` int(11) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT NULL,
  `subtotal` int(20) DEFAULT NULL,
  `discount` int(20) DEFAULT NULL,
  `total_price` int(20) DEFAULT NULL,
  `total_item` int(15) DEFAULT NULL,
  `promo_id` int(15) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_orderhd
-- ----------------------------
INSERT INTO `te_orderhd` VALUES ('1', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800001', '49', '50', '2018-02-01 15:47:12', '40000', '0', '40000', '1', '0', 'Dompul', '2018-02-01 15:47:12', '2018-02-01 15:47:12');
INSERT INTO `te_orderhd` VALUES ('2', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800002', '49', '50', '2018-02-01 15:51:05', '40000', '0', '40000', '1', '0', 'Dompul', '2018-02-01 15:51:05', '2018-02-01 15:51:05');
INSERT INTO `te_orderhd` VALUES ('3', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800003', '49', '50', '2018-02-01 15:54:58', '40000', '0', '40000', '1', '0', 'Dompul', '2018-02-01 15:54:58', '2018-02-01 15:54:58');
INSERT INTO `te_orderhd` VALUES ('4', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800004', '49', '50', '2018-02-01 15:58:51', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 15:58:51', '2018-02-01 15:58:51');
INSERT INTO `te_orderhd` VALUES ('5', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800005', '49', '50', '2018-02-01 16:03:12', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:03:12', '2018-02-01 16:03:12');
INSERT INTO `te_orderhd` VALUES ('6', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800006', '49', '50', '2018-02-01 16:06:12', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:06:12', '2018-02-01 16:06:12');
INSERT INTO `te_orderhd` VALUES ('7', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800007', '49', '50', '2018-02-01 16:07:54', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:07:54', '2018-02-01 16:07:54');
INSERT INTO `te_orderhd` VALUES ('8', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800008', '49', '50', '2018-02-01 16:09:06', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:09:06', '2018-02-01 16:09:06');
INSERT INTO `te_orderhd` VALUES ('9', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800009', '49', '50', '2018-02-01 16:10:28', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:10:28', '2018-02-01 16:10:28');
INSERT INTO `te_orderhd` VALUES ('10', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800010', '49', '50', '2018-02-01 16:12:21', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-01 16:12:21', '2018-02-01 16:12:21');
INSERT INTO `te_orderhd` VALUES ('11', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '01021800011', '49', '50', '2018-02-01 16:24:39', '50000', '0', '50000', '1', '0', 'Dompul', '2018-02-01 16:24:39', '2018-02-02 08:09:50');
INSERT INTO `te_orderhd` VALUES ('12', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '02021800001', '49', '50', '2018-02-02 11:34:25', '50000', '750', '49250', '1', '0', 'Dompul', '2018-02-02 11:34:25', '2018-02-02 08:09:52');
INSERT INTO `te_orderhd` VALUES ('13', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '02021800002', '49', '50', '2018-02-02 11:38:01', '25000', '0', '25000', '1', '0', 'Dompul', '2018-02-02 11:38:01', '2018-02-02 08:09:54');
INSERT INTO `te_orderhd` VALUES ('14', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '02021800003', '49', '50', '2018-02-02 11:45:29', '100000', '1500', '98500', '1', '0', 'Dompul', '2018-02-02 11:45:29', '2018-02-02 08:09:55');
INSERT INTO `te_orderhd` VALUES ('17', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '02021800004', '49', '50', '2018-02-02 15:09:00', '9500', '0', '9500', '1', '0', 'KP', '2018-02-02 15:09:00', '2018-02-02 15:09:00');
INSERT INTO `te_orderhd` VALUES ('18', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '05021800001', '49', '50', '2018-02-05 10:29:31', '200000', '3000', '197000', '1', '0', 'Dompul', '2018-02-05 10:29:31', '2018-02-05 10:29:31');
INSERT INTO `te_orderhd` VALUES ('19', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '05021800002', '49', '50', '2018-02-05 10:32:23', '9500', '0', '9500', '1', '0', 'KP', '2018-02-05 10:32:23', '2018-02-05 10:32:23');
INSERT INTO `te_orderhd` VALUES ('20', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '05021800003', '49', '50', '2018-02-05 10:37:58', '9500', '0', '9500', '1', '0', 'KP', '2018-02-05 10:37:58', '2018-02-05 10:37:58');
INSERT INTO `te_orderhd` VALUES ('21', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '05021800004', '49', '50', '2018-02-05 10:42:26', '9500', '0', '9500', '1', '0', 'KP', '2018-02-05 10:42:26', '2018-02-05 10:42:26');
INSERT INTO `te_orderhd` VALUES ('22', '99 Cell', '127', 'Jl. Pluit Karang Cantik No. 6', 'elimsuhendra@gmail.com', '09021800001', '50', '50', '2018-02-09 14:29:46', '29000', '0', '29000', '3', '0', 'KP', '2018-02-09 14:29:46', '2018-02-09 14:29:46');
INSERT INTO `te_orderhd` VALUES ('23', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800001', '49', '50', '2018-02-14 11:56:52', '1000000', '0', '1000000', '1', '0', 'KP', '2018-02-14 11:56:52', '2018-02-14 11:56:52');
INSERT INTO `te_orderhd` VALUES ('24', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800001', '49', '50', '2018-02-14 12:01:57', '1000000', '0', '1000000', '1', '0', 'KP', '2018-02-14 12:01:57', '2018-02-14 12:01:57');
INSERT INTO `te_orderhd` VALUES ('25', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800001', '49', '50', '2018-02-14 12:13:41', '1019000', '0', '1019000', '3', '0', 'KP', '2018-02-14 12:13:41', '2018-02-14 12:13:41');
INSERT INTO `te_orderhd` VALUES ('26', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800001', '49', '50', '2018-02-14 12:21:31', '1000000', '0', '1000000', '1', '0', 'KP', '2018-02-14 12:21:31', '2018-02-14 12:21:31');
INSERT INTO `te_orderhd` VALUES ('27', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800005', '49', '50', '2018-02-14 16:15:34', '0', '0', '0', '0', '0', 'KP', '2018-02-14 16:15:34', '2018-02-14 16:15:34');
INSERT INTO `te_orderhd` VALUES ('28', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800006', '49', '50', '2018-02-14 17:09:21', '0', '0', '0', '0', '0', 'KP', '2018-02-14 17:09:21', '2018-02-14 17:09:21');
INSERT INTO `te_orderhd` VALUES ('29', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '14021800007', '49', '50', '2018-02-14 17:28:41', '0', '0', '0', '0', '0', 'KP', '2018-02-14 17:28:41', '2018-02-14 17:28:41');
INSERT INTO `te_orderhd` VALUES ('30', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '15021800008', '49', '50', '2018-02-15 11:12:08', '6934250', '15750', '6934250', '6', '0', 'KP', '2018-02-15 11:12:08', '2018-02-15 11:12:08');
INSERT INTO `te_orderhd` VALUES ('31', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '17021800001', '49', '50', '2018-02-17 13:28:53', '148850', '150', '148850', '5', '0', 'KP', '2018-02-17 13:28:53', '2018-02-17 13:28:53');
INSERT INTO `te_orderhd` VALUES ('32', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '17021800002', '49', '50', '2018-02-17 14:05:31', '148850', '150', '148850', '5', '0', 'KP', '2018-02-17 14:05:31', '2018-02-17 14:05:31');
INSERT INTO `te_orderhd` VALUES ('33', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '17021800003', '49', '50', '2018-02-17 14:10:05', '148850', '150', '148850', '5', '0', 'KP', '2018-02-17 14:10:05', '2018-02-17 14:10:05');
INSERT INTO `te_orderhd` VALUES ('34', 'muhamadsaputra', '2', 'OPERATOR NITROGEN', 'saputra@gmail.com', '17021800004', '49', '50', '2018-02-17 14:17:34', '148850', '150', '148850', '5', '0', 'KP', '2018-02-17 14:17:34', '2018-02-17 14:17:34');

-- ----------------------------
-- Table structure for `te_outlet`
-- ----------------------------
DROP TABLE IF EXISTS `te_outlet`;
CREATE TABLE `te_outlet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_name` varchar(100) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `address` text NOT NULL,
  `province_id` varchar(100) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `outlet_pusat` int(11) DEFAULT '0',
  `province_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_outlet
-- ----------------------------
INSERT INTO `te_outlet` VALUES ('1', 'Outlet Pusat', 'MVyKNG_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg', '000000000000', 'y', 'Jalan Oliander', 'Jakarta Barat', '1', '0000-00-00 00:00:00', '2018-01-02 17:12:12', '0', null);
INSERT INTO `te_outlet` VALUES ('17', 'Jakarta Cell', 'jsHkBS_800px-Pizigani_1367_Chart_10MB.jpg', null, 'y', 'Jl. xyz', '11', '1', '2018-01-02 16:34:36', '2018-01-02 17:15:40', '1', 'DKI Jakarta');
INSERT INTO `te_outlet` VALUES ('18', 'Jakarta Cell 2', 'wNEuBK_download.png', null, 'y', 'jl. abc', '11', '1', '2018-01-02 17:15:17', '2018-01-02 17:15:17', '1', 'DKI Jakarta');

-- ----------------------------
-- Table structure for `te_package`
-- ----------------------------
DROP TABLE IF EXISTS `te_package`;
CREATE TABLE `te_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(100) DEFAULT NULL,
  `package_id` varchar(100) DEFAULT NULL,
  `parent_status` char(2) DEFAULT NULL COMMENT 'y: true ; n: false',
  `description` text,
  `status` varchar(2) DEFAULT NULL,
  `upd_by` varchar(100) DEFAULT NULL,
  `percent` float(11,0) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `valid_date` date DEFAULT NULL,
  `no_box` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `level` int(11) DEFAULT NULL COMMENT 'level 0 = master parent, level 1 = level paling kecil',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_package
-- ----------------------------
INSERT INTO `te_package` VALUES ('1', 'SP0KREGOR-JKT HU Besar', '0', 'y', 'HU Besar', 'y', '1', '5', null, '665544123', '2019-02-28', '1', '50', '2018-01-25 11:51:37', '2018-02-02 07:43:19', '0');
INSERT INTO `te_package` VALUES ('6', 'SP0KREGOR-JKT HU Kecil', '1', 'n', 'HU kecil', 'y', '1', '5', null, '112233', '2019-02-28', '1', '1', '2018-01-25 15:45:00', '2018-02-05 05:05:14', '1');
INSERT INTO `te_package` VALUES ('9', 'SP0KREGOR-JKT HU Kecil', '0', 'n', 'HU Kecil', 'y', '1', '5', null, '445566132', '2019-02-28', '2', '1', '2018-01-26 14:59:17', '2018-02-05 05:06:45', '0');

-- ----------------------------
-- Table structure for `te_package_product`
-- ----------------------------
DROP TABLE IF EXISTS `te_package_product`;
CREATE TABLE `te_package_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` varchar(100) DEFAULT NULL,
  `product_id` longtext COMMENT 'pcs, psi',
  `status` varchar(2) DEFAULT NULL,
  `upd_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_package_product
-- ----------------------------
INSERT INTO `te_package_product` VALUES ('14', '6', '4', 'y', '1', '2018-01-26 17:04:02', '2018-02-14 00:28:32');
INSERT INTO `te_package_product` VALUES ('15', '6', '5', 'y', '1', '2018-01-26 17:04:05', '2018-02-14 00:28:38');
INSERT INTO `te_package_product` VALUES ('16', '9', '6', 'n', '1', '2018-01-26 17:04:07', '2018-02-17 00:17:34');
INSERT INTO `te_package_product` VALUES ('17', '9', '7', 'n', '1', '2018-01-27 17:07:52', '2018-02-17 00:17:34');

-- ----------------------------
-- Table structure for `te_product`
-- ----------------------------
DROP TABLE IF EXISTS `te_product`;
CREATE TABLE `te_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `description` text,
  `status` varchar(2) DEFAULT NULL,
  `price` text,
  `images` varchar(100) DEFAULT NULL,
  `barcode` text,
  `valid_date` date DEFAULT NULL,
  `category_product_id` varchar(255) DEFAULT NULL,
  `subcategory_product_id` varchar(255) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  `discount_dompul_reguler` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_product
-- ----------------------------
INSERT INTO `te_product` VALUES ('1', 'DP5', null, 'Dompul 5000', 'y', '5000', null, null, '2019-01-31', null, '1', '1', '2018-01-24 16:20:03', '2018-02-09 14:39:01', '1', null);
INSERT INTO `te_product` VALUES ('2', 'DP10', null, 'Dompul 10000', 'y', '10000', null, '', '2019-01-31', null, '1', '1', '2018-01-24 16:44:52', '2018-01-27 15:11:18', '1', null);
INSERT INTO `te_product` VALUES ('3', 'DOMPUL REGULER', null, 'DOMPUL REGULER', 'y', '1', null, '', '2019-01-31', null, '2', '1', '2018-01-24 16:47:16', '2018-02-13 00:39:19', '1', '1.5');
INSERT INTO `te_product` VALUES ('4', '0812121212', '11', 'KP Reguler', 'y', '10000', null, '111333222', '2019-01-31', null, '3', '2', '2018-01-24 16:50:11', '2018-01-25 11:06:38', '1', null);
INSERT INTO `te_product` VALUES ('5', '08131313', '11', 'Dompul reguler', 'y', '10000', null, '132654987', '2019-01-31', null, '3', '2', '2018-01-24 16:51:26', '2018-01-27 15:58:37', '1', null);
INSERT INTO `te_product` VALUES ('6', '0814141414', '11', 'KP XL 0K LTE 2', 'y', '10000', null, '99887744', '2019-01-31', null, '7', '2', '2018-01-24 16:53:00', '2018-01-24 16:53:00', '1', null);
INSERT INTO `te_product` VALUES ('7', '08554466', '16', 'KP Reguler XL', 'y', '10000', null, '12345678922', '2018-01-31', null, '3', '2', '2018-01-25 10:29:32', '2018-01-27 15:58:37', '1', null);
INSERT INTO `te_product` VALUES ('8', '081745645644', '11', 'HU Kecil', 'y', '10000', null, '445566454654', '2019-01-31', null, '3', '2', '2018-01-25 13:29:47', '2018-01-27 15:58:39', '1', null);
INSERT INTO `te_product` VALUES ('9', '0888888888', '11', 'Kartu Perdana', 'y', '1000000', null, '741741741', '2018-02-05', null, '3', '2', null, '2018-02-05 04:55:25', null, null);
INSERT INTO `te_product` VALUES ('10', 'DP20', null, 'Dompet Reguler 20.000', 'y', '20000', null, '', '2018-02-09', null, '1', '1', '2018-02-09 14:49:47', '2018-02-09 14:49:47', '1', null);

-- ----------------------------
-- Table structure for `te_product_package_adjustment`
-- ----------------------------
DROP TABLE IF EXISTS `te_product_package_adjustment`;
CREATE TABLE `te_product_package_adjustment` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `product_id` int(50) DEFAULT NULL,
  `package_big_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `outlet_stock_total` int(11) DEFAULT NULL,
  `global_stock_total` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL COMMENT 'wto;sales;adjust',
  `relation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_product_package_adjustment
-- ----------------------------
INSERT INTO `te_product_package_adjustment` VALUES ('1', '49', null, '1', null, '1000', '1000', '1000', '2018-01-24 16:20:03', '2018-02-13 02:53:18', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('2', '49', null, '2', null, '900', '900', '900', '2018-01-24 16:44:52', '2018-02-01 03:31:25', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('3', '49', null, '3', null, '10000000', '10000000', '10000000', '2018-01-24 16:47:16', '2018-02-14 03:07:15', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('4', '49', '6', '4', '1', '1', '1', '1', '2018-01-24 16:50:11', '2018-02-02 05:02:16', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('5', '49', '6', '5', '1', '1', '1', '1', '2018-01-24 16:51:26', '2018-02-05 04:51:34', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('6', '49', '9', '6', null, '1', '1', '1', '2018-01-24 16:53:00', '2018-02-02 05:02:30', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('7', '49', '9', '7', null, '1', '1', '1', '2018-01-25 10:29:32', '2018-02-05 04:48:05', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('10', '1', null, '8', null, '1', '1', '1', '2018-01-25 13:29:47', '2018-01-26 17:05:59', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('11', '49', null, '9', null, '1', '1', '1', '2018-01-27 15:08:32', '2018-02-01 06:56:41', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('12', '49', null, '9', null, '1', '1', '1', '2018-02-01 14:48:10', '2018-02-13 23:44:18', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('13', '1', null, '10', null, '0', '0', '1000000', '2018-02-09 14:49:47', '2018-02-09 14:49:47', '1', 'add_product', null);
INSERT INTO `te_product_package_adjustment` VALUES ('22', '49', null, '1', null, '-190', '810', '810', '2018-02-14 16:15:37', '2018-02-13 02:53:18', null, 'Order', '27');
INSERT INTO `te_product_package_adjustment` VALUES ('23', '49', null, '2', null, '-200', '700', '700', '2018-02-14 16:15:37', '2018-02-01 03:31:25', null, 'Order', '27');
INSERT INTO `te_product_package_adjustment` VALUES ('24', '49', null, '3', null, '-525000', '9475000', '9475000', '2018-02-14 16:15:37', '2018-02-14 03:07:23', null, 'Order', '27');
INSERT INTO `te_product_package_adjustment` VALUES ('43', '49', null, '1', null, '-190', '620', '620', '2018-02-15 11:12:10', '2018-02-13 02:53:18', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('44', '49', null, '1', null, '-190', '430', '620', '2018-02-15 11:12:10', '2018-02-13 02:53:18', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('45', '49', null, '2', null, '-200', '500', '500', '2018-02-15 11:12:10', '2018-02-01 03:31:25', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('46', '49', null, '2', null, '-200', '300', '500', '2018-02-15 11:12:10', '2018-02-01 03:31:25', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('47', '49', null, '3', null, '-525000', '8950000', '8950000', '2018-02-15 11:12:10', '2018-02-14 03:07:23', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('48', '49', null, '3', null, '-525000', '8425000', '8950000', '2018-02-15 11:12:10', '2018-02-14 03:07:23', null, 'Order', '30');
INSERT INTO `te_product_package_adjustment` VALUES ('67', '49', null, '9', null, '-2', '-1', '-1', '2018-02-17 14:17:34', '2018-02-13 23:44:18', null, 'Order', '34');
INSERT INTO `te_product_package_adjustment` VALUES ('68', '49', null, '1', null, '-4', '426', '616', '2018-02-17 14:17:34', '2018-02-13 02:53:18', null, 'Order', '34');
INSERT INTO `te_product_package_adjustment` VALUES ('69', '49', null, '2', null, '-10', '290', '490', '2018-02-17 14:17:34', '2018-02-01 03:31:25', null, 'Order', '34');
INSERT INTO `te_product_package_adjustment` VALUES ('70', '49', null, '3', null, '-10000', '8415000', '8940000', '2018-02-17 14:17:34', '2018-02-14 03:07:23', null, 'Order', '34');
INSERT INTO `te_product_package_adjustment` VALUES ('71', '49', '9', '6', null, '-1', '0', '0', '2018-02-17 14:17:34', '2018-02-02 05:02:30', null, 'Order', '34');
INSERT INTO `te_product_package_adjustment` VALUES ('72', '49', '9', '7', null, '-1', '0', '0', '2018-02-17 14:17:34', '2018-02-05 04:48:05', null, 'Order', '34');

-- ----------------------------
-- Table structure for `te_product_type`
-- ----------------------------
DROP TABLE IF EXISTS `te_product_type`;
CREATE TABLE `te_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_name` varchar(100) DEFAULT NULL COMMENT 'Quantity: Barang Kuantity; Serial: Barang Satuan',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` char(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_product_type
-- ----------------------------
INSERT INTO `te_product_type` VALUES ('1', 'Quantity', '0000-00-00 00:00:00', '2018-01-04 15:13:34', 'y');
INSERT INTO `te_product_type` VALUES ('2', 'Serial', '0000-00-00 00:00:00', '2018-01-04 15:14:17', 'y');

-- ----------------------------
-- Table structure for `te_promo`
-- ----------------------------
DROP TABLE IF EXISTS `te_promo`;
CREATE TABLE `te_promo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `promo_name` varchar(100) DEFAULT NULL,
  `promo_type` varchar(100) DEFAULT NULL COMMENT '{promo, voucher}',
  `description` text,
  `voucher_code` text,
  `discount_type` varchar(1) DEFAULT NULL,
  `discount_value` int(10) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `start_date` date DEFAULT '0000-00-00',
  `end_date` date DEFAULT '0000-00-00',
  `upd_by` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_promo
-- ----------------------------
INSERT INTO `te_promo` VALUES ('1', 'Test', 'Voucher', 'Test', 'tau741', 'p', '20', 'y', '2018-01-01', '2018-06-30', '1', '2018-01-19 03:07:07', '2018-01-23 05:50:47');
INSERT INTO `te_promo` VALUES ('2', 'Test', 'Voucher', 'Test', 'anjing', 'v', '5000', 'y', '2018-01-01', '2018-06-30', null, '2018-01-22 09:41:06', '2018-01-23 05:51:02');

-- ----------------------------
-- Table structure for `te_province`
-- ----------------------------
DROP TABLE IF EXISTS `te_province`;
CREATE TABLE `te_province` (
  `id` int(10) unsigned NOT NULL COMMENT 'id entry',
  `number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL COMMENT 'waktu update terakhir',
  `deleted_at` int(10) unsigned DEFAULT NULL COMMENT 'waktu dihapus',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL COMMENT 'id negara',
  `created_at` int(10) unsigned DEFAULT NULL COMMENT 'waktu insert',
  PRIMARY KEY (`id`),
  KEY `index2` (`country_id`) USING BTREE,
  KEY `index4` (`number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='provinsi';

-- ----------------------------
-- Records of te_province
-- ----------------------------
INSERT INTO `te_province` VALUES ('1', '11', null, null, 'Aceh', '1', null);
INSERT INTO `te_province` VALUES ('2', '12', null, null, 'Sumatera Utara', '1', null);
INSERT INTO `te_province` VALUES ('3', '13', null, null, 'Sumatera Barat', '1', null);
INSERT INTO `te_province` VALUES ('4', '14', null, null, 'Riau', '1', null);
INSERT INTO `te_province` VALUES ('5', '15', null, null, 'Jambi', '1', null);
INSERT INTO `te_province` VALUES ('6', '16', null, null, 'Sumatera Selatan', '1', null);
INSERT INTO `te_province` VALUES ('7', '17', null, null, 'Bengkulu', '1', null);
INSERT INTO `te_province` VALUES ('8', '18', null, null, 'Lampung', '1', null);
INSERT INTO `te_province` VALUES ('9', '19', null, null, 'Kepulauan Bangka Belitung', '1', null);
INSERT INTO `te_province` VALUES ('10', '21', null, null, 'Kepulauan Riau', '1', null);
INSERT INTO `te_province` VALUES ('11', '31', null, null, 'DKI Jakarta', '1', null);
INSERT INTO `te_province` VALUES ('12', '32', null, null, 'Jawa Barat', '1', null);
INSERT INTO `te_province` VALUES ('13', '33', null, null, 'Jawa Tengah', '1', null);
INSERT INTO `te_province` VALUES ('14', '34', null, null, 'DI Yogyakarta', '1', null);
INSERT INTO `te_province` VALUES ('15', '35', null, null, 'Jawa Timur', '1', null);
INSERT INTO `te_province` VALUES ('16', '36', null, null, 'Banten', '1', null);
INSERT INTO `te_province` VALUES ('17', '51', null, null, 'Bali', '1', null);
INSERT INTO `te_province` VALUES ('18', '52', null, null, 'Nusa Tenggara Barat', '1', null);
INSERT INTO `te_province` VALUES ('19', '53', null, null, 'Nusa Tenggara Timur', '1', null);
INSERT INTO `te_province` VALUES ('20', '61', null, null, 'Kalimantan Barat', '1', null);
INSERT INTO `te_province` VALUES ('21', '62', null, null, 'Kalimantan Tengah', '1', null);
INSERT INTO `te_province` VALUES ('22', '63', null, null, 'Kalimantan Selatan', '1', null);
INSERT INTO `te_province` VALUES ('23', '64', null, null, 'Kalimantan Timur', '1', null);
INSERT INTO `te_province` VALUES ('24', '65', null, null, 'Kalimantan Utara', '1', null);
INSERT INTO `te_province` VALUES ('25', '71', null, null, 'Sulawesi Utara', '1', null);
INSERT INTO `te_province` VALUES ('26', '72', null, null, 'Sulawesi Tengah', '1', null);
INSERT INTO `te_province` VALUES ('27', '73', null, null, 'Sulawesi Selatan', '1', null);
INSERT INTO `te_province` VALUES ('28', '74', null, null, 'Sulawesi Tenggara', '1', null);
INSERT INTO `te_province` VALUES ('29', '75', null, null, 'Gorontalo', '1', null);
INSERT INTO `te_province` VALUES ('30', '76', null, null, 'Sulawesi Barat', '1', null);
INSERT INTO `te_province` VALUES ('31', '81', null, null, 'Maluku', '1', null);
INSERT INTO `te_province` VALUES ('32', '82', null, null, 'Maluku Utara', '1', null);
INSERT INTO `te_province` VALUES ('33', '91', null, null, 'Papua Barat', '1', null);
INSERT INTO `te_province` VALUES ('34', '92', null, null, 'Papua', '1', null);

-- ----------------------------
-- Table structure for `te_sales_target`
-- ----------------------------
DROP TABLE IF EXISTS `te_sales_target`;
CREATE TABLE `te_sales_target` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `target` text,
  `status` varchar(2) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_sales_target
-- ----------------------------
INSERT INTO `te_sales_target` VALUES ('1', '50', '2018-01-01 00:00:00', '2018-02-28 22:28:25', '120', 'y', '1', '2015-09-30 02:02:34', '2018-02-02 08:18:58');

-- ----------------------------
-- Table structure for `te_salesman_product`
-- ----------------------------
DROP TABLE IF EXISTS `te_salesman_product`;
CREATE TABLE `te_salesman_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `list_product` longtext,
  `list_package` longtext,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_salesman_product
-- ----------------------------
INSERT INTO `te_salesman_product` VALUES ('1', '50', ';;', ';;', '2018-01-04 10:01:19', '2018-02-28 10:01:31', '2018-01-05 10:01:19', '2018-02-17 00:17:34', '1');

-- ----------------------------
-- Table structure for `te_slideshow`
-- ----------------------------
DROP TABLE IF EXISTS `te_slideshow`;
CREATE TABLE `te_slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slideshow_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` char(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_slideshow
-- ----------------------------
INSERT INTO `te_slideshow` VALUES ('1', 'Slideshow 1', 'QD4CVU_Penguins.jpg', 'y', '2018-01-15 17:22:07', '2018-01-15 17:25:02', '1');
INSERT INTO `te_slideshow` VALUES ('2', 'Slideshow 2', 'rMrzW6_Jellyfish.jpg', 'y', '2018-01-15 17:22:04', '2018-01-15 17:25:02', '1');
INSERT INTO `te_slideshow` VALUES ('3', 'Slideshow 3', 'xupwyA_Chrysanthemum.jpg', 'y', '2018-01-15 17:22:00', '2018-01-15 17:25:02', '1');

-- ----------------------------
-- Table structure for `te_sub_district`
-- ----------------------------
DROP TABLE IF EXISTS `te_sub_district`;
CREATE TABLE `te_sub_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(10) DEFAULT NULL,
  `sub_district_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_sub_district
-- ----------------------------

-- ----------------------------
-- Table structure for `te_subcategory_product`
-- ----------------------------
DROP TABLE IF EXISTS `te_subcategory_product`;
CREATE TABLE `te_subcategory_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'level 0 = paling tingggi, level 1 = level tertinggi',
  `subcategory_product_name` varchar(100) DEFAULT NULL,
  `category_product_id` int(11) DEFAULT NULL,
  `description` text,
  `operator_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_subcategory_product
-- ----------------------------
INSERT INTO `te_subcategory_product` VALUES ('1', 'DOMPUL NOMINAL', '0', 'Dompul Nominal', null, '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-24 17:18:17', '1');
INSERT INTO `te_subcategory_product` VALUES ('2', 'DOMPET REGULER', '0', 'Dompet Reguler', null, '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-27 07:53:11', '1');
INSERT INTO `te_subcategory_product` VALUES ('3', 'REGULER XL', '0', '', '1', '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-27 07:53:09', '1');
INSERT INTO `te_subcategory_product` VALUES ('4', 'AKTIF INJ', null, null, '1', '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-27 07:53:07', '1');
INSERT INTO `te_subcategory_product` VALUES ('5', 'AKTIF REGULER', null, null, '1', '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-27 07:53:06', '1');
INSERT INTO `te_subcategory_product` VALUES ('6', 'AKTIF WHITELIST (WL)', '1', '', '1', '0', 'y', '0', '0000-00-00 00:00:00', '2018-01-27 07:53:04', '1');
INSERT INTO `te_subcategory_product` VALUES ('7', 'KP XL YOUTUBE 10K 1GB', '1', 'test', '1', '1', 'y', '2', '2018-01-04 11:29:09', '2018-01-27 07:53:03', '1');
INSERT INTO `te_subcategory_product` VALUES ('8', 'KP XL 0K LTE 2', '1', 'Kartu Perdana XL 2', '1', '1', 'y', '2', '2018-01-04 11:35:17', '2018-01-27 07:53:02', '1');
INSERT INTO `te_subcategory_product` VALUES ('9', 'KP XL 0K NEWPRE', '1', 'Kartu Perdana XL', '1', '1', 'y', '2', '2018-01-04 16:57:26', '2018-01-27 07:53:00', '1');
INSERT INTO `te_subcategory_product` VALUES ('10', 'KP XL 0K RP.1     ', '1', 'Kartu Perdana XL', '1', '1', 'y', '2', '2018-01-04 17:34:55', '2018-01-27 07:52:59', '1');
INSERT INTO `te_subcategory_product` VALUES ('11', 'KP XL 3K LTE 128', '1', 'test', '1', '1', 'y', '2', '2018-01-04 17:41:06', '2018-01-27 07:52:57', '1');
INSERT INTO `te_subcategory_product` VALUES ('12', 'KP XL 3K NEWPRE', '1', 'test', '1', '1', 'y', '2', '2018-01-05 10:43:35', '2018-01-27 07:52:55', '1');

-- ----------------------------
-- Table structure for `te_user`
-- ----------------------------
DROP TABLE IF EXISTS `te_user`;
CREATE TABLE `te_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL,
  `language_id` tinyint(2) DEFAULT '2',
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `birth_date` varchar(100) DEFAULT NULL,
  `last_activity` varchar(100) DEFAULT NULL,
  `description` text,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `login_app` varchar(1) DEFAULT NULL,
  `login_backend` varchar(1) DEFAULT NULL,
  `outlet_id` text,
  `token` text,
  `phone` varchar(100) DEFAULT NULL,
  `sales_cart` mediumtext,
  `sales_dompul` char(2) DEFAULT 'n' COMMENT 'y: sales dompul; n : not sales dompul',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_user
-- ----------------------------
INSERT INTO `te_user` VALUES ('1', 'admin', '1', '2', 'admin@admin.com', '$2y$10$pGWG4e/C/hjEKj2QRFDWxOLdcxvfE2PCWCTaV8tjhr0TZyyBMDqQ.', 'vincent', 'GhsSde_2-9670.jpg', '14-12-2017', null, 'cent', 'UqqP8EdVUDoZ3hsvVBIRm1p5yhFj6Eep7HJ5n74AkfwQFhomBP7N5sthpMbw', 'y', '1', '2015-09-30 02:02:34', '2018-02-14 11:07:53', null, null, null, null, null, null, 'n');
INSERT INTO `te_user` VALUES ('2', 'admin_yudi', '2', '2', 'op_nitro@gmail.com', '$2y$10$5pBEnbzy95nf4ZnU3P28ZOTZrHeYuDOcm/ptbhmVrRICdL72lZZe.', 'WAHYUDI ISKANDAR', '7scb4t_suzu.jpg', '14-12-2017', '2017-12-14 09:06:47', 'ADMIN NITROGEN', null, 'y', '1', '2017-10-02 10:37:50', '2018-01-30 15:04:37', 'y', 'n', '', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJvcF9uaXRybyIsInBhc3N3b3JkIjoiJDJ5JDEwJHJsY0pXZkFaTGNRUHh5MkpHZjlJR09uSnVpV2x1ckkuTGpaS2gvM3ljRXhld1cyc3V5SjdtIiwiaW1hZ2VzIjoiN3NjYjR0X3N1enUuanBnIiwibmFtZSI6Ik9wZXJhdG9yIDEiLCJzdGF0dXMiOiJ5IiwiaWF0IjoxNTEzMjE2NjU4LCJleHAiOjE1MjA5OTI2NTh9.uXnVK5NOvUD2-9Ysod5CdDqp5qCW6vCt4gWLwa2kUxA', null, null, 'n');
INSERT INTO `te_user` VALUES ('3', 'admin_indri', '2', '2', 'admin_nitro@gmail.com', '$2y$10$ewHWoFVun.VepA7KO/cwUe7w/YLQOm7./gX0RKVlm2r5zupPqQK12', 'INDRI QODTRUNADA', 'zPzrvQ_Koala.jpg', '14-12-2017', '2017-12-22 17:23:08', 'admin_nitro', null, 'y', '1', '0000-00-00 00:00:00', '2018-01-30 15:04:38', 'y', 'n', '', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJhZG1pbl9pbmRyaSIsInBhc3N3b3JkIjoiJDJ5JDEwJGV3SFdvRlZ1bi5WZXBBN0tPL2N3VWU3dy9ZTFFPbTcuL2dYMFJLVmxtMnI1enVwUHFRSzEyIiwiaW1hZ2VzIjoielB6cnZRX0tvYWxhLmpwZyIsIm5hbWUiOiJJTkRSSSBRT0RUUlVOQURBIiwic3RhdHVzIjoieSIsImlhdCI6MTUxMzkzODEwNywiZXhwIjoxNTIxNzE0MTA3fQ.wFjCyvWpepIXvm93mqjELj8H9oAxXSxweabTlg22DDM', null, null, 'n');
INSERT INTO `te_user` VALUES ('45', 'admin_abi', '2', '2', 'abi123@gmail.com', '$2y$10$/10Amb0bxc0Ei/.82lmU7eU9LT0d7XBTBr45YtENhNqLLTrorMmUm', 'ABI YUDHA WARDANA', null, '14-12-2017', null, 'ADMIN NITROGEN', null, 'y', '1', '0000-00-00 00:00:00', '2018-01-30 15:04:39', 'y', 'n', '', null, null, null, 'n');
INSERT INTO `te_user` VALUES ('46', 'admin_khotimi', '2', '2', 'op_keppo@gmail.com', '$2y$10$JyhjPktiroQ5.CuNvEsz3eqn8ZUIieakfyGLr6gqJR1ODo0pfy4Ru', 'AHMAD KHOTIMI', 'Hugauv_Hydrangeas.jpg', '14-12-2017', '2017-10-24 17:33:20', 'ADMIN NITROGEN', null, 'y', '1', '2017-10-23 15:52:25', '2018-01-30 15:04:39', 'y', 'n', '', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDYsInVzZXJuYW1lIjoib3Bfa2VwcG8iLCJwYXNzd29yZCI6IiQyeSQxMCRkN0FnT2N1RWVFdmp4ZGdPZWtJMTBPY1Juc3RneFU1OW1hUVBXdURoUkUvQkF1cTY0RzJBSyIsImltYWdlcyI6Ikh1Z2F1dl9IeWRyYW5nZWFzLmpwZyIsIm5hbWUiOiJvcF9LZXBwbyIsInN0YXR1cyI6InkiLCJpYXQiOjE1MDg4NDExOTAsImV4cCI6MTUxNjYxNzE5MH0.83A-3iIMksA24teJnzoIm8BP0WIoJsK2usFpQJuUcsU', null, null, 'n');
INSERT INTO `te_user` VALUES ('47', 'admin_daroni', '2', '2', 'daroni@gmail.com', '$2y$10$IRYrLM4t8h1EddoAwnQBNeDvMMOzmyHnZY80Ye5KQz0iLa61Ddcne', 'DARONI ', 'g9irrH_Koala.jpg', '14-12-2017', '2017-12-19 08:43:47', 'ADMIN NITROGEN', null, 'y', '1', '2017-10-24 14:06:40', '2018-01-30 15:04:40', 'y', 'y', '', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDcsInVzZXJuYW1lIjoiYWRtaW5fZGFyb25pIiwicGFzc3dvcmQiOiIkMnkkMTAkSVJZckxNNHQ4aDFFZGRvQXduUUJOZUR2TU1Pem15SG5aWTgwWWU1S1F6MGlMYTYxRGRjbmUiLCJpbWFnZXMiOiJnOWlyckhfS29hbGEuanBnIiwibmFtZSI6IkRBUk9OSSAiLCJzdGF0dXMiOiJ5IiwiaWF0IjoxNTEzNjQ3ODI3LCJleHAiOjE1MjE0MjM4Mjd9.T1HixnuvK21KCwizF4MpDMo5CFhePE6CMnFRYYWhjYQ', null, null, 'n');
INSERT INTO `te_user` VALUES ('48', 'dedesumardi', '3', '2', 'op_kampungsawah@gmail.com', '$2y$10$7nkGaZe1vAvPl28EtH54luAYrE3fd2uzjkdEkrZUOZXVjoUOI5iZe', 'DEDE SUMARDI', 'V04kad_Koala.jpg', '14-12-2017', '2017-12-22 13:58:57', 'operator untuk kampung sawah', null, 'y', '1', '2017-10-27 13:25:47', '2018-01-30 15:04:41', 'y', 'n', ';15;', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDgsInVzZXJuYW1lIjoiZGVkZXN1bWFyZGkiLCJwYXNzd29yZCI6IiQyeSQxMCQ3bmtHYVplMXZBdlBsMjhFdEg1NGx1QVlyRTNmZDJ1emprZEVrclpVT1pYVmpvVU9JNWlaZSIsImltYWdlcyI6IlYwNGthZF9Lb2FsYS5qcGciLCJuYW1lIjoiREVERSBTVU1BUkRJIiwic3RhdHVzIjoieSIsImlhdCI6MTUxMzgzNjQ2OSwiZXhwIjoxNTIxNjEyNDY5fQ.KzbptCKmT5LzrNv2hFlPI2RHtTBaTPXinTlV-d6PBeg', null, null, 'n');
INSERT INTO `te_user` VALUES ('49', 'muhamadsaputra', '3', '2', 'saputra@gmail.com', '$2y$10$YgKM0Y2yQbfp0TAGw6hnweV6EgmsGx9USyVG7Qjn4GVLkHge/BpZO', 'MUHAMAD SAPUTRA', 'nznHT4_PUTRA.jpg', '18-12-1997', '2017-12-26 06:21:12', 'OPERATOR NITROGEN', null, 'y', '1', '2017-12-14 15:34:31', '2018-01-30 15:04:42', 'y', 'n', ';15;', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDksInVzZXJuYW1lIjoibXVoYW1hZHNhcHV0cmEiLCJwYXNzd29yZCI6IiQyeSQxMCRZZ0tNMFkyeVFiZnAwVEFHdzZobndlVjZFZ21zR3g5VVN5Vkc3UWpuNEdWTGtIZ2UvQnBaTyIsImltYWdlcyI6Im56bkhUNF9QVVRSQS5qcGciLCJuYW1lIjoiTVVIQU1BRCBTQVBVVFJBIiwic3RhdHVzIjoieSIsImlhdCI6MTUxMzkzMzk1MCwiZXhwIjoxNTIxNzA5OTUwfQ.icXBaiuV4PGYijJBWQMnulCbdpwMHf3tr0dlt71u6FU', null, null, 'n');
INSERT INTO `te_user` VALUES ('50', 'sales01', '3', '2', 'christian@solveway.co.id', '$2a$10$8dwnSh0FHFdP7JvLavXQnO9HYd3RY30mTBSw6B.008QrtVp1ZtvEq', 'sales 01', 'teZZ1f_mataharimall.jpg', '02-01-2018', null, 'test', null, 'y', '1', '2018-01-02 13:22:26', '2018-02-17 00:42:52', 'y', 'n', ';5;', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NTAsInBhc3N3b3JkIjoiJDJhJDEwJDhkd25TaDBGSEZkUDdKdkxhdlhRbk85SFlkM1JZMzBtVEJTdzZCLjAwOFFydFZwMVp0dkVxIiwidXNlcm5hbWUiOiJzYWxlczAxIiwidXNlcl9hY2Nlc3NfaWQiOjMsImVtYWlsIjoiY2hyaXN0aWFuQHNvbHZld2F5LmNvLmlkIiwibmFtZSI6InNhbGVzIDAxIiwiaW1hZ2VzIjoidGVaWjFmX21hdGFoYXJpbWFsbC5qcGciLCJyb2xlIjoidXNlciIsImlhdCI6MTUxODUyNDUzMCwiZXhwIjoxNTI2MzAwNTMwfQ.EVu5YIMwK0HgSrfTy6va7vUD1fEulMgVQrhkyFj0-hY', null, '{\"product\":[],\"package\":[],\"dompul\":[{\"dompul_id\":1,\"no_dompul\":\"08521452369\",\"list_dompul\":[{\"id\":1,\"nominal\":\"5\"},{\"id\":2,\"nominal\":\"10\"},{\"id\":3,\"nominal\":\"2\"}]}]}', 'y');

-- ----------------------------
-- Table structure for `te_useraccess`
-- ----------------------------
DROP TABLE IF EXISTS `te_useraccess`;
CREATE TABLE `te_useraccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_useraccess
-- ----------------------------
INSERT INTO `te_useraccess` VALUES ('1', 'Super Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `te_useraccess` VALUES ('3', 'Sales', '2017-10-24 16:06:21', '2018-01-02 13:13:58');
INSERT INTO `te_useraccess` VALUES ('2', 'Admin', '2017-12-14 13:57:49', '2017-12-14 06:59:04');

-- ----------------------------
-- Table structure for `te_wto`
-- ----------------------------
DROP TABLE IF EXISTS `te_wto`;
CREATE TABLE `te_wto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionhd_id` int(11) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `detail` longtext,
  `type` varchar(20) DEFAULT NULL COMMENT 'type jasa: product_id relasi ke tabel service_product\r\ntype product: product_id relasi ke tabel product',
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of te_wto
-- ----------------------------
INSERT INTO `te_wto` VALUES ('1', '1', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `te_wto` VALUES ('2', '1', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `te_wto` VALUES ('3', '1', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `te_wto` VALUES ('4', '2', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `te_wto` VALUES ('5', '2', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `te_wto` VALUES ('6', '3', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":2},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `te_wto` VALUES ('7', '3', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `te_wto` VALUES ('8', '3', '17', 'Karet Cacing', '[]', 'Produk', '10000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `te_wto` VALUES ('9', '4', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 10:39:42', '2017-10-18 10:39:42');
INSERT INTO `te_wto` VALUES ('10', '5', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 10:42:27', '2017-10-18 10:42:27');
INSERT INTO `te_wto` VALUES ('11', '6', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 13:30:56', '2017-10-18 13:30:56');
INSERT INTO `te_wto` VALUES ('12', '6', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-18 13:30:56', '2017-10-18 13:30:56');
INSERT INTO `te_wto` VALUES ('13', '7', '16', 'karet cacing 2', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `te_wto` VALUES ('14', '7', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `te_wto` VALUES ('15', '7', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `te_wto` VALUES ('16', '8', '17', 'Karet Cacing', '[]', 'Produk', '10000', '4', '2017-10-19 09:36:09', '2017-10-19 09:36:09');
INSERT INTO `te_wto` VALUES ('17', '9', '17', 'Karet Cacing', '[]', 'Produk', '10000', '2', '2017-10-19 09:45:04', '2017-10-19 09:45:04');
INSERT INTO `te_wto` VALUES ('18', '10', '17', 'Karet Cacing', '[]', 'Produk', '10000', '5', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `te_wto` VALUES ('19', '10', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `te_wto` VALUES ('20', '10', '16', 'karet cacing 2', '[]', 'Produk', '10000', '1', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `te_wto` VALUES ('21', '11', '17', 'Karet Cacing', '[]', 'Produk', '10000', '6', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `te_wto` VALUES ('22', '12', '17', 'Karet Cacing', '[]', 'Produk', '10000', '4', '2017-10-19 09:47:18', '2017-10-19 09:47:18');
INSERT INTO `te_wto` VALUES ('23', '13', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:06:28', '2017-12-13 15:06:28');
INSERT INTO `te_wto` VALUES ('24', '13', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:06:28', '2017-12-13 15:06:28');
INSERT INTO `te_wto` VALUES ('25', '14', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:07:03', '2017-12-13 15:07:03');
INSERT INTO `te_wto` VALUES ('26', '14', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:07:03', '2017-12-13 15:07:03');
INSERT INTO `te_wto` VALUES ('27', '15', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:11:46', '2017-12-13 15:11:46');
INSERT INTO `te_wto` VALUES ('28', '15', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:11:46', '2017-12-13 15:11:46');
INSERT INTO `te_wto` VALUES ('29', '16', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:12:08', '2017-12-13 15:12:08');
INSERT INTO `te_wto` VALUES ('30', '16', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:12:08', '2017-12-13 15:12:08');
INSERT INTO `te_wto` VALUES ('31', '17', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:30:23', '2017-12-13 15:30:23');
INSERT INTO `te_wto` VALUES ('32', '18', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '16000', '1', '2017-12-13 15:34:02', '2017-12-13 15:34:02');
INSERT INTO `te_wto` VALUES ('33', '19', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:36:29', '2017-12-13 15:36:29');
INSERT INTO `te_wto` VALUES ('34', '20', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 15:37:44', '2017-12-13 15:37:44');
INSERT INTO `te_wto` VALUES ('35', '20', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 15:37:44', '2017-12-13 15:37:44');
INSERT INTO `te_wto` VALUES ('36', '21', '20', 'Tambal ban Mobil', '[]', 'Jasa', '16000', '1', '2017-12-13 15:40:04', '2017-12-13 15:40:04');
INSERT INTO `te_wto` VALUES ('37', '22', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-13 16:01:55', '2017-12-13 16:01:55');
INSERT INTO `te_wto` VALUES ('38', '22', '20', 'Tambal ban Mobil', '[]', 'Jasa', '16000', '1', '2017-12-13 16:01:55', '2017-12-13 16:01:55');
INSERT INTO `te_wto` VALUES ('39', '23', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:04:19', '2017-12-13 17:04:19');
INSERT INTO `te_wto` VALUES ('40', '24', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:07:07', '2017-12-13 17:07:07');
INSERT INTO `te_wto` VALUES ('41', '24', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:07:07', '2017-12-13 17:07:07');
INSERT INTO `te_wto` VALUES ('42', '25', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:10:44', '2017-12-13 17:10:44');
INSERT INTO `te_wto` VALUES ('43', '25', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:10:44', '2017-12-13 17:10:44');
INSERT INTO `te_wto` VALUES ('44', '26', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:14:37', '2017-12-13 17:14:37');
INSERT INTO `te_wto` VALUES ('45', '26', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:14:37', '2017-12-13 17:14:37');
INSERT INTO `te_wto` VALUES ('46', '27', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:18:40', '2017-12-13 17:18:40');
INSERT INTO `te_wto` VALUES ('47', '27', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:18:40', '2017-12-13 17:18:40');
INSERT INTO `te_wto` VALUES ('48', '28', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:20:52', '2017-12-13 17:20:52');
INSERT INTO `te_wto` VALUES ('49', '28', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:20:52', '2017-12-13 17:20:52');
INSERT INTO `te_wto` VALUES ('50', '29', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-13 17:27:58', '2017-12-13 17:27:58');
INSERT INTO `te_wto` VALUES ('51', '29', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '32000', '1', '2017-12-13 17:27:59', '2017-12-13 17:27:59');
INSERT INTO `te_wto` VALUES ('52', '30', '21', 'Tambal ban Motor', '[{\"id\":16,\"product_name\":\"Spuyer\",\"unit\":\"Pcs\",\"description\":\"Spuyer\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-12-13 14:44:06\",\"upd_by\":\"1\",\"price\":5000,\"images\":\"3oR0MQ_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"KC001\",\"flag_daily_report\":\"n\",\"qty_default\":2}]', 'Jasa', '5000', '1', '2017-12-14 08:58:06', '2017-12-14 08:58:06');
INSERT INTO `te_wto` VALUES ('53', '31', '26', 'ISI BARU N2 MOTOR', '[{\"id\":20,\"product_name\":\"Tabung Nitrogen 100 Psi\",\"unit\":\"Psi\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-14 15:55:06\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":\"TBA02\",\"flag_daily_report\":\"n\",\"qty_default\":3}]', 'Jasa', '5000', '3', '2017-12-15 15:26:53', '2017-12-15 15:26:53');
INSERT INTO `te_wto` VALUES ('54', '32', '18', 'Tabung Nitrogen 2000 Psi', '[]', 'Produk', '0', '1', '2017-12-15 15:31:19', '2017-12-15 15:31:19');
INSERT INTO `te_wto` VALUES ('55', '33', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('56', '33', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '3000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('57', '33', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '10000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('58', '33', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('59', '33', '28', 'ISI BARU JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('60', '33', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-18 07:42:19', '2017-12-18 07:42:19');
INSERT INTO `te_wto` VALUES ('61', '34', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-18 11:17:09', '2017-12-18 11:17:09');
INSERT INTO `te_wto` VALUES ('62', '34', '23', 'M-ONE 350ML', '[{\"id\":24,\"product_name\":\"M-ONE 350 ML\",\"unit\":\"Pcs\",\"description\":\"M-One 350 ML\",\"status\":\"y\",\"created_at\":\"2017-10-24 18:21:32\",\"updated_at\":\"2017-12-15 09:52:57\",\"upd_by\":\"1\",\"price\":31000,\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"M001\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '35000', '1', '2017-12-18 11:17:09', '2017-12-18 11:17:09');
INSERT INTO `te_wto` VALUES ('63', '34', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-18 11:17:09', '2017-12-18 11:17:09');
INSERT INTO `te_wto` VALUES ('64', '34', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '3000', '1', '2017-12-18 11:17:09', '2017-12-18 11:17:09');
INSERT INTO `te_wto` VALUES ('65', '35', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-18 12:11:16', '2017-12-18 12:11:16');
INSERT INTO `te_wto` VALUES ('66', '35', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '3000', '1', '2017-12-18 12:11:16', '2017-12-18 12:11:16');
INSERT INTO `te_wto` VALUES ('67', '36', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:40:00', '2017-12-20 14:40:00');
INSERT INTO `te_wto` VALUES ('68', '37', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:40:44', '2017-12-20 14:40:44');
INSERT INTO `te_wto` VALUES ('69', '38', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:41:27', '2017-12-20 14:41:27');
INSERT INTO `te_wto` VALUES ('70', '39', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:42:05', '2017-12-20 14:42:05');
INSERT INTO `te_wto` VALUES ('71', '40', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:42:47', '2017-12-20 14:42:47');
INSERT INTO `te_wto` VALUES ('72', '41', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:43:42', '2017-12-20 14:43:42');
INSERT INTO `te_wto` VALUES ('73', '42', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '10', '2017-12-20 14:44:19', '2017-12-20 14:44:19');
INSERT INTO `te_wto` VALUES ('74', '43', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":4,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '4', '2017-12-20 14:44:39', '2017-12-20 14:44:39');
INSERT INTO `te_wto` VALUES ('75', '44', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '10', '2017-12-20 14:46:13', '2017-12-20 14:46:13');
INSERT INTO `te_wto` VALUES ('76', '45', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '10', '2017-12-20 14:46:52', '2017-12-20 14:46:52');
INSERT INTO `te_wto` VALUES ('77', '46', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '10', '2017-12-20 14:47:55', '2017-12-20 14:47:55');
INSERT INTO `te_wto` VALUES ('78', '47', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '10', '2017-12-20 14:48:42', '2017-12-20 14:48:42');
INSERT INTO `te_wto` VALUES ('79', '48', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":4,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '4', '2017-12-20 14:49:03', '2017-12-20 14:49:03');
INSERT INTO `te_wto` VALUES ('80', '49', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:51:00', '2017-12-20 14:51:00');
INSERT INTO `te_wto` VALUES ('81', '50', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:51:39', '2017-12-20 14:51:39');
INSERT INTO `te_wto` VALUES ('82', '51', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:53:40', '2017-12-20 14:53:40');
INSERT INTO `te_wto` VALUES ('83', '52', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:54:24', '2017-12-20 14:54:24');
INSERT INTO `te_wto` VALUES ('84', '53', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:55:12', '2017-12-20 14:55:12');
INSERT INTO `te_wto` VALUES ('85', '54', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:56:30', '2017-12-20 14:56:30');
INSERT INTO `te_wto` VALUES ('86', '55', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '10', '2017-12-20 14:57:10', '2017-12-20 14:57:10');
INSERT INTO `te_wto` VALUES ('87', '56', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":5,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '5', '2017-12-20 14:57:43', '2017-12-20 14:57:43');
INSERT INTO `te_wto` VALUES ('88', '57', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '10', '2017-12-20 14:58:31', '2017-12-20 14:58:31');
INSERT INTO `te_wto` VALUES ('89', '58', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '10', '2017-12-20 14:59:17', '2017-12-20 14:59:17');
INSERT INTO `te_wto` VALUES ('90', '59', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '10', '2017-12-20 15:00:31', '2017-12-20 15:00:31');
INSERT INTO `te_wto` VALUES ('91', '60', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '10', '2017-12-20 15:01:11', '2017-12-20 15:01:11');
INSERT INTO `te_wto` VALUES ('92', '61', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":10,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '10', '2017-12-20 15:02:17', '2017-12-20 15:02:17');
INSERT INTO `te_wto` VALUES ('93', '62', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":11,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '11', '2017-12-20 15:03:15', '2017-12-20 15:03:15');
INSERT INTO `te_wto` VALUES ('94', '63', '16', 'Spuyer', '[]', 'Produk', '5000', '14', '2017-12-20 15:04:42', '2017-12-20 15:04:42');
INSERT INTO `te_wto` VALUES ('95', '64', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-20 15:06:16', '2017-12-20 15:06:16');
INSERT INTO `te_wto` VALUES ('96', '64', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '1', '2017-12-20 15:06:16', '2017-12-20 15:06:16');
INSERT INTO `te_wto` VALUES ('97', '64', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":11,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '11', '2017-12-20 15:06:16', '2017-12-20 15:06:16');
INSERT INTO `te_wto` VALUES ('98', '65', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '1', '2017-12-20 17:01:11', '2017-12-20 17:01:11');
INSERT INTO `te_wto` VALUES ('99', '66', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"qty_default\":5,\"unit\":\"Psi\",\"price\":0,\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"flag_daily_report\":\"y\",\"status\":\"y\",\"product_code\":null,\"description\":\"Tabung Nitrogen 100 psi\",\"product_name\":\"TABUNG NITROGEN 100 psi\",\"images\":null,\"created_at\":\"2017-12-18 07:32:59\"}]', 'Jasa', '10000', '5', '2017-12-20 17:01:12', '2017-12-20 17:01:12');
INSERT INTO `te_wto` VALUES ('100', '66', '22', 'M-ONE 500ML', '[{\"id\":25,\"qty_default\":2,\"unit\":\"Pcs\",\"price\":41000,\"updated_at\":\"2017-12-15 09:52:41\",\"upd_by\":\"1\",\"flag_daily_report\":\"n\",\"status\":\"y\",\"product_code\":\"T002\",\"description\":\"Oli\",\"product_name\":\"M-ONE 500ML\",\"images\":\"WSv4uL_Jellyfish.jpg\",\"created_at\":\"2017-10-27 10:11:46\"}]', 'Jasa', '45000', '2', '2017-12-20 17:01:12', '2017-12-20 17:01:12');
INSERT INTO `te_wto` VALUES ('101', '66', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-20 17:01:12', '2017-12-20 17:01:12');
INSERT INTO `te_wto` VALUES ('102', '67', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-20 17:02:12', '2017-12-20 17:02:12');
INSERT INTO `te_wto` VALUES ('103', '68', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '4', '2017-12-20 17:27:57', '2017-12-20 17:27:57');
INSERT INTO `te_wto` VALUES ('104', '68', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-20 17:27:57', '2017-12-20 17:27:57');
INSERT INTO `te_wto` VALUES ('105', '68', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '1', '2017-12-20 17:27:57', '2017-12-20 17:27:57');
INSERT INTO `te_wto` VALUES ('106', '69', '28', 'ISI BARU JEEP', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-20 17:42:41', '2017-12-20 17:42:41');
INSERT INTO `te_wto` VALUES ('107', '69', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-20 17:42:41', '2017-12-20 17:42:41');
INSERT INTO `te_wto` VALUES ('108', '70', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-20 17:55:25', '2017-12-20 17:55:25');
INSERT INTO `te_wto` VALUES ('109', '71', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-20 18:24:39', '2017-12-20 18:24:39');
INSERT INTO `te_wto` VALUES ('110', '72', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-20 18:27:50', '2017-12-20 18:27:50');
INSERT INTO `te_wto` VALUES ('111', '73', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-20 18:58:35', '2017-12-20 18:58:35');
INSERT INTO `te_wto` VALUES ('112', '74', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-20 20:19:53', '2017-12-20 20:19:53');
INSERT INTO `te_wto` VALUES ('113', '75', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '4', '2017-12-20 20:22:30', '2017-12-20 20:22:30');
INSERT INTO `te_wto` VALUES ('114', '76', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-20 20:23:56', '2017-12-20 20:23:56');
INSERT INTO `te_wto` VALUES ('115', '77', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-20 20:25:07', '2017-12-20 20:25:07');
INSERT INTO `te_wto` VALUES ('116', '78', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-20 20:25:31', '2017-12-20 20:25:31');
INSERT INTO `te_wto` VALUES ('117', '79', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-20 21:12:39', '2017-12-20 21:12:39');
INSERT INTO `te_wto` VALUES ('118', '79', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '1', '2017-12-20 21:12:39', '2017-12-20 21:12:39');
INSERT INTO `te_wto` VALUES ('119', '80', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"qty_default\":4,\"unit\":\"Psi\",\"price\":0,\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"flag_daily_report\":\"y\",\"status\":\"y\",\"product_code\":null,\"description\":\"Tabung Nitrogen 100 psi\",\"product_name\":\"TABUNG NITROGEN 100 psi\",\"images\":null,\"created_at\":\"2017-12-18 07:32:59\"}]', 'Jasa', '4000', '4', '2017-12-21 05:59:55', '2017-12-21 05:59:55');
INSERT INTO `te_wto` VALUES ('120', '80', '20', 'Tambal ban Mobil', '[{\"id\":17,\"qty_default\":1,\"unit\":\"Pcs\",\"price\":16000,\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"flag_daily_report\":\"n\",\"status\":\"y\",\"product_code\":\"KC002\",\"description\":\"Karet Cacing\",\"product_name\":\"Karet Cacing Mobil\",\"images\":null,\"created_at\":\"2017-10-10 17:05:28\"}]', 'Jasa', '20000', '1', '2017-12-21 05:59:55', '2017-12-21 05:59:55');
INSERT INTO `te_wto` VALUES ('121', '81', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '4', '2017-12-21 06:29:15', '2017-12-21 06:29:15');
INSERT INTO `te_wto` VALUES ('122', '81', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-21 06:29:15', '2017-12-21 06:29:15');
INSERT INTO `te_wto` VALUES ('123', '82', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '1', '2017-12-21 06:31:11', '2017-12-21 06:31:11');
INSERT INTO `te_wto` VALUES ('124', '83', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-21 06:51:57', '2017-12-21 06:51:57');
INSERT INTO `te_wto` VALUES ('125', '84', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":2,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '2', '2017-12-21 07:08:15', '2017-12-21 07:08:15');
INSERT INTO `te_wto` VALUES ('126', '85', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":5,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '5', '2017-12-21 07:30:39', '2017-12-21 07:30:39');
INSERT INTO `te_wto` VALUES ('127', '86', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-21 08:11:41', '2017-12-21 08:11:41');
INSERT INTO `te_wto` VALUES ('128', '87', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '4', '2017-12-21 08:12:09', '2017-12-21 08:12:09');
INSERT INTO `te_wto` VALUES ('129', '88', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":6,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '6', '2017-12-21 08:13:23', '2017-12-21 08:13:23');
INSERT INTO `te_wto` VALUES ('130', '89', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-21 08:59:38', '2017-12-21 08:59:38');
INSERT INTO `te_wto` VALUES ('131', '90', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-21 11:18:54', '2017-12-21 11:18:54');
INSERT INTO `te_wto` VALUES ('132', '91', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-21 11:38:31', '2017-12-21 11:38:31');
INSERT INTO `te_wto` VALUES ('133', '92', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-21 12:13:24', '2017-12-21 12:13:24');
INSERT INTO `te_wto` VALUES ('134', '93', '21', 'Tambal ban Motor', '[{\"id\":26,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 08:29:45\",\"price\":12000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"T003\",\"product_name\":\"Karet Cacing Motor\",\"created_at\":\"2017-12-13 14:46:59\",\"images\":null}]', 'Jasa', '15000', '1', '2017-12-21 12:21:27', '2017-12-21 12:21:27');
INSERT INTO `te_wto` VALUES ('135', '94', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-21 12:24:45', '2017-12-21 12:24:45');
INSERT INTO `te_wto` VALUES ('136', '95', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-21 12:27:22', '2017-12-21 12:27:22');
INSERT INTO `te_wto` VALUES ('137', '96', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":3,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '3', '2017-12-21 12:37:10', '2017-12-21 12:37:10');
INSERT INTO `te_wto` VALUES ('138', '97', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-21 12:37:50', '2017-12-21 12:37:50');
INSERT INTO `te_wto` VALUES ('139', '98', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":6,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '6', '2017-12-21 12:46:49', '2017-12-21 12:46:49');
INSERT INTO `te_wto` VALUES ('140', '99', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":8,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '8', '2017-12-21 13:02:47', '2017-12-21 13:02:47');
INSERT INTO `te_wto` VALUES ('141', '100', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-21 13:33:16', '2017-12-21 13:33:16');
INSERT INTO `te_wto` VALUES ('142', '101', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 16:17:54', '2017-12-21 16:17:54');
INSERT INTO `te_wto` VALUES ('143', '102', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-21 16:59:06', '2017-12-21 16:59:06');
INSERT INTO `te_wto` VALUES ('144', '102', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-21 16:59:06', '2017-12-21 16:59:06');
INSERT INTO `te_wto` VALUES ('145', '103', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 17:01:56', '2017-12-21 17:01:56');
INSERT INTO `te_wto` VALUES ('146', '104', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-21 17:08:02', '2017-12-21 17:08:02');
INSERT INTO `te_wto` VALUES ('147', '105', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 17:25:40', '2017-12-21 17:25:40');
INSERT INTO `te_wto` VALUES ('148', '106', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 17:30:37', '2017-12-21 17:30:37');
INSERT INTO `te_wto` VALUES ('149', '107', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-21 17:30:59', '2017-12-21 17:30:59');
INSERT INTO `te_wto` VALUES ('150', '108', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 17:44:21', '2017-12-21 17:44:21');
INSERT INTO `te_wto` VALUES ('151', '109', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-21 17:44:33', '2017-12-21 17:44:33');
INSERT INTO `te_wto` VALUES ('152', '110', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '1', '2017-12-21 17:50:31', '2017-12-21 17:50:31');
INSERT INTO `te_wto` VALUES ('153', '111', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '1', '2017-12-21 17:50:39', '2017-12-21 17:50:39');
INSERT INTO `te_wto` VALUES ('154', '112', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-21 18:03:13', '2017-12-21 18:03:13');
INSERT INTO `te_wto` VALUES ('155', '113', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-21 18:39:05', '2017-12-21 18:39:05');
INSERT INTO `te_wto` VALUES ('156', '114', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 18:39:28', '2017-12-21 18:39:28');
INSERT INTO `te_wto` VALUES ('157', '115', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-21 18:47:35', '2017-12-21 18:47:35');
INSERT INTO `te_wto` VALUES ('158', '116', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-21 19:54:07', '2017-12-21 19:54:07');
INSERT INTO `te_wto` VALUES ('159', '117', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 19:54:16', '2017-12-21 19:54:16');
INSERT INTO `te_wto` VALUES ('160', '118', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 20:11:42', '2017-12-21 20:11:42');
INSERT INTO `te_wto` VALUES ('161', '119', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '1', '2017-12-21 20:24:50', '2017-12-21 20:24:50');
INSERT INTO `te_wto` VALUES ('162', '120', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"qty_default\":4,\"unit\":\"Psi\",\"price\":0,\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"flag_daily_report\":\"y\",\"status\":\"y\",\"product_code\":null,\"description\":\"Tabung Nitrogen 100 psi\",\"product_name\":\"TABUNG NITROGEN 100 psi\",\"images\":null,\"created_at\":\"2017-12-18 07:32:59\"}]', 'Jasa', '4000', '4', '2017-12-21 20:24:50', '2017-12-21 20:24:50');
INSERT INTO `te_wto` VALUES ('163', '121', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-21 20:47:16', '2017-12-21 20:47:16');
INSERT INTO `te_wto` VALUES ('164', '122', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":3,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '3', '2017-12-21 20:47:35', '2017-12-21 20:47:35');
INSERT INTO `te_wto` VALUES ('165', '123', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-21 20:51:12', '2017-12-21 20:51:12');
INSERT INTO `te_wto` VALUES ('166', '124', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '1', '2017-12-22 06:37:44', '2017-12-22 06:37:44');
INSERT INTO `te_wto` VALUES ('167', '124', '23', 'M-ONE 350ML', '[{\"id\":24,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:57\",\"price\":31000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"M-One 350 ML\",\"product_code\":\"M001\",\"product_name\":\"M-ONE 350 ML\",\"created_at\":\"2017-10-24 18:21:32\",\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\"}]', 'Jasa', '35000', '1', '2017-12-22 06:37:44', '2017-12-22 06:37:44');
INSERT INTO `te_wto` VALUES ('168', '125', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-22 06:38:13', '2017-12-22 06:38:13');
INSERT INTO `te_wto` VALUES ('169', '126', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 07:01:28', '2017-12-22 07:01:28');
INSERT INTO `te_wto` VALUES ('170', '127', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 07:14:54', '2017-12-22 07:14:54');
INSERT INTO `te_wto` VALUES ('171', '128', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 07:18:35', '2017-12-22 07:18:35');
INSERT INTO `te_wto` VALUES ('172', '129', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 07:22:56', '2017-12-22 07:22:56');
INSERT INTO `te_wto` VALUES ('173', '130', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 07:38:14', '2017-12-22 07:38:14');
INSERT INTO `te_wto` VALUES ('174', '130', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '1', '2017-12-22 07:38:14', '2017-12-22 07:38:14');
INSERT INTO `te_wto` VALUES ('175', '131', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-22 08:02:43', '2017-12-22 08:02:43');
INSERT INTO `te_wto` VALUES ('176', '132', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '2', '2017-12-22 08:03:18', '2017-12-22 08:03:18');
INSERT INTO `te_wto` VALUES ('177', '133', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 08:06:17', '2017-12-22 08:06:17');
INSERT INTO `te_wto` VALUES ('178', '134', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '1', '2017-12-22 08:24:59', '2017-12-22 08:24:59');
INSERT INTO `te_wto` VALUES ('179', '134', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '1', '2017-12-22 08:24:59', '2017-12-22 08:24:59');
INSERT INTO `te_wto` VALUES ('180', '135', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 08:25:32', '2017-12-22 08:25:32');
INSERT INTO `te_wto` VALUES ('181', '135', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '1', '2017-12-22 08:25:32', '2017-12-22 08:25:32');
INSERT INTO `te_wto` VALUES ('182', '136', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 09:01:15', '2017-12-22 09:01:15');
INSERT INTO `te_wto` VALUES ('183', '137', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 09:02:15', '2017-12-22 09:02:15');
INSERT INTO `te_wto` VALUES ('184', '138', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":3,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '3', '2017-12-22 09:02:35', '2017-12-22 09:02:35');
INSERT INTO `te_wto` VALUES ('185', '139', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 09:08:24', '2017-12-22 09:08:24');
INSERT INTO `te_wto` VALUES ('186', '140', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 09:27:09', '2017-12-22 09:27:09');
INSERT INTO `te_wto` VALUES ('187', '141', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-22 09:27:18', '2017-12-22 09:27:18');
INSERT INTO `te_wto` VALUES ('188', '142', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 09:37:40', '2017-12-22 09:37:40');
INSERT INTO `te_wto` VALUES ('189', '143', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 09:51:50', '2017-12-22 09:51:50');
INSERT INTO `te_wto` VALUES ('190', '144', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 09:52:14', '2017-12-22 09:52:14');
INSERT INTO `te_wto` VALUES ('191', '145', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '2', '2017-12-22 10:13:46', '2017-12-22 10:13:46');
INSERT INTO `te_wto` VALUES ('192', '145', '20', 'Tambal ban Mobil', '[{\"id\":17,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-13 14:31:07\",\"price\":16000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Karet Cacing\",\"product_code\":\"KC002\",\"product_name\":\"Karet Cacing Mobil\",\"created_at\":\"2017-10-10 17:05:28\",\"images\":null}]', 'Jasa', '20000', '1', '2017-12-22 10:13:46', '2017-12-22 10:13:46');
INSERT INTO `te_wto` VALUES ('193', '146', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 10:35:11', '2017-12-22 10:35:11');
INSERT INTO `te_wto` VALUES ('194', '147', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 11:16:56', '2017-12-22 11:16:56');
INSERT INTO `te_wto` VALUES ('195', '148', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-22 11:38:17', '2017-12-22 11:38:17');
INSERT INTO `te_wto` VALUES ('196', '149', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 11:48:43', '2017-12-22 11:48:43');
INSERT INTO `te_wto` VALUES ('197', '150', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 12:36:18', '2017-12-22 12:36:18');
INSERT INTO `te_wto` VALUES ('198', '151', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 12:37:02', '2017-12-22 12:37:02');
INSERT INTO `te_wto` VALUES ('199', '152', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 12:40:43', '2017-12-22 12:40:43');
INSERT INTO `te_wto` VALUES ('200', '153', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 12:42:08', '2017-12-22 12:42:08');
INSERT INTO `te_wto` VALUES ('201', '154', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-22 12:53:05', '2017-12-22 12:53:05');
INSERT INTO `te_wto` VALUES ('202', '155', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 12:56:02', '2017-12-22 12:56:02');
INSERT INTO `te_wto` VALUES ('203', '156', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 13:20:15', '2017-12-22 13:20:15');
INSERT INTO `te_wto` VALUES ('204', '157', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"qty_default\":4,\"unit\":\"Psi\",\"price\":0,\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"flag_daily_report\":\"y\",\"status\":\"y\",\"product_code\":null,\"description\":\"Tabung Nitrogen 100 psi\",\"product_name\":\"TABUNG NITROGEN 100 psi\",\"images\":null,\"created_at\":\"2017-12-18 07:32:59\"}]', 'Jasa', '10000', '4', '2017-12-22 13:20:17', '2017-12-22 13:20:17');
INSERT INTO `te_wto` VALUES ('205', '158', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 13:29:34', '2017-12-22 13:29:34');
INSERT INTO `te_wto` VALUES ('206', '159', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-22 13:30:27', '2017-12-22 13:30:27');
INSERT INTO `te_wto` VALUES ('207', '160', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '3000', '2', '2017-12-22 13:30:55', '2017-12-22 13:30:55');
INSERT INTO `te_wto` VALUES ('208', '161', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-22 13:41:36', '2017-12-22 13:41:36');
INSERT INTO `te_wto` VALUES ('209', '162', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '1', '2017-12-22 13:41:47', '2017-12-22 13:41:47');
INSERT INTO `te_wto` VALUES ('210', '163', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 14:05:28', '2017-12-22 14:05:28');
INSERT INTO `te_wto` VALUES ('211', '163', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '1', '2017-12-22 14:05:28', '2017-12-22 14:05:28');
INSERT INTO `te_wto` VALUES ('212', '164', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 14:49:05', '2017-12-22 14:49:05');
INSERT INTO `te_wto` VALUES ('213', '164', '22', 'M-ONE 500ML', '[{\"id\":25,\"unit\":\"Pcs\",\"qty_default\":1,\"updated_at\":\"2017-12-15 09:52:41\",\"price\":41000,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"n\",\"description\":\"Oli\",\"product_code\":\"T002\",\"product_name\":\"M-ONE 500ML\",\"created_at\":\"2017-10-27 10:11:46\",\"images\":\"WSv4uL_Jellyfish.jpg\"}]', 'Jasa', '45000', '1', '2017-12-22 14:49:05', '2017-12-22 14:49:05');
INSERT INTO `te_wto` VALUES ('214', '165', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":4,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '4000', '4', '2017-12-22 14:50:04', '2017-12-22 14:50:04');
INSERT INTO `te_wto` VALUES ('215', '166', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":2,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '5000', '2', '2017-12-22 15:02:36', '2017-12-22 15:02:36');
INSERT INTO `te_wto` VALUES ('216', '167', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"unit\":\"Psi\",\"qty_default\":1,\"updated_at\":\"2017-12-18 07:32:59\",\"price\":0,\"upd_by\":\"1\",\"status\":\"y\",\"flag_daily_report\":\"y\",\"description\":\"Tabung Nitrogen 100 psi\",\"product_code\":null,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"created_at\":\"2017-12-18 07:32:59\",\"images\":null}]', 'Jasa', '10000', '1', '2017-12-22 15:21:02', '2017-12-22 15:21:02');
INSERT INTO `te_wto` VALUES ('217', '168', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-22 16:41:57', '2017-12-22 16:41:57');
INSERT INTO `te_wto` VALUES ('218', '168', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-22 16:41:57', '2017-12-22 16:41:57');
INSERT INTO `te_wto` VALUES ('219', '169', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-22 16:44:05', '2017-12-22 16:44:05');
INSERT INTO `te_wto` VALUES ('220', '170', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 16:50:10', '2017-12-22 16:50:10');
INSERT INTO `te_wto` VALUES ('221', '171', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 17:00:03', '2017-12-22 17:00:03');
INSERT INTO `te_wto` VALUES ('222', '172', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-22 17:15:00', '2017-12-22 17:15:00');
INSERT INTO `te_wto` VALUES ('223', '173', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '5000', '2', '2017-12-22 17:16:03', '2017-12-22 17:16:03');
INSERT INTO `te_wto` VALUES ('224', '174', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '10000', '1', '2017-12-22 17:20:33', '2017-12-22 17:20:33');
INSERT INTO `te_wto` VALUES ('225', '174', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-12-22 17:20:33', '2017-12-22 17:20:33');
INSERT INTO `te_wto` VALUES ('226', '174', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-22 17:20:33', '2017-12-22 17:20:33');
INSERT INTO `te_wto` VALUES ('227', '176', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-22 17:56:36', '2017-12-22 17:56:36');
INSERT INTO `te_wto` VALUES ('228', '178', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 18:12:19', '2017-12-22 18:12:19');
INSERT INTO `te_wto` VALUES ('229', '180', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":3}]', 'Jasa', '4000', '3', '2017-12-22 18:15:58', '2017-12-22 18:15:58');
INSERT INTO `te_wto` VALUES ('230', '182', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-22 18:28:23', '2017-12-22 18:28:23');
INSERT INTO `te_wto` VALUES ('231', '182', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 18:28:23', '2017-12-22 18:28:23');
INSERT INTO `te_wto` VALUES ('232', '184', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 18:51:16', '2017-12-22 18:51:16');
INSERT INTO `te_wto` VALUES ('233', '186', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-22 19:18:47', '2017-12-22 19:18:47');
INSERT INTO `te_wto` VALUES ('234', '188', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-22 19:19:25', '2017-12-22 19:19:25');
INSERT INTO `te_wto` VALUES ('235', '190', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-22 20:29:19', '2017-12-22 20:29:19');
INSERT INTO `te_wto` VALUES ('236', '190', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-22 20:29:19', '2017-12-22 20:29:19');
INSERT INTO `te_wto` VALUES ('237', '192', '28', 'ISI BARU JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-22 20:54:01', '2017-12-22 20:54:02');
INSERT INTO `te_wto` VALUES ('238', '192', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '5000', '4', '2017-12-22 20:54:02', '2017-12-22 20:54:02');
INSERT INTO `te_wto` VALUES ('239', '192', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":3}]', 'Jasa', '15000', '3', '2017-12-22 20:54:02', '2017-12-22 20:54:02');
INSERT INTO `te_wto` VALUES ('240', '194', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-22 21:05:15', '2017-12-22 21:05:15');
INSERT INTO `te_wto` VALUES ('241', '196', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-22 21:31:04', '2017-12-22 21:31:04');
INSERT INTO `te_wto` VALUES ('242', '198', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '10000', '4', '2017-12-22 21:31:51', '2017-12-22 21:31:51');
INSERT INTO `te_wto` VALUES ('243', '200', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 21:33:21', '2017-12-22 21:33:21');
INSERT INTO `te_wto` VALUES ('244', '202', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-22 22:07:32', '2017-12-22 22:07:32');
INSERT INTO `te_wto` VALUES ('245', '209', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 06:20:33', '2017-12-23 06:20:33');
INSERT INTO `te_wto` VALUES ('246', '211', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-12-23 06:33:35', '2017-12-23 06:33:35');
INSERT INTO `te_wto` VALUES ('247', '213', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '10000', '1', '2017-12-23 06:52:58', '2017-12-23 06:52:58');
INSERT INTO `te_wto` VALUES ('248', '213', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":3}]', 'Jasa', '4000', '3', '2017-12-23 06:52:58', '2017-12-23 06:52:58');
INSERT INTO `te_wto` VALUES ('249', '215', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-23 06:53:16', '2017-12-23 06:53:16');
INSERT INTO `te_wto` VALUES ('250', '217', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '10000', '1', '2017-12-23 07:28:55', '2017-12-23 07:28:55');
INSERT INTO `te_wto` VALUES ('251', '217', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 07:28:55', '2017-12-23 07:28:55');
INSERT INTO `te_wto` VALUES ('252', '217', '22', 'M-ONE 500ML', '[{\"id\":25,\"product_name\":\"M-ONE 500ML\",\"unit\":\"Pcs\",\"description\":\"Oli\",\"status\":\"y\",\"created_at\":\"2017-10-27 10:11:46\",\"updated_at\":\"2017-12-15 09:52:41\",\"upd_by\":\"1\",\"price\":41000,\"images\":\"WSv4uL_Jellyfish.jpg\",\"product_code\":\"T002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '45000', '1', '2017-12-23 07:28:55', '2017-12-23 07:28:55');
INSERT INTO `te_wto` VALUES ('253', '219', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-23 07:55:19', '2017-12-23 07:55:19');
INSERT INTO `te_wto` VALUES ('254', '221', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 07:55:43', '2017-12-23 07:55:43');
INSERT INTO `te_wto` VALUES ('255', '223', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-12-23 07:57:52', '2017-12-23 07:57:52');
INSERT INTO `te_wto` VALUES ('256', '225', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-23 08:02:51', '2017-12-23 08:02:51');
INSERT INTO `te_wto` VALUES ('257', '227', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-23 08:10:41', '2017-12-23 08:10:41');
INSERT INTO `te_wto` VALUES ('258', '227', '28', 'ISI BARU JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-23 08:10:41', '2017-12-23 08:10:41');
INSERT INTO `te_wto` VALUES ('259', '229', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '5000', '2', '2017-12-23 08:22:38', '2017-12-23 08:22:38');
INSERT INTO `te_wto` VALUES ('260', '229', '16', 'Spuyer', '[]', 'Produk', '5000', '1', '2017-12-23 08:22:38', '2017-12-23 08:22:38');
INSERT INTO `te_wto` VALUES ('261', '231', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":3}]', 'Jasa', '3000', '3', '2017-12-23 08:31:38', '2017-12-23 08:31:38');
INSERT INTO `te_wto` VALUES ('262', '233', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 09:00:27', '2017-12-23 09:00:27');
INSERT INTO `te_wto` VALUES ('263', '235', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-23 09:00:45', '2017-12-23 09:00:45');
INSERT INTO `te_wto` VALUES ('264', '235', '23', 'M-ONE 350ML', '[{\"id\":24,\"product_name\":\"M-ONE 350 ML\",\"unit\":\"Pcs\",\"description\":\"M-One 350 ML\",\"status\":\"y\",\"created_at\":\"2017-10-24 18:21:32\",\"updated_at\":\"2017-12-15 09:52:57\",\"upd_by\":\"1\",\"price\":31000,\"images\":\"2JcBIB_Outlet_Green_Nitrogen_Booth_Standart_Pertamina.jpg\",\"product_code\":\"M001\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '35000', '1', '2017-12-23 09:00:45', '2017-12-23 09:00:45');
INSERT INTO `te_wto` VALUES ('265', '237', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '5000', '2', '2017-12-23 09:00:59', '2017-12-23 09:00:59');
INSERT INTO `te_wto` VALUES ('266', '239', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-23 09:01:22', '2017-12-23 09:01:22');
INSERT INTO `te_wto` VALUES ('267', '241', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-23 09:01:30', '2017-12-23 09:01:30');
INSERT INTO `te_wto` VALUES ('268', '243', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '5000', '2', '2017-12-23 09:27:29', '2017-12-23 09:27:29');
INSERT INTO `te_wto` VALUES ('269', '245', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-12-23 09:43:13', '2017-12-23 09:43:13');
INSERT INTO `te_wto` VALUES ('270', '247', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 09:46:37', '2017-12-23 09:46:37');
INSERT INTO `te_wto` VALUES ('271', '250', '28', 'ISI BARU JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '15000', '4', '2017-12-23 10:19:24', '2017-12-23 10:19:24');
INSERT INTO `te_wto` VALUES ('272', '252', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '10000', '4', '2017-12-23 10:19:38', '2017-12-23 10:19:38');
INSERT INTO `te_wto` VALUES ('273', '254', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '5000', '4', '2017-12-23 10:30:17', '2017-12-23 10:30:17');
INSERT INTO `te_wto` VALUES ('274', '256', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 10:33:11', '2017-12-23 10:33:11');
INSERT INTO `te_wto` VALUES ('275', '258', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '10000', '4', '2017-12-23 10:51:25', '2017-12-23 10:51:25');
INSERT INTO `te_wto` VALUES ('276', '260', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-12-23 11:03:14', '2017-12-23 11:03:14');
INSERT INTO `te_wto` VALUES ('277', '262', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '10000', '4', '2017-12-23 11:11:21', '2017-12-23 11:11:21');
INSERT INTO `te_wto` VALUES ('278', '264', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 11:21:01', '2017-12-23 11:21:01');
INSERT INTO `te_wto` VALUES ('279', '266', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '10000', '1', '2017-12-23 11:43:35', '2017-12-23 11:43:35');
INSERT INTO `te_wto` VALUES ('280', '266', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-23 11:43:35', '2017-12-23 11:43:35');
INSERT INTO `te_wto` VALUES ('281', '268', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 11:44:25', '2017-12-23 11:44:25');
INSERT INTO `te_wto` VALUES ('282', '270', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 11:52:45', '2017-12-23 11:52:45');
INSERT INTO `te_wto` VALUES ('283', '272', '29', 'ISI TAMBAH JEEP', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '5000', '4', '2017-12-23 11:54:50', '2017-12-23 11:54:50');
INSERT INTO `te_wto` VALUES ('284', '274', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 11:56:47', '2017-12-23 11:56:47');
INSERT INTO `te_wto` VALUES ('285', '276', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":3}]', 'Jasa', '4000', '3', '2017-12-23 12:07:16', '2017-12-23 12:07:16');
INSERT INTO `te_wto` VALUES ('286', '276', '20', 'Tambal ban Mobil', '[{\"id\":17,\"product_name\":\"Karet Cacing Mobil\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-12-13 14:31:07\",\"upd_by\":\"1\",\"price\":16000,\"images\":null,\"product_code\":\"KC002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '20000', '1', '2017-12-23 12:07:16', '2017-12-23 12:07:16');
INSERT INTO `te_wto` VALUES ('287', '278', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-23 12:11:28', '2017-12-23 12:11:28');
INSERT INTO `te_wto` VALUES ('288', '280', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-12-23 12:24:50', '2017-12-23 12:24:50');
INSERT INTO `te_wto` VALUES ('289', '282', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 12:33:17', '2017-12-23 12:33:17');
INSERT INTO `te_wto` VALUES ('290', '284', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-23 12:39:21', '2017-12-23 12:39:21');
INSERT INTO `te_wto` VALUES ('291', '286', '21', 'Tambal ban Motor', '[{\"id\":26,\"product_name\":\"Karet Cacing Motor\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-12-13 14:46:59\",\"updated_at\":\"2017-12-13 08:29:45\",\"upd_by\":\"1\",\"price\":12000,\"images\":null,\"product_code\":\"T003\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '15000', '1', '2017-12-23 12:49:50', '2017-12-23 12:49:50');
INSERT INTO `te_wto` VALUES ('292', '351', '22', 'M-ONE 500ML', '[{\"id\":25,\"product_name\":\"M-ONE 500ML\",\"unit\":\"Pcs\",\"description\":\"Oli\",\"status\":\"y\",\"created_at\":\"2017-10-27 10:11:46\",\"updated_at\":\"2017-12-15 09:52:41\",\"upd_by\":\"1\",\"price\":41000,\"images\":\"WSv4uL_Jellyfish.jpg\",\"product_code\":\"T002\",\"flag_daily_report\":\"n\",\"qty_default\":1}]', 'Jasa', '45000', '1', '2017-12-24 13:22:27', '2017-12-24 13:22:27');
INSERT INTO `te_wto` VALUES ('293', '351', '26', 'ISI BARU N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":1}]', 'Jasa', '5000', '1', '2017-12-24 13:22:28', '2017-12-24 13:22:28');
INSERT INTO `te_wto` VALUES ('294', '353', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-24 13:22:48', '2017-12-24 13:22:48');
INSERT INTO `te_wto` VALUES ('295', '355', '27', 'ISI TAMBAH N2 MOTOR', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '3000', '2', '2017-12-24 13:40:03', '2017-12-24 13:40:03');
INSERT INTO `te_wto` VALUES ('296', '357', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-24 13:52:48', '2017-12-24 13:52:48');
INSERT INTO `te_wto` VALUES ('297', '359', '24', 'ISI BARU N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":2}]', 'Jasa', '10000', '2', '2017-12-24 14:04:48', '2017-12-24 14:04:48');
INSERT INTO `te_wto` VALUES ('298', '362', '25', 'ISI TAMBAH N2 MOBIL', '[{\"id\":28,\"product_name\":\"TABUNG NITROGEN 100 psi\",\"unit\":\"Psi\",\"description\":\"Tabung Nitrogen 100 psi\",\"status\":\"y\",\"created_at\":\"2017-12-18 07:32:59\",\"updated_at\":\"2017-12-18 07:32:59\",\"upd_by\":\"1\",\"price\":0,\"images\":null,\"product_code\":null,\"flag_daily_report\":\"y\",\"qty_default\":4}]', 'Jasa', '4000', '4', '2017-12-24 14:10:28', '2017-12-24 14:10:28');
