@extends($view_path.'.layouts.master')
@section('content')
@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'title','label' => 'Title','value' => (old('title') ? old('title') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'title'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'content','label' => 'Description','value' => (old('description') ? old('description') : ''),'form_class' => 'col-md-12', 'class' => 'editor'])!!}

        <div class="form-group form-md-line-input col-md-12">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_time','label' => 'Valid Time','value' => (old('valid_time') ? old('valid_time') : ''),'attribute' => 'readonly required','class' => 'datetimerange'])!!}

          <label>Image</label><br>
          <label class="btn green input-file-label-image_card">
            <input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
          </label>
              <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
            <input type="hidden" name="remove-single-image-image" value="n">
            <br>
          <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

          <div class="form-group single-image-image col-md-12">
            <img src="{{asset($image_path2.'/'.'none.png')}}" class="img-responsive thumbnail single-image-thumbnail">
          </div>

          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','value' => (old('status') ? old('status') : '')])!!}

          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
      </div>
    </div>
  </div>
</form>
@endsection