@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>
  .modal-dialog {
    width: 1300px;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
  
  .canvas-image_card{
    width: 1100px;
    height: 800px;
  }
</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
            <div class="col-md-6">
              <label for="tag">Outlet <span class="required no-margin-bottom" aria-required="true"></span></label>
              <div class="form-group form-md-line-input no-padding-top">
                <select class="select2" name="outlet" class="outlet" id="outlet">
                    @foreach($outlet as $sc)
                        <option value="{{$sc->id}}">{{$sc->outlet_name}}</option>
                    @endforeach
                </select>
              </div>
            </div>

            <div class="col-md-6">
              <label for="tag">Parent <span class="required no-margin-bottom" aria-required="true"></span></label>
              <div class="form-group form-md-line-input no-padding-top">
                <select class="select2" name="parent" class="parent" id="parent">
                    <option value="0">-- Select Parent Package --</option>
                    @foreach($package as $sc)
                      <option value="{{$sc->id}}">{{$sc->package_name}} - {{$sc->barcode}}</option>
                    @endforeach
                </select>
              </div>
            </div>        

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'package_name','label' => 'Package Name','value' => (old('package_name') ? old('package_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'number','name' => 'percent','label' => 'Percent (%)','value' => (old('percent') ? old('percent') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'percent'])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'barcode','label' => 'No Barcode','value' => (old('barcode') ? old('barcode') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'barcode'])!!}

            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" id="valid_date" class="form-control" name="valid_date" value="" readonly="" placeholder="Valid Date">
                <label for="form_floating_Hqd">Valid Date <span class="" aria-required="true">*</span></label>
                <small></small>
              </div>
            </div>

            {!!view($view_path.'.builder.text',['type' => 'number','name' => 'no_box','label' => 'No Box','value' => (old('no_box') ? old('no_box') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6', 'class' => 'no_box'])!!}

            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="number" id="qty" class="form-control" name="qty" value="" placeholder="Qty">
                <label for="form_floating_Hqd">Qty <span class="" aria-required="true">*</span></label>
                <small></small>
              </div>
            </div>

            <!-- <div class="form-group form-md-line-input col-md-12">
                <label>Image</label><br>
                <label class="btn green input-file-label-image">
                    <input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
                </label>
                 
                 <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
                <input type="hidden" name="remove-single-image-image" value="n">
                <br>
                <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

                <div class="form-group single-image-image col-md-12">
                    <img src="{{asset($image_path2.'/'.'none.png')}}" class="img-responsive thumbnail single-image-thumbnail">
                </div>
            </div> -->
            @foreach($outlet as $o)
              <div class="col-md-12 product_outlet" id="product_outlet-{{$o->id}}">
                <label for="tag">Product <span class="required no-margin-bottom" aria-required="true"></span></label>
                <div class="form-group form-md-line-input no-padding-top">
                 
                    <select class="select2" name="productOutlet-{{$o->id}}[]" class="product" id="product" multiple="">
                        @foreach($product as $sc)
                            @if(!in_array($sc->id, $productNoStock) && $o->id == $sc->outlet_id)
                              <option value="{{$sc->id}}">{{$sc->product_name}} - {{$sc->operator_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
              </div>
            @endforeach

      </div>
    </div>
      <div class="row">
            <!-- <div class="col-md-12">
              <div class="col-md-6">
                <label for="tag">Product <span class="required no-margin-bottom" aria-required="true"></span></label>
                <div class="form-group form-md-line-input no-padding-top">
                  <select class="select2" name="product" class="product" id="product">
                      <option value="0">Select Product</option>
                      @foreach($product as $p)
                        <option value="{{$p->id}}">{{$p->product_name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>

              <div class="col-md-6">

                <div class="form-group form-md-line-input" rrc="">

                  <input type="number" class="form-control qty_default" name="qty_default" id="qty_default" value="" placeholder="Qty Default">

                  <label for="form_floating_RRC">Qty Default <span class="required" aria-required="true"></span></label>

                  <small></small>

                </div>
              </div>
            </div>

            <div class="col-md-12">

              {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-product'])!!}

            </div>

            <hr/>

            <div class="table-responsive redeem-auto col-md-12">

              <table class="table table-bordered">

                <thead>
                  <th>Product Name</th>
                  <th>Qty Default</th>
                  <th>Action</th>

                </thead>

                <tbody class="product-data">

                </tbody>

            </table>

            </div> -->

            <div class="col-md-12 actions">
              {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          </div>
      </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      //onclick event add remove outlet
      $(document).on('click','.add-product',function(){
        var product_name      = $('#product option:selected').text();
        var product_id        = $('#product').val();
        var qty_default       = $('#qty_default').val();
        console.log(product_name);
        console.log(qty_default);
        if(product_id == 0){
            alert('Please select product !');
        }else if(qty_default < 1 ){
            alert('Qty default must greater than 0 !');
        }else{
            var temp              = '<tr><td>'+product_name+'<input type="hidden" name="product_id[]" value="'+product_id+'"></td><td>'+qty_default+'<input type="hidden" name="qty_default[]" value="'+qty_default+'"></td><td><button type="button" class="btn btn-danger delete-product"><i class="fa fa-trash"></i></button></td></tr>';
            $('.product-data').append(temp);
        }
      })

      $(document).on('click','.delete-product',function(){
        $(this).closest('tr').remove();
      });

      $( ".service_price" ).blur(function() {  
          // alert('test');
          //number-format the user input
          var val   = $(this).val();
          var val2  = $.formatRupiah(val);
          $(this).val(val2);             
      });

      $("#valid_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

      });

      var outlet_id = $('#outlet').val();
      if(outlet_id){
        $('.product_outlet').hide();
        $('#product_outlet-'+outlet_id).show();
      }

      $('#outlet').on('change',function(e){
          var id = $(this).val();
          $('.product_outlet').hide();
          $('#product_outlet-'+id).show();
      });

      // $('#product').prop('disabled',true);
      // $('#parent').on('change',function(e){
      //     var id = $(this).val();
      //     $('#product').prop('disabled', true);
      //     if(id != 0){
      //       $('#product').prop('disabled', false);
      //     }          
      // });
    });
  </script>
@endpush
@endsection
