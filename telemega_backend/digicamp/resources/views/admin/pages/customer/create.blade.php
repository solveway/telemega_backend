@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
      <div class="portlet-body form">
          @include('admin.includes.errors')
        <div class="row">
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Owner Info</h4>
            </div>
          </div>
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_ktp','label' => 'No. KTP','value' => (old('no_ktp') ? old('no_ktp') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_npwp','label' => 'No. NPWP','value' => (old('no_npwp') ? old('no_npwp') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_dompul','label' => 'No. Dompul','value' => (old('no_dompul') ? old('no_dompul') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}  

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'customer_name','label' => 'Customer Name','value' => (old('customer_name') ? old('customer_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'name', 'required' => 'y'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_hp','label' => 'Phone','value' => (old('no_hp') ? old('no_hp') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}        

          {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
            <h4>Outlet Info</h4>
            </div>
              <!-- <small>Note: Leave blank to use default value</small> -->
          </div>
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'shop_name','label' => 'Outlet Name','value' => (old('shop_name') ? old('shop_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}

          {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'required' => 'y'])!!}

          <div class="col-md-12">
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map" class="gmaps_outlet"> </div>
            <input type="hidden" name="latitude" id="latbox" value="{{(old('latitude') ? old('latitude') : $default_lat)}}"></button>
            <input type="hidden" name="longitude" id="lngbox" value="{{(old('longitude') ? old('longitude') : $default_lng)}}"></button>
          </div>
          <div class="clearfix"></div>

          <div class="form-group form-md-line-input col-md-12">
              <label>Image</label><br>
              <label class="btn green input-file-label-image">
                <input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
              </label>
               
               <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
              <input type="hidden" name="remove-single-image-image" value="n">
              <br>
              <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

            <div class="form-group single-image-image col-md-12">
                <img src="{{asset($image_path2.'/'.'none.png')}}" class="img-responsive thumbnail single-image-thumbnail">
            </div>
          </div>

          {!!view($view_path.'.builder.text',['type' => 'number','name' => 'radius','label' => 'Radius','value' => (old('radius') ? old('radius') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required' => 'y'])!!}

      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      var password = $(".password");
      var username = $(".username");
      var name = $(".name");
      
      $('#generate-password').on('click', function(e){
            var randomstring = Math.random().toString(36).slice(-6);
            password.val(randomstring);
        });

        $('#show-password').on('click', function(e){
            if(password.attr("type") == "password"){
                password.attr("type", "text");
                $("#show-password").addClass("text-primary");
                $("#show-password").removeClass("text-default");
            }
            else{
                password.attr("type", "password");
                $("#show-password").addClass("text-default");
                $("#show-password").removeClass("text-primary");
            }
        });

        $("#birth_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

            });

            $('.user_access').on('change', function(e){
            if(this.value == 2){
              $("#outlet").prop('disabled', true);
            }else{
              $("#outlet").prop('disabled', false);
            }
        });

        $('.name').keyup(function(){ 
          console.log('keyup');
            var slug = convertToUsername($(this).val());
            $(".username").val(slug);    
        });

        function convertToUsername(Text)
        {
            return Text
                .toLowerCase()
                .replace(/ /g,'')
                .replace(/[^\w-]+/g,'')
                ;
        }

        setTimeout(function(){ 
          var lat = $('#latbox').val();
          var lng = $('#lngbox').val();

          google.maps.event.trigger(map, 'resize'); 
        
          Markerlatlng = new google.maps.LatLng(lat, lng);

          map.setCenter(Markerlatlng); // setCenter takes a LatLng object
        }, 2500);
    });

    function initAutocomplete() {
        var lat = parseFloat($('#latbox').val());
        var lng = parseFloat($('#lngbox').val());
        
        markers = [];
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox").value = place.geometry.location.lat();
            document.getElementById("lngbox").value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
@endpush
@endsection