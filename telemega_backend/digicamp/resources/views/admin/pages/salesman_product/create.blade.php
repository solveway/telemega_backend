@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
      <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
      </div>
      <div class="portlet-body form">
          @include('admin.includes.errors')
        <div class="row">
              <div class="col-md-6">
                    <div class="form-group" style=''>
                          <label for="tag">Salesman</label>
                          <select class="select2 user_id" name="user_id">
                            @foreach($user as $u)
                <option value="{{$u->id}}">{{$u->name.' - '.$u->outlet_name}}</option>
              @endforeach
                          </select>
                    </div>
                </div>
          
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="start_date" class="form-control" name="start_date" value="" readonly="" placeholder="Start Date">
              <label for="form_floating_Hqd">Start Date<span class="" aria-required="true">*</span></label>
              <small></small>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="end_date" class="form-control" name="end_date" value="" readonly="" placeholder="End Date">
              <label for="form_floating_Hqd">End Date<span class="" aria-required="true">*</span></label>
              <small></small>
            </div>
          </div>
        
      </div>  
      {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
    </div>
  </div>
</form>
@push('custom_scripts')
  <script>
    $(document).ready(function(){
        $("#start_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

            });

            $("#end_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

            });

            $( ".target" ).blur(function(){
            //number-format the user input
            var val = $(this).val();
            console.log(val);
            var val2 = parseFloat(val.replace(/,/g, ""))
                          .toFixed(2)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            console.log(val2);
            $(this).val(val2);             
        }); 
    });
  </script>
@endpush
@endsection