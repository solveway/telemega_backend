@extends($view_path.'.layouts.master')
@section('content')
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green">
      <i class="icon-layers font-green title-icon"></i>
      <span class="caption-subject bold uppercase"> {{$title}}</span>
    </div>
    <div class="actions">
      <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
    </div>
  </div>
  <div class="portlet-body">
    <div class="row">
      <div class="col-md-3">
          <ul class="list-unstyled profile-nav">
              <li>
                  <img src="{{asset('components/both/images/user')}}/{{ $user->id }}/{{ $user->images }}" onerror="this.src='{{asset($image_path2.'/'.'none.png') }}';" class="img-responsive pic-bordered" alt="" />

              </li>
          </ul>
      </div>

      <div class="col-md-9">
        <div class="row">
          <div class="col-md-2">
            <p class="sbold cus_p">From <span class="t2dot"> : </span></p>
          </div>

          <div class="col-md-4 text-left">
            <p class="cus_p">
              {{ $user->name }}
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <p class="sbold cus_p">To <span class="t2dot"> : </span></p>
          </div>

          <div class="col-md-4 text-left">
            <p class="cus_p">
              admin
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <p class="sbold cus_p">Tanggal <span class="t2dot"> : </span></p>
          </div>

          <div class="col-md-4 text-left">
            <p class="cus_p">
              {{ date_format($data->created_at,"d - m - Y") }}
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <p class="sbold cus_p">Subject <span class="t2dot"> : </span></p>
          </div>

          <div class="col-md-4 text-left">
            <p class="cus_p">
              {{ $data->inbox_name }}
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <p class="sbold cus_p">Message <span class="t2dot"> : </span></p>
          </div>

          <div class="col-md-10 text-left">
            <p class="cus_p">
              {{ $data->content }}
            </p>
          </div>
        </div>
        <!--end row-->
      </div>
    </div>
  </div>
</div>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,textarea,checkbox,.remove-single-image').prop('readonly',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
    });
  </script>
@endpush
