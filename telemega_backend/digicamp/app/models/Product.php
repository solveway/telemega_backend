<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
	protected $table = 'product';
}