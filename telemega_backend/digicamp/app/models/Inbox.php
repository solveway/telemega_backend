<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model{
	protected $table 		= 'inbox';
	protected $user     	= 'digipos\models\User';

    public function user(){
        return $this->belongsTo($this->user,'sales_id');
    }
}