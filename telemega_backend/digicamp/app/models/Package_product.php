<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Package_product extends Model{
	protected $table 	= 'package_product';
	protected $product 	= 'digipos\models\product';

	public function product(){
		return $this->belongsTo($this->product,'id');
	}
}
