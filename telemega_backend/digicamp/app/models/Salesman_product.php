<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Salesman_product extends Model{
	protected $table 		= 'salesman_product';
	protected $user     	= 'digipos\models\User';

    public function user(){
        return $this->belongsTo($this->user,'user_id');
    }
}
