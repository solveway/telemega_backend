<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Package;
use digipos\models\Category_Product;
use digipos\models\Subcategory_product;
use digipos\models\Operator;
use digipos\models\Package_product;
use digipos\models\Product_package_adjustment;
use digipos\models\Outlet;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class PackageController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Package";
		$this->data['title']	= $this->title;
		$this->root_link 		= "package";
		$this->model 			= new Package;
		$this->package_product  = new Package_product;

		$this->bulk_action			= false;
		$this->bulk_action_data 	= [2];
		$this->image_path 			= 'components/both/images/package/';
		$this->data['image_path'] 	= $this->image_path;	
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->field = [
			[
				'name' 		=> 'package_name',
				'label' 	=> 'Package Name',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			[
				'name' 		=> 'barcode',
				'label' 	=> 'Barcode',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			[
				'name' 		=> 'description',
				'label' 	=> 'Description',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			// [
			// 	'name' 		=> 'status',
			// 	'label' 	=> 'Status',
			// 	'type' 		=> 'check',
			// 	'data' 		=> ['y' => 'Active','n' => 'Not Active'],
			// 	'tab' 		=> 'general'
			// ]
		];

		$this->model = $this->model;
		return $this->build('index');
	}

	public function create(){
		$this->data['title'] 				= "Create Package";
		$this->data['product_exist'] 		= Package_product::get();
		// dd($this->data['product_exist']);
		$this->data['productNoStock'] 		= Product_package_adjustment::where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where('outlet_stock_total','<',1)->pluck('product_id')->toArray();
		$this->data['product']  			= Product::join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->leftJoin('operator', 'operator.id', 'subcategory_product.operator_id')->join('Product_package_adjustment', 'Product_package_adjustment.product_id', 'product.id')->select('product.*','operator.operator_name as operator_name', 'subcategory_product.operator_id', 'Product_package_adjustment.outlet_id')->where([['product.status', 'y'],['product.subcategory_product_id', '!=', 1],['product.subcategory_product_id', '!=', 2]])->whereNotIn('product.id', function($q){$q->select('product_id')->from('package_product')->where('status', 'y');})->get();
		// dd(Product::join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->leftJoin('operator', 'operator.id', 'subcategory_product.operator_id')->join('Product_package_adjustment', 'Product_package_adjustment.product_id', 'product.id')->select('product.*','operator.operator_name as operator_name', 'subcategory_product.operator_id', 'Product_package_adjustment.outlet_id')->where([['product.status', 'y'],['product.subcategory_product_id', '!=', 1],['product.subcategory_product_id', '!=', 2]])->whereNotIn('product.id', function($q){$q->select('product_id')->from('package_product')->where('status', 'y');})->pluck('product_name')->toArray());
		$this->data['package']				= $this->model->where([['status', 'y'],['package_id', 0]])->get();
		$this->data['outlet']				= Outlet::where([['status', 'y'],['outlet_pusat', 0]])->get();
		// dd($this->data['service_category']);

		// dd($this->data['package']);
		return $this->render_view('pages.package.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'package_name' 			=> 'required',
		]);

		if(!is_numeric($request->qty)){
			Alert::fail('Qty must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		if(!is_numeric($request->percent)){
			Alert::fail('Percent must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		$product_outlet_id 	= $request->outlet;
		if($request->parent != 0){
			if(count($request->input('productOutlet-'.$product_outlet_id)) == 0){
				Alert::fail('Product must be selected');
				return redirect()->to($this->data['path'].'/create')->withInput($request->input());
			}
		}
		// dd($request->product);
		$level = 0;
		// if($request->parent != 0){
		// 	$data = $this->model->get();

		// 	if($data){
		// 		$level = 1;
		// 		$package_id = $request->package_id;
		// 		for($i=0; $i<count($data); $i++){
		// 			if($data[$i]['id'] == $package_id){
		// 				if($level == 0){
		// 					$level = 1;
		// 				}

		// 				if($data[$i]['level'] != 0){
		// 					$data[$i]['level'] += 1;
		// 					$i = 0;
		// 					$package_id = $data[$i]['package_id'];
		// 				} 
		// 			}
		// 		}
		// 	}
		// }

		if($request->parent != 0){
        	$data = $this->model->where('id', $request->parent)->first();
        	
        	if(count($data) > 0){
        		$level = $data->level + 1;
        	}
        }

		$this->model->package_name			= $request->package_name;
		$this->model->package_id			= $request->parent;
		$this->model->description			= $request->description;
		$this->model->percent				= $request->percent;
		$this->model->barcode				= $request->barcode;
		$this->model->no_box				= $request->no_box;
		$this->model->qty					= $request->qty;
		$this->model->valid_date			= date_format(date_create($request->valid_date),'Y-m-d');
		$this->model->level 				= $level;
		$this->model->parent_status 		= ($request->parent == 0 ? 'y' : 'n');
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();
		// dd($request->product);

    	//insert tabel product_package
		$product = $request->input('productOutlet-'.$product_outlet_id);
		if($product){
			$temp_product 		= [];
			$temp_pp_adjustment = [];
			$curr_global_stock_total = 0;
			foreach ($product as $key => $p_id) {
				$temp_product[]			= [
					'package_id' => $this->model->id,
					'product_id' => $p_id,
					'status' 	 => 'y',
					'upd_by'	 => auth()->guard($this->guard)->user()->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				];

				if(count($temp_product) > 0){
					// dd($temp_product);
					Package_product::insert($temp_product);
				}


				//edit table product_package_adjustment field package_id and package_big_id
				if($request->parent != 0){//update package_big_id in table product_package_adjustment
		    		Product_package_adjustment::where('product_id', $p_id)->update(['package_big_id'=> $request->parent, 'package_id' => $this->model->id]);
		    	}else{//update package_big_id in table product_package_adjustment
		    		Product_package_adjustment::where('product_id', $p_id)->update(['package_id' => $this->model->id]);
		    	}


				// //check if product_id exist in table
				// $Product_package_adjustment = Product_package_adjustment::where('product_id', $p_id)->where('id',  DB::raw("(select max(`id`) from te_product_package_adjustment npa where product_id = $p_id)"))->orderBy('id', 'desc')->first();

				// $Product_package_adjustment2 = Product_package_adjustment::orderBy('id', 'desc')->first();
				// // dd($Product_package_adjustment);
				// if(count($Product_package_adjustment) == 0){
				// 	//add product serial
				// 	// dd($Product_package_adjustment2);
				// 	$temp_global_stock_total 	 = 1;
				// 	if(isset($Product_package_adjustment2)){
				// 		$temp_global_stock_total = $Product_package_adjustment2->global_stock_total;
				// 	}

				// 	$temp_pp_adjustment[]	= [
				// 		'outlet_id'  			=> $request->outlet,
				// 		'package_id' 			=> $this->model->id,
				// 		'product_id' 			=> $p_id,
				// 		'package_big_id'		=> $request->parent,
				// 		'qty' 	 	 			=> 1,
				// 		'outlet_stock_total' 	=> 1,
				// 		'global_stock_total' 	=> $temp_global_stock_total + $curr_global_stock_total,
				// 		'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
				// 		'created_at' 			=> Carbon::now(),
				// 		'updated_at' 			=> Carbon::now()
				// 	];
				// 	// var_dump($curr_global_stock_total);
				// 	$curr_global_stock_total += $temp_global_stock_total;
				// }
			}

			// dd($temp_pp_adjustment);
			// if(count($temp_pp_adjustment) > 0){
			// 	//insert Product_package_adjustment
			// 	Product_package_adjustment::insert($temp_pp_adjustment);
			// }
		}


		
		// $product_id							= $request->product_id;
		// $temp_product						= [];
		// if($product_id){
		// 	$qty_default						= $request->qty_default;
		// 	foreach ($product_id as $key => $p_id) {
		// 		$qtyDefault		= $qty_default[$key];

		// 		$temp_product[]			= [
		// 			'service_product_id'	=> $this->model->id,
		// 			'product_id'			=> $p_id,
		// 			'qty_default'			=> $qtyDefault,
		// 			'upd_by'				=> auth()->guard($this->guard)->user()->id,
		// 			'created_at'			=> Carbon::now(),
		// 			'updated_at'			=> Carbon::now()
		// 		];
		// 	}

		// 	if(count($temp_product) > 0){
		// 		// dd($temp_product);
		// 		Service_product_dt::insert($temp_product);
		// 	}
		// }

		Alert::success('Successfully add new Package');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		
		// if($this->model->package_id == 0 && $this->model->parent_status == 'y'){
		// 	$this->data['outlet_package']		= Product_package_adjustment::where('package_big_id', $id)->orderBy('id', 'desc')->first();
		// }else{
		// 	$this->data['outlet_package']		= Product_package_adjustment::where('package_id', $id)->orderBy('id', 'desc')->first();
		// }
		$this->data['outlet_package']		= Product_package_adjustment::where('package_id', $id)->whereOr('package_big_id', $id)->orderBy('id', 'desc')->first();

		if($this->data['outlet_package'] == null){
			Alert::fail('Please create package small with this package first !');
			return redirect()->to($this->data['path']);
		}
		
		// dd($this->data['outlet']);
		$this->data['title'] 				= "Edit Package ".$this->model->package_name." - ".$this->model->barcode;
		$package_product 					= Package_product::where([['package_id', $id],['status', 'y']])->pluck('product_id')->toArray();
		// dd($package_product);
		$this->data['productNoStock'] 		= Product_package_adjustment::where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where('outlet_stock_total','<',1)->pluck('product_id')->toArray();
		$package_product 					= $this->formatArrayToTitikKoma($package_product);
		$this->data['package_product']		= $package_product;
		// dd($product_exist);
		$product2  							= Product::join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->join('operator', 'operator.id', 'subcategory_product.operator_id');
		// dd($product2->pluck('product.id')->toArray());
		$product2 							= $product2->join('Product_package_adjustment', 'Product_package_adjustment.product_id', 'product.id')->select('product.*', 'operator.operator_name', 'Product_package_adjustment.outlet_id', 'Product_package_adjustment.package_id');
		// dd($this->data['outlet_package']);
		$product2 							= $product2->where([['product.status', 'y'],['product.product_type_id', 2], ['Product_package_adjustment.outlet_id', $this->data['outlet_package']->outlet_id]]);
		// dd($product2->pluck('product.id')->toArray());
		$product2 							= $product2->whereNotIn('product.id', function($q)use($id){$q->select('product_id')->from('package_product')->where([['status', 'y'],['package_id','!=',$id]]);})->get();
		// dd($product2->pluck('id')->toArray());
		$this->data['product']    			= $product2;
		// dd($this->data['product']);
		$this->data['package']				= Package::where([['status', 'y'],['package_id', 0], ['id', '!=', $id]])->get();
		$this->data['data'] 				= Package::leftJoin('Product_package_adjustment', 'Product_package_adjustment.package_id', 'package.id')->where('package.id',$id)->select('package.*', 'Product_package_adjustment.outlet_id')->first();
		// dd($this->data['data']);
		$this->data['outlet']				= Outlet::where([['status', 'y'],['outlet_pusat', 0]])->get();
		return $this->render_view('pages.package.edit');
	}

	public function update(Request $request, $id){
		
		$this->validate($request,[
			'package_name' 			=> 'required',
		]);

		if(!is_numeric($request->qty)){
			Alert::fail('Qty must be number');
			return redirect()->to($this->data['path'].'/'.$id.'/edit/')->withInput($request->input());
		}

		if(!is_numeric($request->percent)){
			Alert::fail('Percent must be number');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}
		
		// dd($product_outlet_id);
		$level = 0;
		// if($request->parent != 0){
		// 	$data = $this->model->get();

		// 	if($data){
		// 		$level = 1;
		// 		$package_id = $request->package_id;
		// 		for($i=0; $i<count($data); $i++){
		// 			if($data[$i]['id'] == $package_id){
		// 				if($level == 0){
		// 					$level = 1;
		// 				}

		// 				if($data[$i]['level'] != 0){
		// 					$data[$i]['level'] += 1;
		// 					$i = 0;
		// 					$package_id = $data[$i]['package_id'];
		// 				} 
		// 			}
		// 		}
		// 	}
		// }

		$this->model 						= $this->model->find($id);
		$product_outlet_id 					= $request->curr_outlet_id;
		if($request->parent != 0){
			//get package which have id request parent
        	$data = Package::where('id', $request->parent)->first();
        	
        	if(count($data) == 1){
        		$level = $data->level + 1;
        	}

        	//if subcategory has child
        	// $data2 = $this->model->where([['id', $id]])->get();
        	// dd($subcategoryProduct2);

        	// $child = $this->model->where('parent_id', $id)->pluck('id')->toArray();
        	//if subcategory has child

        }else{ 
        	// check if has not parent then change level to 0
        	// $subcategoryProduct = $this->model->where('parent_id', $request->parent)->first();
        	// $subcategory->level = 0;

        	// $subcategoryProduct2 = $this->model->where('id', $subcategory->id)->get();
        	// // if has child then minus child subcategory with 1
        	// if(count($subcategoryProduct2)){
        	// 	foreach($subcategoryProduct2 as $sb2){
        	// 		$sb2->level -= 1;
        	// 		$sb2->save();
        	// 	}
        	// }
        }

        if($this->model->parent_status == 'y'){
    		$curr_id = $id;
    		$curr_level = $level;
    		//get full table

    		$data2 		= Package::where([['id', '!=', $id]])->orderBy('level', 'asc')->get();
    		for($i=0; $i<count($data2); $i++){
    			if($data2[$i]['parent_id'] == $curr_id){
    				$curr_level 	+= 1;
    				$data3 			= Package::find($data2[$i]['id']); 			
    				$data3->level 	= $curr_level;
					$data3->save();
					$curr_id 		= $data3->id;

					if($data2['$i']['parent_status'] == 'y'){
						$i = 0;
					}else{
						break;
					}
    			}
    		}
    	}

		$package_id_bef 					= $this->model->package_id;
		
		$this->model->package_name			= $request->package_name;
		$this->model->package_id			= $request->parent;
		$this->model->description			= $request->description;
		$this->model->percent				= $request->percent;
		$this->model->barcode				= $request->barcode;
		$this->model->no_box				= $request->no_box;
		$this->model->qty					= $request->qty;
		$this->model->level 				= $level;
		$this->model->valid_date 			= date_format(date_create($request->valid_date),'Y-m-d');
		$this->model->parent_status 		= ($request->parent == 0 ? 'y' : 'n');
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' 		=> 'image',
						'file_opt' 	=> ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$product 				 = $request->input('productOutlet-'.$product_outlet_id);
		// dd($product);
		if(!$product && $this->model->package_id != 0){
			Alert::fail('Product must selected');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}
		$this->model->save();
		
		//edit table product_package_adjustment field package_
		if($request->parent != 0){//update package_big_id in table product_package_adjustment
    		Package::where('id', $request->parent)->update(['parent_status' => 'y']);
    		// Product_package_adjustment::where('package_id', $this->model->id)->update(['package_big_id'=> $request->parent]);
    	}else{//update package_big_id in table product_package_adjustment and update parent_status yg package_id dipakai
    		$package = Package::where('package_id', $package_id_bef)->get();
    		// if this->model has package_id
    		// dd($package);
    		if(count($package) > 0){
    			Package::where('id', $package_id_bef)->update(['parent_status' => 'y']);
    		}else{
    			Package::where('id', $package_id_bef)->update(['parent_status' => 'n']);
    		}

    		// Product_package_adjustment::where('package_id', $this->model->id)->update(['package_big_id' => null]);
    	}

		// dd($request->product);
		$package_product1 				= Package_product::where('package_id', $id)->get();
		$Product_package_adjustment1 	= Product_package_adjustment::get();
		$parent 						= ($request->parent != 0 ? $request->parent : null);
		$productremovArr 				= [];
		//if product array exist
		// dd('productOutlet-'.$product_outlet_id);
		// dd($product);
		if($product){
			$productPackageAdjustment  	= Product_package_adjustment::where('package_id', $id)->pluck('product_id')->toArray();
			$PackageProduct  			= package_product::where('package_id', $id)->pluck('product_id')->toArray();
			
			if(count($productPackageAdjustment) > count($product)){
				$arrayDiff = array_diff($productPackageAdjustment,$product);
			}else{
				$arrayDiff = array_diff($product,$productPackageAdjustment);
			}
			// dd($arrayDiff);
			foreach ($arrayDiff as $key => $val) {
				Product_package_adjustment::where([['product_id', $val],['outlet_stock_total', '>',0]])->update(['package_big_id' => null, 'package_id' => null]);
				// $flagAddPPA = 0;
			}

			if(count($PackageProduct) > count($product)){
				$arrayDiff2 = array_diff($PackageProduct,$product);
			}else{
				$arrayDiff2 = array_diff($product,$PackageProduct);
			}

			foreach ($arrayDiff2 as $key => $val) {
				package_product::where([['product_id', $val],['package_id', $id]])->update(['status' => 'n']);
				// $flagAddPPA = 0;
			}

			$temp_product 		= [];
			$temp_pp_adjustment = [];
			foreach($product as $key => $p_id) {
				// $package_product 			= Package_product::where([['package_id', $id],['product_id', $p_id]])->first();
				$curr_global_stock_total 	= 0;
				// dd($package_product);

				// if(count($package_product) == 1){
				// 	Package_product::where([['package_id', $id],['product_id', $p_id]])->update(['status' => 'y']);
				// }else{
				// 	$temp_product[]			= [
				// 		'package_id' => $this->model->id,
				// 		'product_id' => $p_id,
				// 		'status' 	 => 'y',
				// 		'upd_by'	 => auth()->guard($this->guard)->user()->id,
				// 		'created_at' => Carbon::now(),
				// 		'updated_at' => Carbon::now()
				// 	];
				// }

				if(count($package_product1) > 0){
					$flagUpdatePackageProduct = 1;
					$flagInsertPackageProduct = 1;
					foreach ($package_product1 as $key => $pp1) {
						if($pp1->product_id == $p_id && $pp1->package_id == $id && $pp1->status == 'y'){
							$flagUpdatePackageProduct = 0;
							$flagInsertPackageProduct = 0;
							break;
						}elseif($pp1->product_id == $p_id && $pp1->package_id == $id && $pp1->status == 'n'){
							$flagUpdatePackageProduct = 1;
							$flagInsertPackageProduct = 0;						}
					}
					

					if($flagInsertPackageProduct == 1){
						$flagUpdatePackageProduct = 0;
						$temp_product[]			= [
							'package_id' => $id,
							'product_id' => $p_id,
							'status' 	 => 'y',
							'upd_by'	 => auth()->guard($this->guard)->user()->id,
							'created_at' => Carbon::now(),
							'updated_at' => Carbon::now()
						];
					}

					if($flagUpdatePackageProduct == 1){
						Package_product::where([['package_id', $pp1->package_id],['product_id', $p_id]])->update(['status' => 'y']);
					}
					// var_dump($p_id.' - '.$flagInsertPackageProduct.' - '.$flagUpdatePackageProduct);
				}

				//if $product bigger than $productPackageAdjustment insert different array from $product
				// dd(count($product).' - '.count($productPackageAdjustment));
				// if(count($product) > count($productPackageAdjustment)){
				// 	//check if product_id exist in table
				// 	$Product_package_adjustment  = Product_package_adjustment::where('id',  DB::raw("(select max(`id`) from te_product_package_adjustment npa where product_id = $p_id)"))->orderBy('id', 'desc')->first();
				// 	// dd(count($Product_package_adjustment));
				// 	// $Product_package_adjustment2 = Product_package_adjustment::orderBy('updated_at', 'desc')->where('product_id', $p_id)->first();
				// 	// dd($Product_package_adjustment);
				// 	//if Product_package_adjustment not found
				// 	if(count($Product_package_adjustment) > 0){

				// 	}elseif(count($Product_package_adjustment) == 0){
				// 		//add product serial
				// 		$temp_global_stock_total 	 = 1;
				// 		// if(isset($Product_package_adjustment2)){
				// 		// 	$temp_global_stock_total = $Product_package_adjustment2->global_stock_total;
				// 		// }
				// 		$temp_pp_adjustment[]	= [
				// 			'outlet_id'  			=> $request->outlet,
				// 			'package_id' 			=> $this->model->id,
				// 			'product_id' 			=> $p_id,
				// 			'package_big_id'		=> $request->parent,
				// 			'qty' 	 	 			=> 1,
				// 			'outlet_stock_total' 	=> 1,
				// 			'global_stock_total' 	=> $temp_global_stock_total,
				// 			'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
				// 			'created_at' 			=> Carbon::now(),
				// 			'updated_at' 			=> Carbon::now()
				// 		];
				// 		// dd($temp_pp_adjustment);
				// 		// var_dump($curr_global_stock_total);
				// 		// $curr_global_stock_total += $temp_global_stock_total;
				// 	}
				// }
				// dd(count($Product_package_adjustment1));
				if(count($Product_package_adjustment1) > 0){
					$flagUpdate = 1;
					foreach ($Product_package_adjustment1 as $key => $ppa1) {
						if($ppa1->product_id == $p_id && $ppa1->outlet_stock_total > 0 && $ppa1->package_id == $id && $ppa1->package_big_id == $parent){
							$flagUpdate = 0;
							break;
						}
					}

					if($flagUpdate == 1){
						// var_dump($p_id);
						Product_package_adjustment::where('product_id', $p_id)->update(['package_id' => $id,'package_big_id' => $parent]);
					}
				}
			}
			// dd($temp_product);
			if(count($temp_product) > 0 && $temp_product != ''){
				// dd($temp_product);
				Package_product::insert($temp_product);
			}

			// dd($temp_pp_adjustment);
			// if(count($temp_pp_adjustment) > 0 && $temp_pp_adjustment != ''){
			// 	//insert Product_package_adjustment
			// 	Product_package_adjustment::insert($temp_pp_adjustment);
			// }
		}else{//if product array null
			// if(count($package_product1) > 0){
			// 	Package_product::where([['package_id', $id]])->update(['status' => 'n']);
			// }

			// if(count($Product_package_adjustment1) > 0){
			// 	Product_package_adjustment::where([['outlet_id', $product_outlet_id],['package_id', $id]])->update(['package_big_id' => null, 'package_id' => null]);
			// }
		}
		
		Alert::success('Successfully add new Package');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Service ".$this->model->product_name;
		$this->data['product']  			= Product::where('status', 'y')->get();
		$this->data['service_category']		= Category_service::get();
		$this->data['data']  				= $this->model;
		$this->data['data2']  				= Service_product_dt::join('product','product.id','service_product_dt.product_id')->select('service_product_dt.*', 'product.product_name as product_name')->where('service_product_id', $id)->get();

		return $this->render_view('pages.service_product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
