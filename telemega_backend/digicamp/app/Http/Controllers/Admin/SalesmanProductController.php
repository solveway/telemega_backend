<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Outlet;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\Sales_target;
use digipos\models\Salesman_product;
use digipos\models\Product;
use digipos\models\Product_package_adjustment;
use digipos\models\Package;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class SalesmanProductController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.salesman-product');
		$this->root_url			= "sales/salesman-product";
		// $this->primary_field 	= "name";
		$this->root_link 		= "salesman-product";
		$this->model 			= new Salesman_product;
		// $this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		// $this->bulk_action_data = [4];
		$this->image_path 		= 'components/both/images/salesman_product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->merchant_id		= '';

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'user_id',
				'label' => 'Sales Name',
				'sorting' => 'y',
				'search' => 'text',
				'search_data' => $this->get_user_sales(),
				'belongto' => ['method' => 'user','field' => 'name']
			],
			[
				'name' => 'outlet_name',
				'label' => 'Outlet Name',
				'sorting' => 'y',
				'search' => 'text',
			],
			[
				'name' => 'start_date',
				'label' => 'Start Date',
				'sorting' => 'y',
				'search' => 'text',

			],
			[
				'name' => 'end_date',
				'label' => 'End Date',
				'sorting' => 'y',
				'search' => 'text',
			],
			// [
			// 	'name' => 'status',
			// 	'label' => 'Status',
			// 	'type' => 'check',
			// 	'data' => ['y' => 'Active','n' => 'Not Active'],
			// 	'tab' => 'general'
			// ]
		];


		$this->model = $this->model->join('user','user.id','salesman_product.user_id')->join('outlet','outlet.id','user.outlet_id')->select('salesman_product.*', 'user.name as user_name', 'outlet.outlet_name as outlet_name');
		return $this->build('index');

		// global
		// $this->data['user'] = $this->get_user();
		// return $this->render_view('pages.user.index');
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		$this->data['user'] = User::join('outlet', 'outlet.id', 'user.outlet_id')->where('user_access_id',3)->select('user.*', 'outlet.outlet_name')->get();
		// $this->data['outlet'] = Outlet::where('status','y')->get();

		return $this->render_view('pages.salesman_product.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'start_date'			=> 'required',
			'end_date'				=> 'required'
		]);

		// $outlet_value = "";
		// if($request->user_access_id != '2'){
		// 	foreach($request->outlet as $key => $o){
		// 		if($key == 0) $outlet_value .= ";";
		// 		$outlet_value .= $o.";";
		// 	}
		// }

		$this->model->user_id 			= $request->user_id;
		// $this->model->status 			= 'y';
		$this->model->start_date 		= date_format(date_create($request->start_date),'Y-m-d');
		$this->model->end_date 			= date_format(date_create($request->end_date), 'Y-m-d');
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create sales target');
		// return redirect()->to($this->data['path']);
		return redirect()->to($this->data['path'].'/'.$this->model->id.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['user'] = $this->model->find($id);
		$this->data['title'] = 'View '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] = $this->get_user_access();

		$this->data['ouser'] = "";
		$oulet_id = explode(";", $this->data['user']->outlet_id);
		$i = 0;
		foreach($oulet_id as $key => $o){
			if($o != ""){
				$oname = Outlet::find($o)->outlet_name;
				if($i != 0) $this->data['ouser'] .= ", ";
				$this->data['ouser'] .= $oname;

				$i++;
			}
		}

		return $this->render_view('pages.user.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['data'] 	= $this->model->join('user', 'user.id', 'salesman_product.user_id')->join('outlet', 'outlet.id', 'user.outlet_id')->select('salesman_product.*', 'user.name', 'outlet.id as outlet_id')->find($id);
		$this->data['title'] 	= 'Edit '.$this->title.' '.$this->data['data']->username;	
		$this->data['user'] 	= User::join('outlet', 'outlet.id', 'user.outlet_id')->where('user_access_id',3)->select('user.*', 'outlet.outlet_name')->get();
		// dd($this->data['user']);
		$outlet_id 				= $this->data['data']->outlet_id;
		/*get product serial*/
		// get all product, with outlet_stock_total > 0
		$this->data['product'] 				= Product::leftJoin('product_type', 'product_type.id', 'product.product_type_id')->where([['product.status', 'y']])->Join('product_package_adjustment', 'product_package_adjustment.product_id', 'product.id')->where([['product_type_name','Serial'],['product_package_adjustment.outlet_id',$outlet_id],['product_package_adjustment.outlet_stock_total', '>', '0']])->select('product.*', 'product_package_adjustment.product_id', 'product_package_adjustment.package_id')->get(); 

		//get product outlet_id, from Product_package_adjustment
		$this->data['serial_product']  				= Product::join('product_type', 'product_type.id', 'product.product_type_id')->whereIn('product.id', function($q)use($outlet_id){$q->select('product_id')->from('product_package_adjustment')->where([['outlet_id', $outlet_id], ['package_id', null], ['package_big_id', null], ['outlet_stock_total', '>', 0]]);})->select('product.*')->where([['product_type_name','Serial']])->get();

		$this->data['serial_product_selected'] 		= Salesman_product::where([['user_id', '!=', $this->data['data']->user_id], ['list_product', '!=', null]])->pluck('list_product')->toArray();
		// dd($this->data['serial_product_selected']);

		// $this->data['serial_product_selected'] 	= Salesman_product::where('id', $outlet_id)->get();
		// dd($this->data['data']->list_product);
		/*end get product serial*/

		/*get package small from product serial*/
		//get package which has product and package_id = 0
		$package_small  					= Package::join('package_product', 'package_product.package_id', 'package.id')->where([['package_product.status', 'y'], ['package.package_id', 0],['package.status', 'y']])->select('package.*', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id')->groupBy('package_product.package_id');
		// dd($package_small->get());
		$this->data['package_small']  		= $package_small->get();
		// dd($this->data['package_small']);
		$package_small_exist 	= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where([['outlet_id', $outlet_id],['package_big_id', null], ['package_id', '!=',null], ['package_id', '!=',null],['product_type_id', 2], ['outlet_stock_total', '>',0]])->orderBy('package_id', 'asc')->groupBy('package_id')->pluck('product_package_adjustment.package_id')->toArray();
		/*end get package small from product serial*/

	
		//get package which has package_id, join with package product
		$this->data['package_small2']  		= Package::join('package_product', 'package_product.package_id', 'package.id')->where([['package.status', 'y'], ['package_product.status', 'y'],['package.package_id', '!=',0]])->select('package.*', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id')->groupBy('package_product.package_id')->orderBy('package_product.package_id')->get();
		// dd($this->data['package_small2']);
		/*get package big*/
		//get parent package which package has product
		$this->data['package_big']  		= Package::where([['status', 'y'],['package_id', '0']])->whereNotIn('id', $package_small_exist)->get();
		// dd($this->data['package_big']);
		/*end get package big*/

		$this->data['productNoStock'] 			= Product_package_adjustment::where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id and outlet_id = $outlet_id)"))->where([['outlet_stock_total','<',1]])->pluck('product_id')->toArray();

		$this->data['package_selected'] 		= Salesman_product::where([['user_id', '!=', $this->data['data']->user_id], ['list_package', '!=', null]])->pluck('list_package')->toArray();
		// qty product
		$this->data['qty_product']  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity']])->select('product.*')->get();
		
		$this->data['qty_product_outlet']  		= Product::Join('product_package_adjustment', 'product_package_adjustment.product_id', 'product.id')->join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity'],['product_package_adjustment.outlet_id', $outlet_id]])->select('product.*', 'product_package_adjustment.outlet_stock_total')->where('Product_package_adjustment.id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id and outlet_id = $outlet_id)"))->get();


		return $this->render_view('pages.salesman_product.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'start_date'			=> 'required',
			'end_date'				=> 'required'
		]);

		$start_date 					= date_format(date_create($request->start_date),'Y-m-d');
		$end_date 						= date_format(date_create($request->end_date),'Y-m-d');
		if($start_date > $end_date){
			Alert::fail('Start date must lower than end date !');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}

		$this->model 					= $this->model->find($id);
		// $this->model->user_id 			= $request->user_id;
		// $this->model->status 			= 'y';
		$this->model->start_date 		= date_format(date_create($request->start_date),'Y-m-d');
		$this->model->end_date 			= date_format(date_create($request->end_date), 'Y-m-d');
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);

		$productSerial 				= $request->product;
		$packageList  				= '';
		$productSerialList 			= '';
		$temp_package_big_id 		= [];
		foreach ($productSerial as $key => $dy) {
			// dd($dy);
			$ps 	= explode("-",$dy);

			if($ps[0] == 'package_big'){
				// array_push($packageArr, $ps[1]);
				if($packageList == ''){
					$packageList .= ';'.$ps[1].';';
				}else{
					$packageList .= $ps[1].';';
				}
			}

			if($ps[0] == 'package_small'){
				// array_push($packageArr, $ps[1]);
				if($packageList == ''){
					$packageList .= ';'.$ps[1].';';
				}else{
					$packageList .= $ps[1].';';
				}
			}

			if($ps[0] == 'kartu_perdana'){
				// array_push($productSerialArr, $ps[1]);
				if($productSerialList == ''){
					$productSerialList .= ';'.$ps[1].';';
				}else{
					$productSerialList .= $ps[1].';';
				}
			}
		}

		// $temp_package_big 			= [];
		// $temp_package_small 		= [];
		// $temp_product_serial 		= [];
		// $temp_arr					= [];
		// if(count($productSerialList) > 0){
		// 	foreach ($productSerial as $key => $val) {
		// 		$this->formatArrayToTitikKoma($temp_product_serial);
		// 	}
		
		// }

		// if(count($packageArr) > 0){
		// 	foreach ($productSerial as $key => $val) {
		// 		$this->formatArrayToTitikKoma($temp_product_serial);
		// 	}
		// }
		// dd($productSerialList);
		$this->model->list_product 	= $productSerialList;
		$this->model->list_package 	= $packageList;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully update Sales Target');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}

	public function get_user_sales(){
		$q = $this->build_array(User::where('user_access_id',3)->get(),'id','name');
		return $q;
	}
}
