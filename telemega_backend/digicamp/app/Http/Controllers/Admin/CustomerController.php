<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Customer;
use digipos\models\Province;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CustomerController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title 			= "Customer";
		// $this->root_url			= "users/customer";
		$this->primary_field 	= "name";
		$this->root_link 		= "customer";
		$this->model 			= new Customer;
		$this->bulk_action		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/both/images/customer/';
		$this->image_path2 		= 'components/both/images/web/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= $this->image_path2;
		// $this->data['root_url']		= $this->root_url;

		$this->data['default_lat']	=  "-6.1896776";
		$this->data['default_lng']	=  "106.8381752";
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'images',
				'label' => 'Images',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' => 'customer_name',
				'label' => 'Customer Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'email',
				'label' => 'Email',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'status',
				'label' => 'Status',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		$this->model = $this->model;
		// dd($this->model->get());
		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 						= "Create Customer";
		$this->data['province']						= Province::get();

		return $this->render_view('pages.customer.create');
	}

	public function store(Request $request){
		$this->validate($request,[
		// 	'name' 			=> 'required|unique:product,product_name',
		// 	'price' 		=> 'required',
		// ],[
  //           'name.required' => 'No Kartu is Required.',
  //           'name.unique' 	=> 'No Kartu has already been taken.',
        ]);

		$this->model->no_ktp					= $request->no_ktp;
		$this->model->no_npwp					= $request->no_npwp;
		$this->model->no_dompul					= $request->no_dompul;
		$this->model->customer_name				= $request->customer_name;
		$this->model->shop_name					= $request->shop_name;
		$this->model->no_hp						= $request->no_hp;
		$this->model->email						= $request->email;
		$this->model->address 					= $request->address;
		// $this->model->category_product_id	= $request->category_product;
		$this->model->lat 						= $request->latitude;
		$this->model->long 						= $request->longitude;
		$this->model->radius 					= $request->radius;
		$this->model->upd_by					= $this->decode_rupiah($request->price);
		$this->model->status					= 'y';
		$this->model->upd_by 					= auth()->guard($this->guard)->user()->id;
		// ($request->daily_report == 'y' ? $this->model->flag_daily_report = 'y' : $this->model->flag_daily_report = 'n');
		// dd($this->image_path.$curr_id.'/');
		if($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();

		// $this->increase_version();

		Alert::success('Successfully add new Customer');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);		
		$this->data['title'] 				= "Edit Product ".$this->model->customer_name;
		$this->data['data']  				= $this->model;
		// dd($this->data['data']->subcategory_product_id);
		// $this->data['province']				= Province::get();

		return $this->render_view('pages.customer.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
		// 	'name' 			=> 'required|unique:product,product_name',
		// 	'price' 		=> 'required',
		// ],[
  //           'name.required' => 'No Kartu is Required.',
  //           'name.unique' 	=> 'No Kartu has already been taken.',
        ]);
		
		$this->model							= $this->model->find($id);
		$this->model->no_ktp					= $request->no_ktp;
		$this->model->no_npwp					= $request->no_npwp;
		$this->model->no_dompul					= $request->no_dompul;
		$this->model->customer_name				= $request->customer_name;
		$this->model->shop_name					= $request->shop_name;
		$this->model->no_hp						= $request->no_hp;
		$this->model->email						= $request->email;
		$this->model->address 					= $request->address;
		// $this->model->category_product_id	= $request->category_product;
		$this->model->lat 						= $request->latitude;
		$this->model->long 						= $request->longitude;
		$this->model->radius 					= $request->radius;
		$this->model->upd_by					= $this->decode_rupiah($request->price);
		$this->model->status					= 'y';
		$this->model->upd_by 					= auth()->guard($this->guard)->user()->id;
		// ($request->daily_report == 'y' ? $this->model->flag_daily_report = 'y' : $this->model->flag_daily_report = 'n');
		// dd($this->image_path.$curr_id.'/');
		if($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}


		// dd($this->model);
		$this->model->save();
		// $this->increase_version();
		
		Alert::success('Successfully edit Customer');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['dompul'] 				= ($this->model->subcategory_product_id == 0 ? '1' : '0');
		$this->data['dompul_reg'] 			= ($this->model->id == 3 ? '1' : '0');

		if($this->data['dompul'] == '0'){
			$this->data['data']  			= $this->model->join('province', 'province.id', 'product.province_id')->join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->join('product_type', 'product_type.id', 'product.product_type_id')->select('product.*', 'province.name as province_name', 'subcategory_product.subcategory_product_name as subcategory_product_name', 'product_type.product_type_name')->where('product.id', $id)->first();
		}else{
			$this->data['data']  			= $this->model->join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->join('product_type', 'product_type.id', 'product.product_type_id')->select('product.*', 'subcategory_product.subcategory_product_name as subcategory_product_name', 'product_type.product_type_name')->where('product.id', $id)->first();
		}
		
		// dd($this->data['data']);
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function detail($id, $slug){
		$this->data['title'] 	= 'Detail Posting ';
		$query 	= $this->model2->join('order_status', 'order_status.id', 'order_post.status')->join('category', 'category.id', 'order_post.category_id')->join('province', 'province.province_id', 'order_post.province_id')->join('customer', 'customer.id', 'order_post.customer_id')->select('order_post.*','customer.id as cst_id','customer.name as cst_name', 'customer.images','order_status.desc','province.name as prov_name', 'category.category_name')->first();

		$conv = json_decode($query->bid_mitra_id);

		if(count($conv) > 0){
			foreach($conv as $q => $bd){
				$query2 = Mitra::select('name')->where('id', $bd->id)->first();
				$mitra[$q] = [
							'name' => $query2->name,
							'price' => $bd->price,
							];
			}
		}

		$this->data['data1'] = $query;
		$this->data['data2'] = $mitra;

		return $this->render_view('pages.customer.detail');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export();
	}
}	
