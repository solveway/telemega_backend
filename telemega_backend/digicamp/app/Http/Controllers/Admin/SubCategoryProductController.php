<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Adjustemnt;
use digipos\models\Province;
use digipos\models\Category_product;
use digipos\models\Subcategory_product;
use digipos\models\Operator;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class SubcategoryProductController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Category Product";
		$this->data['title']	= $this->title;
		$this->root_link 		= "subcategory-product";
		$this->model 			= new Subcategory_product;

		$this->bulk_action			= true;
		$this->bulk_action_data 	= [3];
		// $this->image_path 			= 'components/both/images/product/';
		// $this->data['image_path'] 	= $this->image_path;
		// $this->image_path2 			= 'components/both/images/web/';
		// $this->data['image_path2'] 	= $this->image_path2;
		// $this->product_type 			= ['Kartu Perdana (KP)','Dompet Pulsa (Dompul)'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			[
				'name' 		=> 'subcategory_product_name',
				'label' 	=> 'Subcategory Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			// [
			// 	'name' 		=> 'category_product_name',
			// 	'label' 	=> 'Category Name',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			[
				'name' 		=> 'parent_name',
				'label' 	=> 'Parent Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'level',
				'label' 	=> 'Level',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' 	=> 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		// $this->model = $this->model->join('order_status', 'order_status.id', 'orderhd.order_status')->where('type_order', 'not like', '%post%')->select('orderhd.*', 'order_status.desc');


		$this->model = $this->model->whereNotIn('id', [1,2])->select(DB::raw('te_subcategory_product.*, (SELECT t.subcategory_product_name FROM te_subcategory_product t WHERE t.id=te_subcategory_product.parent_id) AS parent_name'));
		// dd($this->model->get());
		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 					= "Create Subcategory Product";
		// $this->data['unit']  					= $this->unit;
		$this->data['province']					= Province::get();
		$this->data['operator']					= Operator::where('status', 'y')->get();
		$this->data['category_product']			= Category_product::where([['status', 'y'],['id', '!=', 2]])->get();
		$this->data['parent']					= Subcategory_product::where('level', 0)->get();

		return $this->render_view('pages.subcategory-product.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'name' 		=> 'required|unique:subcategory_product,subcategory_product_name',
		],[
            'name.required' => 'Subcategory Name is Required.',
            'name.unique' 	=> 'Subcategory Name has already been taken.',
        ]);

        $level = 0;
        // check if has parent then get subcategory id from it's parent_id + 1
        if($request->parent != 0){
        	$subcategoryProduct = $this->model->where('id', $request->parent)->first();
        	
        	if(count($subcategoryProduct) > 0){
        		$level = $subcategoryProduct->level + 1;
        	}
         }

		$this->model->subcategory_product_name		= $request->name;
		$this->model->category_product_id 			= $request->category_product;
		$this->model->description					= $request->description;
		$this->model->parent_id						= $request->parent;
		$this->model->operator_id					= $request->operator;
		$this->model->level							= $level;
		$this->model->status 						= 'y';
		$this->model->upd_by 						= auth()->guard($this->guard)->user()->id;
		// ($request->daily_report == 'y' ? $this->model->flag_daily_report = 'y' : $this->model->flag_daily_report = 'n');

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$curr_id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// dd($this->model);
		$this->model->save();

		// $this->increase_version();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 							= $this->model->find($id);
		$this->data['title'] 					= "Edit Subcategory Product ".$this->model->product_name;
		// $this->data['unit']  				= $this->unit;
		$this->data['data']  					= $this->model;
		$this->data['province']					= Province::get();
		$this->data['operator']					= Operator::where('status', 'y')->get();
		$this->data['category_product']			= Category_product::where('status', 'y')->get();
		//get subcategory bigger than itself and if has child not include
		$this->data['parent']					= Subcategory_product::where([['level', 0],['id', '!=', $id]])->get();

		return $this->render_view('pages.subcategory-product.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'name' 		=> 'required|unique:subcategory_product,subcategory_product_name,'.$id,
		],[
            'name.required' => 'Category Name is Required.',
            'name.unique' 	=> 'Category Name has already been taken.',
        ]);

		$this->model 								= $this->model->find($id);
		$level = 0;
		// check if has parent then get subcategory id from it's parent_id + 1
        if($request->parent != 0){
        	$subcategoryProduct = $this->model->where('id', $request->parent)->first();
        	
        	if(count($subcategoryProduct) > 0){
        		$level = $subcategoryProduct->level + 1;
        	}

        	//if subcategory has child
        	$subcategoryProduct2 = $this->model->where([['id', '!=', $id],['level', '>', $this->model->level]])->orderBy('level', 'asc')->get();
        	// dd($subcategoryProduct2);
        	$child = $this->model->where('parent_id', $id)->pluck('id')->toArray();
        	if(count($subcategoryProduct2) > 0){
        		$curr_id = $id;
        		$curr_level = $this->model->level;
        		foreach($subcategoryProduct2 as $sub2) {
        			$subcategoryProduct3 = $this->model->where('parent_id', $curr_id)->first();
        			if(count($subcategoryProduct3) > 0){
        				$curr_level += 1;
        				$sub2->level = $curr_level;
    					$curr_id = $sub2->id;
    					$sub2->save();
        			}
        		}
        	}
        }else{ 
        	// check if has not parent then change level to 0
        	// $subcategoryProduct = $this->model->where('parent_id', $request->parent)->first();
        	// $subcategory->level = 0;

        	// $subcategoryProduct2 = $this->model->where('id', $subcategory->id)->get();
        	// // if has child then minus child subcategory with 1
        	// if(count($subcategoryProduct2)){
        	// 	foreach($subcategoryProduct2 as $sb2){
        	// 		$sb2->level -= 1;
        	// 		$sb2->save();
        	// 	}
        	// }
        }

		$this->model->subcategory_product_name		= $request->name;
		$this->model->category_product_id 			= $request->category_product;
		$this->model->description					= $request->description;
		$this->model->parent_id						= $request->parent;
		$this->model->operator_id					= $request->operator;
		$this->model->level							= $level;
		$this->model->status 						= 'y';
		$this->model->upd_by 						= auth()->guard($this->guard)->user()->id;


		// if($request->input('remove-single-image-image') == 'y'){
		// 	if($this->model->images != NULL){
		// 		File::delete($this->image_path.$this->model->id.'/'.$this->model->images);
		// 		$this->model->images = '';
		// 	}
		// }

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// dd($this->model);
		$this->model->save();
		// $this->increase_version();
		
		Alert::success('Successfully add new Product');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
