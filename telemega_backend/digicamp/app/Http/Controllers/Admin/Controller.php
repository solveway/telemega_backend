<?php namespace digipos\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;

use DB;
use Request;
use View;
use Cache;
use Cookie;
use App;
use Session;
use digipos\models\Config;
use digipos\models\Mslanguage;
use digipos\models\Store;

use digipos\Libraries\Breadcrumb;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data         = [];
    public $root_path    = '_admin';
    public $view_path    = 'admin';
    public $guard        = 'admin';
    public $auth_guard   = 'auth:admin';
    public $guest_guard  = 'guest:admin';
    public $role_guard   = 'role:admin';
    public $merchant_access = 'all';
    public $store_access    = 'all';

    public function __construct() {
        
        $this->data['guard']        = $this->guard;
        $this->data['root_path']    = $this->root_path;
        $this->data['view_path']    = $this->view_path;
        Event::listen(Authenticated::class, function ($event) {
            $this->load_language($event->user);
            $this->data['authmenu'] = Session('authmenux');
            $this->data['msmenu']   = Session('msmenu');
            // dd($this->data['msmenu']);  
            $this->data['access_merchant']   = Session('access_merchant');            
            $this->data['access_store']   = Session('access_store');            
        });

        //Global Number
        $no = 1;
        if (Request::has('page')){
            $no = Request::input('page') * 10 - 9;
        }
        $this->data['no'] = $no;
        $this->data['breadcrumb'] = Breadcrumb::_get($this->data);

        //Set Global
        $query_config = new Config;
        //$config = $this->cache_query('config',$query_config,'get');
        $config = $query_config->get();
        foreach($config as $c){
            $this->data[$c->name] = $c->value;
            $this->data['email_config'][$c->name] = $c->value;
        }
        
        //Current path
        $path = explode('/',Request::path());
        if(isset($path[2]) && in_array($path[2],['create','edit','view','sorting','destroy','ext'])){
            $path = $path[1];
        }else{
            if(isset($path[1])){
                if(isset($path[2]) && (int)$path[2]){
                    $path = $path[1];
                }else{
                    $path = isset($path[2]) ? $path[1].'/'.$path[2] : $path[1];
                }
            }else{
                $path = $path[0];
            }
        }  
        $this->data['path'] = $this->root_path.'/'.$path;
     }

    public function render_view($view = ''){
        $data = $this->data;
        if ($view == 'builder'){
            return view($this->view_path.'.builder.view',$data);
        }else{
            return view($this->view_path.'.'.$view,$data);
        }
    }

    public function redirect_to($url){
        return Redirect()->to($url);
    }

    public function redirect_back(){
        return Redirect()->back();
    }

    public function cache_query($name,$query,$type='',$time = 60){
        $c = Cache::remember($name, $time, function() use($query,$type){
            if (!empty($type)){
                if ($type == 'first'){
                    $q = $query->first();
                }else{
                    $q = $query->get();
                }
                return $q;
            }else{
                return $query;
            }
        });
        //Cache::flush();
        return $c;
    }

    public function load_language($user){
        if($user->language_id == ''){
            $language   = 'id';
        }else{
            $language   = $user->language->language_name_alias;
        }

        $this->data['global_language'] = $this->cache_query('config',Mslanguage::where('status','y'),'get');
        App::setLocale($language);
    }

    public function displayToSql($date){
        if($date != ""){
            $date_explode = explode("-", $date);

            if(count($date_explode) == 3){
                return $date_explode[2]."-".$date_explode[1]."-".$date_explode[0];
            }
        }
    }

    public function dtpToSql($date){
        if($date != ""){
            $date_explode = explode("/", $date);

            if(count($date_explode) == 3){
                return $date_explode[2]."-".$date_explode[1]."-".$date_explode[0];
            }
        }
    }

    public function myStore(){
        $my_store = json_decode(auth()->guard($this->guard)->user()->store_id);
        // dd($my_store);
        $store = [];
        if(in_array(auth()->guard($this->guard)->user()->store_id, ["0","1"])){
            $store        = Store::pluck('id')->toArray();
        }else{
            if(is_array($my_store)){
                $store         = Store::whereIn('id',$my_store)->pluck('id')->toArray();
            }else if($my_store == 2){
                $store         = Store::where('merchant_id',auth()->guard($this->guard)->user()->merchant_id)->pluck('id')->toArray();
            }
        }

        return $store;
    }

    public function formatArrayToTitikKoma($arr){
        $push_data2     = '';
        foreach($arr as $pn){
            $push_data2 .= ';'.$pn;
        }

        if($push_data2 != ''){
            $push_data2     .=  ';';    
        }
        return $push_data2;
    }

    public function formatTitikKomaToArray($data){
        $data = explode(";",$data);
        $push_data2 = [];
        for($i=0; $i< count($data); $i++){
            if($data[$i] != '' && isset($data[$i])){
                $push_data2[] = $data[$i];
            }
        }
        return $push_data2;
    }

    public function decode_rupiah($price){
        if($price != 'Nan'){
            $price = str_replace(',', '', $price);
            $price = substr($price, 0, strpos($price, '.'));
            return $price;
        }else{
             return null;
        }
    }

    public function increase_version(){
        $config                   = Config::where('name', 'product_version')->first();
        $config->value            += 1; 
        $config->save();
    }

}
