<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Outlet;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\Sales_target;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class SalesTargetController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.sales-target');
		$this->root_url			= "sales/sales-target";
		$this->primary_field 	= "name";
		$this->root_link 		= "sales-target";
		$this->model 			= new Sales_target;
		// $this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [4];
		$this->image_path 		= 'components/both/images/sales_target/';
		$this->data['image_path'] 	= $this->image_path;
		$this->merchant_id		= '';

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'user_id',
				'label' => 'Sales Name',
				'sorting' => 'y',
				'search' => 'text',
				'search_data' => $this->get_user_sales(),
				'belongto' => ['method' => 'user','field' => 'name']
			],
			[
				'name' => 'outlet_name',
				'label' => 'Outlet Name',
				'sorting' => 'y',
				'search' => 'text',
			],
			[
				'name' => 'start_date',
				'label' => 'Start Date',
				'sorting' => 'y',
				'search' => 'text',

			],
			[
				'name' => 'end_date',
				'label' => 'End Date',
				'sorting' => 'y',
				'search' => 'text',
			],
			[
				'name' => 'status',
				'label' => 'Status',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];


		$this->model = $this->model->join('user','user.id','sales_target.user_id')->join('outlet','outlet.id','user.outlet_id')->select('sales_target.*', 'user.name as user_name', 'outlet.outlet_name as outlet_name');
		return $this->build('index');

		// global
		// $this->data['user'] = $this->get_user();
		// return $this->render_view('pages.user.index');
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		$this->data['user'] = User::join('outlet', 'outlet.id', 'user.outlet_id')->where('user_access_id',3)->select('user.*', 'outlet.outlet_name')->get();
		// $this->data['outlet'] = Outlet::where('status','y')->get();

		return $this->render_view('pages.sales_target.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'target'				=> 'required',
			'start_date'			=> 'required',
			'end_date'				=> 'required'
		]);

		// $outlet_value = "";
		// if($request->user_access_id != '2'){
		// 	foreach($request->outlet as $key => $o){
		// 		if($key == 0) $outlet_value .= ";";
		// 		$outlet_value .= $o.";";
		// 	}
		// }

		$this->model->user_id 			= $request->user_id;
		$this->model->target 			= $this->decode_rupiah($request->target);
		$this->model->status 			= 'y';
		$this->model->start_date 		= date_format(date_create($request->start_date),'Y-m-d');
		$this->model->end_date 			= date_format(date_create($request->end_date), 'Y-m-d');
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create sales target');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['user'] = $this->model->find($id);
		$this->data['title'] = 'View '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] = $this->get_user_access();

		$this->data['ouser'] = "";
		$oulet_id = explode(";", $this->data['user']->outlet_id);
		$i = 0;
		foreach($oulet_id as $key => $o){
			if($o != ""){
				$oname = Outlet::find($o)->outlet_name;
				if($i != 0) $this->data['ouser'] .= ", ";
				$this->data['ouser'] .= $oname;

				$i++;
			}
		}

		return $this->render_view('pages.user.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['data'] 	= $this->model->join('user', 'user.id', 'sales_target.user_id')->select('sales_target.*', 'user.name')->find($id);
		$this->data['title'] 	= 'Edit '.$this->title.' '.$this->data['data']->username;	
		$this->data['user'] 	= User::join('outlet', 'outlet.id', 'user.outlet_id')->where('user_access_id',3)->select('user.*', 'outlet.outlet_name')->get();
		// dd($this->data['user']);
		return $this->render_view('pages.sales_target.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'target'				=> 'required',
			'start_date'			=> 'required',
			'end_date'				=> 'required'
		]);

		$this->model 					= $this->model->find($id);
		$this->model->user_id 			= $request->user_id;
		$this->model->target 			= $this->decode_rupiah($request->target);
		$this->model->status 			= 'y';
		$this->model->start_date 		= date_format(date_create($request->start_date),'Y-m-d');
		$this->model->end_date 			= date_format(date_create($request->end_date), 'Y-m-d');
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully update Sales Target');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}

	public function get_user_sales(){
		$q = $this->build_array(User::where('user_access_id',3)->get(),'id','name');
		return $q;
	}
}
