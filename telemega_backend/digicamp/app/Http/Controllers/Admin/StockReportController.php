<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use Carbon\Carbon;

use digipos\models\Outlet;
use digipos\models\Product_adjustment;
use digipos\models\Product;

use digipos\Libraries\Alert;
use digipos\Libraries\Timeelapsed;
use Illuminate\Http\Request;

class StockReportController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Laporan Stok Oulet";
		$this->root_url			= "report/stock-report";
		$this->root_link 		= "stock-report";
		$this->model 			= new Product_adjustment;
		$this->product 			= new Product;
		$this->outlet 			= new Outlet;
		$this->data['root_url']	= $this->root_url;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->data['title'] 	= $this->title;
		$this->data['data1'] 	= $this->model->get();
		$this->data['product'] 	= Product::where('status', 'y')->get();
		return $this->render_view('pages.reports.stock_outlet');
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function filter($request){
		$query = $this->model->join('outlet', 'outlet.id', 'product_adjustment.outlet_id')->join('product', 'product.id', 'product_adjustment.product_id');

		$product 	= $this->product->where('status', 'y');
		$outlet 	= $this->outlet->where('status', 'y')->get();

		if($request->input('search_date_from') != ""){
			// dd($request->input('search_date_from'));
			$search_date_from = date('d-m-Y', strtotime($request->input('search_date_from') . ' -1 day'));
			$search_from = $this->displayToSql($search_date_from);
			// dd($search_from);
			$query->whereDate('product_adjustment.created_at', '>=', $search_from);
		}

		if($request->input('search_date_to') != ""){
			$search_to = $this->displayToSql($request->input('search_date_to'));
			$query->whereDate('product_adjustment.created_at', '<=', $search_to);
		}

		$count_product = 0;
		if($request->input('product_id') != ""){
			if(in_array('0', $request->input('product_id'))){
				$product2= $product->get();
				$count_product = count($this->product->get()) * 3;
			}else{
				$count_product = count($request->input('product_id')) * 3;
				$query->whereIn('product_adjustment.product_id', $request->input('product_id'));
				$product2 = $product->whereIn('product.id', $request->input('product_id'))->get();
			}
		}else{
			return '<div style="text-align:center;color:red;">Please select product !</div>';
		}

		$display = "";
		$get_last = "";
		
		$query2 = $query->select('product_adjustment.*', 'product.product_name as product_name', 'outlet.outlet_name as outlet_name')->get();
		// $query2 = $query->select('product_adjustment.*', 'product.product_name as product_name', 'outlet.outlet_name as outlet_name')->pluck('created_at', 'id')->toArray();

		// dd($query2);

		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Outlet</th>
		                  <th class="bg-blue-madison font-white" colspan="'.$count_product.'" style="text-align:center;">Product</th>
		                </tr>
		              </thead>
		              <tbody>';
		// dd($order);  
		$display .= '<tr><td></td>';

		foreach($product2 as $p2){
			$display .= '<td style="text-align:center;font-weight: bold;" colspan="3">'.$p2->product_name.'</td>';
		}
		$display .= '</tr>';

		$display .= '<tr><td></td>';

		foreach($product2 as $p2){
			$display .= '<td style="font-weight: bold;">Stock</td>';
			$display .= '<td style="font-weight: bold;">Out</td>';
			$display .= '<td style="font-weight: bold;">remaining</td>';
		}
		$display .= '</tr>';

		foreach($outlet as $key => $o){
			$display .= '<tr><td style="font-weight: bold;">'.$o->outlet_name.'</td>';

			foreach($product2 as $p2){
				$flagFirstStock = 0;
				$firstStock 	= 0;
				$stockOut 		= 0;
				$remainingStock = 0;
				$id_product 	= $p2->id;
				foreach($query2 as $q2){
					if($q2->outlet_id == $o->id && $q2->product_id == $p2->id){
						if($flagFirstStock == 0){
							// var_dump('outlet_stock_total: '.$q2->product_name.' - '.$q2->outlet_stock_total);
							$firstStock 	= $q2->outlet_stock_total;
							$remainingStock = $q2->outlet_stock_total;
							$stockOut 		= $remainingStock - $firstStock;
							$flagFirstStock = 1;
						}elseif($flagFirstStock == 1){
							if($q2->qty > 0){
								$firstStock += $q2->qty;
							}
							$remainingStock = $q2->outlet_stock_total;
							$stockOut 		= $remainingStock - $firstStock;
						}
					}
				}
				$display .= '<td id="stock-'.$id_product.'">'.$firstStock.'</td>
								<td id="stock_out-'.$id_product.'">'.$stockOut.'</td>
								<td id="remaining_stock-'.$id_product.'">'.$remainingStock.'</td>';
			}
			$display .= '</tr>';
		}



		$display .='</tbody>
					</table>
					</div>';

		return $display;
	}
}
