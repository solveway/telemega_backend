<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Adjustemnt;
use digipos\models\Province;
use digipos\models\Category_product;
use digipos\models\Subcategory_product;
use digipos\models\Product_type;
use digipos\models\Product_package_adjustment;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ProductController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Product";
		$this->data['title']	= $this->title;
		$this->root_link 		= "manage-product";
		$this->model 			= new Product;

		$this->bulk_action			= true;
		$this->bulk_action_data 	= [1];
		$this->image_path 			= 'components/both/images/product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->data['product_type'] = Product_type::orderBy('product_type_name', 'desc')->get();

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			// [
			// 	'name' => 'images',
			// 	'label' => 'Image',
			// 	'type' => 'image',
			// 	'file_opt' => ['path' => $this->image_path, 'custom_path_id' => 'y']
			// ],
			[
				'name' 		=> 'product_name',
				'label' 	=> 'Product Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			// [
			// 	'name' 		=> 'province_name',
			// 	'label' 	=> 'Province Name',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		// $this->model = $this->model->join('province', 'province.id', 'product.province_id')->join('product_package_adjustment', 'product_package_adjustment.product_id', 'product.id')->join('outlet', 'outlet.id', 'product_package_adjustment.outlet_id')->select('product.*', 'province.name as province_name', 'outlet.outlet_name as outlet_name', 'product_package_adjustment.outlet_id as outlet_id');
		// dd($this->model->get());
		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 						= "Create product";
		$this->data['unit']  						= $this->unit;
		$this->data['province']						= Province::get();
		$this->data['category_product']				= Category_product::where([['status', 'y'],['id', '!=', '0']])->get();
		$this->data['subcategory_product']			= Subcategory_product::where([['status', 'y'],['id', '!=', '0']])->get();
		$this->data['outlet']						= Outlet::where([['status', 'y'],['outlet_pusat', 0]])->get();

		return $this->render_view('pages.product.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'name' 		=> 'required|unique:product,product_name',
			'price' 	=> 'required',
		],[
            'name.required' => 'Product name is Required.',
            'name.unique' 	=> 'Product name has already been taken.',
        ]);

		$last_id 							= $this->model->orderBy('id', 'desc')->first();
		$curr_id 							= ($last_id ? $last_id->id + 1 : 1);
		$this->model->product_name			= $request->name;
		$this->model->description			= $request->description;
		$this->model->province_id			= ($request->product_type == 1 ? null : $request->province);
		$this->model->barcode				= $request->no_barcode;
		$this->model->subcategory_product_id 	= $request->subcategory_product;
		$this->model->product_type_id 		= $request->product_type;
		// $this->model->category_product_id	= $request->category_product;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->valid_date			= date_format(date_create($request->date),'Y-m-d');
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		// ($request->daily_report == 'y' ? $this->model->flag_daily_report = 'y' : $this->model->flag_daily_report = 'n');
		// dd($this->image_path.$curr_id.'/');
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$curr_id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();

		$ppa 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where('product_package_adjustment.id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where('product.product_type_id' ,1)->orderBy('Product_package_adjustment.id', 'desc')->first();
		// dd($ppa);
		//check if product_id exist in table
		$qty 						= ($request->product_type == 2 ? 1 : 0);
		$outlet_stock_total 		= ($request->product_type == 2 ? 1 : 0);
		$global_stock_total 		= ($request->product_type == 2 ? 1 : $ppa->global_stock_total);
		$temp_pp_adjustment[]	= [
			'outlet_id'  			=> $request->outlet,
			'package_id' 			=> null,
			'product_id' 			=> $this->model->id,
			'package_big_id'		=> null,
			'qty' 	 	 			=> $qty,
			'outlet_stock_total' 	=> $outlet_stock_total,
			'global_stock_total' 	=> $global_stock_total,
			'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
			'created_at' 			=> Carbon::now(),
			'updated_at' 			=> Carbon::now(),
			'type' 					=> 'add_product'
		];

		if(count($temp_pp_adjustment) > 0){
			//insert Product_package_adjustment
			Product_package_adjustment::insert($temp_pp_adjustment);
		}

		Alert::success('Successfully add new Product');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		// dd($this->model);		
		$this->data['title'] 				= "Edit Product ".$this->model->product_name;
		$this->data['unit']  				= $this->unit;
		$this->data['data']  				= $this->model;
		// dd($this->data['data']->subcategory_product_id);		
		$this->data['province']				= Province::get();
		$this->data['category_product']		= Category_product::where([['status', 'y'],['id', '!=', '0']])->get();
		$this->data['subcategory_product']	= Subcategory_product::where([['status', 'y'],['id', '!=', '0']])->whereNotIn('id', [1,2])->get();
		$this->data['dompul'] 				= ($this->model->subcategory_product_id == 1 ? '1' : '0');
		$this->data['dompul_reg'] 			= ($this->model->subcategory_product_id == 2 ? '1' : '0');
		$this->data['outlet']				= Outlet::where([['status', 'y'],['outlet_pusat', 0]])->get();																																												

		return $this->render_view('pages.product.edit');
	}

	public function update(Request $request, $id){
		// dd($this->model->get());
		// dd($id);
		$this->model 						= $this->model->find($id);
		if($this->model->id != 3){
			$this->validate($request,[
				'name' 		=> 'required|unique:product,product_name,'.$id,
				'price' 	=> 'required',
			]);
		}
		

		$this->model->product_name			= $request->name;
		$this->model->description			= $request->description;
		$this->model->price					= ($this->model->id != 3 ? $this->decode_rupiah($request->price) : 0);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;

		if($this->model->subcategory_product_id != 1 && $this->model->subcategory_product_id != 2){
			$this->model->province_id				= $request->province;
			$this->model->barcode					= $request->barcode;
			$this->model->subcategory_product_id 	= $request->subcategory_product;
			$this->model->product_type_id 			= $this->model->product_type_id;
			$this->model->valid_date				= date_format(date_create($request->date),'Y-m-d');
		}else{
			// $this->model->province_id				= null;
			// $this->model->barcode					= null;
			// // $this->model->product_type_id 			= $this->model->product_type_id;
			// $this->model->valid_date				= date_format(date_create($request->date),'Y-m-d');
		}

		if($request->input('remove-single-image-image') == 'y'){
			if($this->model->images != NULL){
				File::delete($this->image_path.$this->model->id.'/'.$this->model->images);
				$this->model->images = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();
		// $this->increase_version();
		
		Alert::success('Successfully edit Product');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['dompul'] 				= ($this->model->subcategory_product_id == 1 ? '1' : '0');
		$this->data['dompul_reg'] 			= ($this->model->id == 2 ? '1' : '0');

		if($this->data['dompul'] == '0'){
			$this->data['data']  			= $this->model->join('province', 'province.id', 'product.province_id')->join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->join('product_type', 'product_type.id', 'product.product_type_id')->select('product.*', 'province.name as province_name', 'subcategory_product.subcategory_product_name as subcategory_product_name', 'product_type.product_type_name')->where('product.id', $id)->first();
		}else{
			$this->data['data']  			= $this->model->join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->join('product_type', 'product_type.id', 'product.product_type_id')->select('product.*', 'subcategory_product.subcategory_product_name as subcategory_product_name', 'product_type.product_type_name')->where('product.id', $id)->first();
		}
		
		// dd($this->data['data']);
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
