<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Product_package_adjustment;
use digipos\models\Province;
use digipos\models\Package;
use digipos\models\Package_product;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ProductAdjustmentController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Adjustment";
		$this->data['title']	= $this->title;
		$this->root_link 		= "adjustment";
		$this->serial_product 	= Product::join('package_product', 'package_product.id', 'product.id')->join('package', 'package.id', 'package_product.package_id')->select('product.*', 'product.description as product_description','package.package_name', 'package.barcode as package_barcode', 'package.description as package_description', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id', 'package_product.status as package_product_status');
		$this->outlet 			= new Outlet;
		$this->model 			= $this->outlet;

		$this->bulk_action			= false;
		// $this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];
		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			// [
			// 	'name' => 'images',
			// 	'label' => 'Image',
			// 	'type' => 'image',
			// 	'file_opt' => ['path' => $this->image_path, 'custom_path_id' => 'y']
			// ],
			[
				'name' 		=> 'outlet_name',
				'label' 	=> 'Outlet Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			// [
			// 	'name' 		=> 'product_name',
			// 	'label' 	=> 'Product Name',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'package_description',
			// 	'label' 	=> 'Package Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'product_description',
			// 	'label' 	=> 'Product Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'description',
			// 	'label' 	=> 'Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'barcode',
			// 	'label' 	=> 'Barcode',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'status',
			// 	'label' 	=> 'Status',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name'			=> 'updated_at',
			// 	'label'			=> 'Adjustment Datetime',
			// 	'belongto'		=> ['method' => 'product_adjustment', 'field' => 'updated_at'],
			// 	'sorting' 		=> 'y',
			// 	// 'search'		=> 'select',
			// 	// 'search_data' 	=> $st_order,
			// ]
		];

		// dd($this->model->get());
		$this->model = $this->model->join('province', 'province.id', 'outlet.province_id')->where('outlet.status', 'y')->select('outlet.*');
		// dd($this->model->get());
		return $this->build('index');
	}

	public function create(){
		
		// $this->data['title'] 			= "Create product";
		// $this->data['unit']  			= $this->unit;
		// // $this->data['province']		= Province::get();

		// return $this->render_view('pages.product.create');
	}

	public function store(Request $request){
		// $this->validate($request,[
		// 	'name' 		=> 'required|unique:product,product_name',
		// ]);

		// $this->model->product_name			= $request->name;
		// $this->model->description			= $request->description;
		// $this->model->unit					= $request->unit;
		// $this->model->price					= $this->decode_rupiah($request->price);
		// $this->model->status 				= 'y';
		// $this->model->upd_by 				= auth()->guard($this->guard)->user()->id;

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// // dd($this->model);
		// $this->model->save();

		// Alert::success('Successfully add new Outlet');
		// return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Product Package Adjustment ".$this->model->product_name;
		$this->data['data']  					= $this->model;
		// $this->data['adjust']  				= Product_package_adjustment::join('outlet', 'outlet.id', 'product_package_adjustment.outlet_id')->join('product', 'product.id', 'product_package_adjustment.product_id')->join('package', 'package.id', 'product_package_adjustment.package_id')->select('product_package_adjustment.*', 'outlet.outlet_name', 'product.product_name', 'package.package_name', 'package.package_id')->where('outlet_id', $id)->orderBy('id', 'desc')->get();

		/*get product serial*/
		// get all product, with outlet_stock_total > 0
		$this->data['product'] 				= Product::leftJoin('product_type', 'product_type.id', 'product.product_type_id')->where([['product.status', 'y']])->Join('product_package_adjustment', 'product_package_adjustment.product_id', 'product.id')->where([['product_type_name','Serial'],['product_package_adjustment.outlet_id',$id],['product_package_adjustment.outlet_stock_total', '>', '0']])->select('product.*', 'product_package_adjustment.product_id', 'product_package_adjustment.package_id')->get(); 

		//get product outlet_id, from Product_package_adjustment
		$this->data['serial_product']  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->whereIn('product.id', function($q)use($id){$q->select('product_id')->from('product_package_adjustment')->where([['outlet_id', $id], ['package_id', null], ['package_big_id', null], ['outlet_stock_total', '>', 0]]);})->select('product.*')->where([['product_type_name','Serial']])->get();

		// get product outlet_id, from Product_package_adjustment
		$package_product_exist 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where([['outlet_id', $id],['package_big_id', null], ['package_id', null],['product_package_adjustment.product_id', '!=',null],['product_type_id', 2], ['outlet_stock_total', '>',0]])->orderBy('package_big_id', 'asc')->pluck('product_package_adjustment.product_id')->toArray();
		
		$this->data['package_product_exist'] 	= (count($package_product_exist) > 0 ? $package_product_exist : []);
		/*end get product serial*/

		/*get package small from product serial*/
		//get package which has product and package_id = 0
		$package_small  					= Package::join('package_product', 'package_product.package_id', 'package.id')->where([['package_product.status', 'y'], ['package.package_id', 0],['package.status', 'y']])->select('package.*', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id')->groupBy('package_product.package_id');
		// dd($package_small->get());
		$this->data['package_small']  		= $package_small->get();

		$package_small_exist 	= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where([['outlet_id', $id],['package_big_id', null], ['package_id', '!=',null], ['package_id', '!=',null],['product_type_id', 2], ['outlet_stock_total', '>',0]])->orderBy('package_id', 'asc')->groupBy('package_id')->pluck('product_package_adjustment.package_id')->toArray();

		$this->data['package_small_exist'] 	= (count($package_small_exist) > 0 ? $package_small_exist : []);
		/*end get package small from product serial*/

	
		//get package which has package_id, join with package product
		$this->data['package_small2']  		= Package::join('package_product', 'package_product.package_id', 'package.id')->where([['package.status', 'y'], ['package_product.status', 'y'],['package.package_id', '!=',0]])->select('package.*', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id')->groupBy('package_product.package_id')->orderBy('package_product.package_id')->get();
		
		/*get package big*/
		//get parent package which package has product
		$this->data['package_big']  		= Package::where([['status', 'y'],['package_id', '0']])->whereNotIn('id', $package_small_exist)->get();
		// dd($this->data['package_big']);
		$package_big_exist 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where([['outlet_id', $id],['package_big_id', '!=',null], ['package_id', '!=',null],['product_package_adjustment.product_id', '!=',null],['product_type_id', 2], ['outlet_stock_total', '>',0]])->orderBy('package_big_id', 'asc')->groupBy('package_big_id')->pluck('product_package_adjustment.package_big_id')->toArray();

		$this->data['package_big_exist'] 		= (count($package_big_exist) > 0 ? $package_big_exist : []);
		/*end get package big*/

		$this->data['productNoStock'] 			= Product_package_adjustment::where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id and outlet_id = $id)"))->where([['outlet_stock_total','<',1]])->pluck('product_id')->toArray();	
		
		// qty product
		$this->data['qty_product']  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity']])->select('product.*')->get();
		
		$this->data['qty_product_outlet']  		= Product::Join('product_package_adjustment', 'product_package_adjustment.product_id', 'product.id')->join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity'],['product_package_adjustment.outlet_id', $id]])->select('product.*', 'product_package_adjustment.outlet_stock_total')->where('Product_package_adjustment.id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->get();
		// dd($this->data['qty_product_outlet']);
		return $this->render_view('pages.product_adjustment.edit');
	}

	public function update(Request $request, $id){
		// $this->validate($request,[
		// 	'buying_price' 		=> 'required',
		// ]);
		
		// if($request->buying_price == 'NaN'){
		// 	Alert::fail('Buying Price must numeric !');
		// 	return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		// }

		
		// $this->model 				= $this->model->find($id);
		$productSerial					= $request->product;
		$productQty						= $request->qty_product_adjust;
		$outlet_id						= $id;

		$upd_by 						= auth()->guard($this->guard)->user()->id;

		// $product_adjustment  			= Product_adjustment::where('id', DB::raw("(select max(`id`) from ni_product_adjustment npa where outlet_id = ni_product_adjustment.outlet_id)"))->where('product_adjustment.product_id', $id)->orderBy('id', 'desc')->groupBy('outlet_id')->get();
		// // dd($adjust);
		// $data = [];

		// $temp_arr = [];
		// $global_stock_total_prev 				= 0;
		// $global_stock_total_prev2 				= 0;
		// $global_buying_price_average_prev 		= 0;
		// $global_buying_price_average_prev2 		= 0;
		// $flagStatus 							= 0;

		// $outlet_stock_total 			= 0;
		// $qty 							= 0;
		// $global_stock_total				= 0;
		// $outlet_buying_price_average 	= 0;
		// $global_buying_price_average 	= 0;
		// $buying_price2					= 0;
		// $buying_price_prev				= 0;
		// $flagFilledOutlet 				= 0;

	
		// product serial
		$packageBigArr 		= [];
		$packageSmallArr 	= [];
		$productSerialArr 	= [];
		$temp_package_big_id = [];
		// dd($productSerial);

		//get array package_big, package_id and product_id
		if($productSerial){
			//get package product which has this outlet id from product_package_adjustment
			// $packageProductAdjustment 				= Product_package_adjustment::where('outlet_id', $outlet_id);
			
			// if(count($packageProductAdjustment->get()) > 0){
			// 	Product_package_adjustment::where('outlet_id', $outlet_id)->delete();
			// }	

			foreach ($productSerial as $key => $dy) {
				// dd($dy);
				$ps 	= explode("-",$dy);

				if($ps[0] == 'package_big'){
					array_push($packageBigArr, $ps[1]);
				}

				if($ps[0] == 'package_small'){
					array_push($packageSmallArr, $ps[1]);
				}

				if($ps[0] == 'kartu_perdana'){
					array_push($productSerialArr, $ps[1]);
				}
			}
		}

		$packageBigArr2 = [];
		$packageSmallArr2 = [];
		$productSerialArr2 = [];

		$packageBigArr2 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where('Product_package_adjustment.product_id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where([['outlet_stock_total','>',0],['product.product_type_id', 2], ['package_big_id', '!=', null],['outlet_id',$outlet_id]])->orderBy('package_big_id')->select('product_package_adjustment.*', 'product.product_type_id');
		if(count($packageBigArr) > 0){
			$packageBigArr2 		= $packageBigArr2->whereNotIn('package_big_id', $packageBigArr);
		}

		$packageSmallArr2 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where('Product_package_adjustment.product_id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where([['outlet_stock_total','>',0],['product.product_type_id', 2], ['package_big_id', null], ['package_id', '!=', null],['outlet_id',$outlet_id]])->orderBy('package_big_id')->select('product_package_adjustment.*', 'product.product_type_id');
		if(count($packageSmallArr) > 0){
			$packageSmallArr2 	= $packageSmallArr2->whereNotIn('package_id', $packageSmallArr);
		}

		$productSerialArr2 		= Product_package_adjustment::join('product', 'product.id', 'product_package_adjustment.product_id')->where('Product_package_adjustment.product_id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = te_product_package_adjustment.product_id)"))->where([['outlet_stock_total','>',0],['product.product_type_id', 2], ['package_big_id', null], ['package_id',null],['outlet_id',$outlet_id]])->orderBy('package_big_id')->select('product_package_adjustment.*', 'product.product_type_id');
		if(count($productSerialArr) > 0){
			$productSerialArr2 		= $productSerialArr2->whereNotIn('product_id', $productSerialArr);
		}
		// dd($packageBigArr2->get());
		// dd($packageSmallArr2);
		// dd($productSerialArr2);

		// dd($productLastId);
		$temp_package_big = [];
		$curr_package_id = '';
		$temp_package_big2 = [];
		$temp_package_small2 = [];
		$temp_package_product = [];
		if(count($packageBigArr2->get()) > 0){
			// $packageBigArr2_1 = $packageBigArr2->groupBy('package_id')->get();
			// Package_product::whereIn('package_id', )
			
			foreach($packageBigArr2->get() as $val1){
				$temp_package_big[] =[
					'outlet_id'  			=> $outlet_id,
					'package_id' 			=> $val1->package_id,
					'product_id' 			=> $val1->product_id,
					'package_big_id'		=> $val1->package_big_id,
					'qty' 	 	 			=> -1,
					'outlet_stock_total' 	=> 0,
					'global_stock_total' 	=> 0,
					'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
					'created_at' 			=> Carbon::now(),
					'updated_at' 			=> Carbon::now(),
					'type' 					=> 'adjust'
				];

				if($curr_package_id != $val1->package_id){
					array_push($temp_package_big2, $val1->package_big_id);
					array_push($temp_package_small2, $val1->package_id);
					// Package_product::where('package_id', $val1->package_id)->update(['status' => 'n']);
					// Package::where('id', $val1->package_id)->update(['status' => 'n']);
					// Package::where('id', $val1->package_big_id)->update(['status' => 'n']);
					$curr_package_id = $val1->package_id;
				}
			}
		}

		// dd($temp_package_big);
		$temp_package_small = [];
		$curr_package_id = '';
		if(count($packageSmallArr2->get()) > 0){
			foreach($packageSmallArr2->get() as $val2){
				$temp_package_small[] =[
					'outlet_id'  			=> $outlet_id,
					'package_id' 			=> $val2->package_id,
					'product_id' 			=> $val2->product_id,
					'package_big_id'		=> $val2->package_big_id,
					'qty' 	 	 			=> -1,
					'outlet_stock_total' 	=> 0,
					'global_stock_total' 	=> 0,
					'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
					'created_at' 			=> Carbon::now(),
					'updated_at' 			=> Carbon::now(),
					'type' 					=> 'adjust'
				];

				if($curr_package_id != $val2->package_id){
					array_push($temp_package_small2, $val1->package_id);
					// Package_product::where('package_id', $val2->package_id)->update(['status' => 'n']);
					// Package::where('id', $val2->package_id)->update(['status' => 'n']);
					$curr_package_id = $val2->package_id;
				}
			}
		}
		// dd($temp_package_small);

		$temp_product_serial = [];
		if(count($productSerialArr2->get()) > 0){
			foreach($productSerialArr2->get() as $val3){
				$temp_product_serial[] =[
					'outlet_id'  			=> $outlet_id,
					'package_id' 			=> $val3->package_id,
					'product_id' 			=> $val3->product_id,
					'package_big_id'		=> $val3->package_big_id,
					'qty' 	 	 			=> -1,
					'outlet_stock_total' 	=> 0,
					'global_stock_total' 	=> 0,
					'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
					'created_at' 			=> Carbon::now(),
					'updated_at' 			=> Carbon::now(),
					'type' 					=> 'adjust'
				];
			}
		}
		// dd($temp_product_serial);

		$temp_pp_adjustment = [];
		if($productQty){
			$qty_product  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity']])->select('product.*')->get();

			if($qty_product){
				foreach ($qty_product as $key => $val4) {
					if($productQty[$key] != null){
						$ppa_last 				= Product_package_adjustment::where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where product_id = $val4->id)"))->where('outlet_id', $outlet_id)->orderBy('id', 'desc')->groupBy('outlet_id')->first();
						$last_outlet_stock_total = 0;
						$last_global_stock_total = 0;
						if($ppa_last != null){
							$last_outlet_stock_total = $ppa_last->outlet_stock_total;
							$last_global_stock_total = $ppa_last->global_stock_total;
						}
						$newQty  				= $productQty[$key] - $last_outlet_stock_total;
						$newGst  				= $newQty + $last_global_stock_total;
						// // dd($ppa_last);
						// $package_id 			= null;
						// $product_id 			= $val4->id;
						// $package_big_id 		= null;
						// $qty 					= $newQty;
						// $outlet_stock_total 	= $productQty[$key];
						// $global_stock_total 	= $newGst;
						// $this->insertProductPackageAdjustment($outlet_id, $package_id, $product_id, $package_big_id, $qty, $outlet_stock_total, $global_stock_total);

						$temp_pp_adjustment[]	= [
							'outlet_id'  			=> $outlet_id,
							'package_id' 			=> null,
							'product_id' 			=> $val4->id,
							'package_big_id'		=> null,
							'qty' 	 	 			=> $newQty,
							'outlet_stock_total' 	=> $productQty[$key],
							'global_stock_total' 	=> $newGst,
							'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
							'created_at' 			=> Carbon::now(),
							'updated_at' 			=> Carbon::now(),
							'type' 					=> 'adjust'
						];
					}
				}
			}
		}

		if(count($temp_package_big) > 0){
			Product_package_adjustment::insert($temp_package_big);
		}

		if(count($temp_package_small) > 0){
			Product_package_adjustment::insert($temp_package_small);
		}

		if(count($temp_product_serial) > 0){
			Product_package_adjustment::insert($temp_product_serial);
		}

		if(count($temp_package_big2)){
			Package::whereIn('id', $temp_package_big2)->update(['status' => 'n']);
		}

		if(count($temp_package_small2)){
			Package_product::where('package_id', $temp_package_small2)->update(['status' => 'n']);
			Package::where('id', $temp_package_small2)->update(['status' => 'n']);
		}
		
		// dd($temp_pp_adjustment);
		if(count($temp_pp_adjustment) > 0){
			//insert Product_package_adjustment
			Product_package_adjustment::insert($temp_pp_adjustment);
		}

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}

	public function insertProductPackageAdjustment($outlet_id, $package_id, $product_id, $package_big_id, $qty, $outlet_stock_total, $global_stock_total){
		$temp_pp_adjustment[]	= [
			'outlet_id'  			=> $outlet_id,
			'package_id' 			=> $package_id,
			'product_id' 			=> $product_id,
			'package_big_id'		=> $package_big_id,
			'qty' 	 	 			=> $qty,
			'outlet_stock_total' 	=> $outlet_stock_total,
			'global_stock_total' 	=> $global_stock_total,
			'upd_by'	 			=> auth()->guard($this->guard)->user()->id,
			'created_at' 			=> Carbon::now(),
			'updated_at' 			=> Carbon::now(),
			'type' 					=> 'adjust',
		];
		// dd($temp_pp_adjustment);
		if(count($temp_pp_adjustment) > 0){
			//insert Product_package_adjustment
			Product_package_adjustment::insert($temp_pp_adjustment);
		}
	}
}
