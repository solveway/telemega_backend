<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;
use digipos\models\Product;

class ProductController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function index(request $request,$id){
		//dd($request->session()->get('cart'));
		//$request->session()->flush();
		$this->data['product']				= Product::where('name_alias',$id)
												->with(['product_category.category','product_data_images' => function($query){
													$query->where('status','y');
												},'product_data_attribute_master.product_data_attribute.product_attribute_data.attribute'])
												->where('status','y')
												->first();
		$this->data['product_attribute']	= $this->generate_product_attribute($this->data['product']);
		return $this->render_view('pages.product');
	}

	public function addCart(request $request){
        $request->request->add(['action' => 'add']);
		$result 				= $this->check_stock($request);
		$status 				= $result['status'];
		$result['type']			= 'danger';
		if($status == 'no_product'){
			$result['text']		= 'Product not found';
		}else if($status == 'available'){
			$this->add_cart($request);
			$result['text']		= 'Product added to cart';
			$result['type']		= 'success';
		}else{
			$result['text']		= 'This attribute is out of stock';
		}
		return response()->json(['result' => $result]);
	}

	public function removeCart(request $request){
        $this->remove_cart($request);
        return response()->json(['status' => 'success']);
	}

	public function totalCart(){
		$total 	= $this->get_total_cart();
		return $total;
	}
}
