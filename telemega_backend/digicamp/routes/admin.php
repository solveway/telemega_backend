<?php
	Route::get('login','LoginController@index');
	Route::post('cek_login','LoginController@cek_login');
	Route::post('forgot_password','LoginController@forgot_password');
	Route::get('logout','LoginController@logout');
	
	Route::get('profile', 'LoginController@profile');
	Route::post('update_profile', 'LoginController@update_profile');
	Route::get('change-language/{id}','LoginController@change_language');
	Route::get('thanks_page','LoginController@thanks_page');

	//Main route
	Route::get('/', 'IndexController@index');

	Route::group(['prefix' => 'users'], function(){
		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');
		Route::get('user/get_store/{id}','UserController@get_store');

		Route::resource('access-right', 'UseraccessController');
		Route::any('access-right/ext/{action}','UseraccessController@ext');

		Route::resource('members', 'MembersController');
		Route::any('members/ext/{action}','MembersController@ext');
	});

	Route::group(['prefix' => 'outlets'], function(){
		Route::resource('manage-outlet', 'OutletController');
		Route::any('manage-outlet/ext/{action}','OutletController@ext');
	});

	Route::group(['prefix' => 'products'], function(){
		Route::resource('manage-product', 'ProductController');
		Route::any('manage-product/ext/{action}','ProductController@ext');

		Route::resource('package', 'PackageController');
		Route::any('package/ext/{action}','PackageController@ext');

		Route::resource('category-product', 'CategoryProductController');
		Route::any('category-product/ext/{action}','CategoryProductController@ext');

		Route::resource('subcategory-product', 'SubcategoryProductController');
		Route::any('subcategory-product/ext/{action}','SubcategoryProductController@ext');

		Route::resource('promo', 'PromoController');
		Route::any('promo/ext/{action}','PromoController@ext');
	});

	Route::group(['prefix' => 'stocks'], function(){
		Route::resource('adjustment', 'ProductAdjustmentController');
		Route::any('adjustment/ext/{action}','ProductAdjustmentController@ext');

		Route::resource('outlet-stock-transfer', 'OutletStockTransferController');
		Route::any('outlet-stock-transfer/ext/{action}','OutletStockTransferController@ext');

	});

	Route::group(['prefix' => 'account-&-payment'], function(){
		Route::resource('bank-account', 'BankAccountController');
		Route::any('bank-account/ext/{action}','BankAccountController@ext');
	});

	Route::group(['prefix' => 'news'], function(){
		Route::resource('manage-news', 'NewsController');
		Route::any('manage-news/ext/{action}','NewsController@ext');
	});

	Route::group(['prefix' => 'mobile'], function(){
		Route::resource('banner', 'BannerController');
		Route::any('banner/{ext}/{action}','BannerController@ext');

		Route::resource('slide-mobile', 'SlideshowController');
		Route::any('slide-mobile/ext/{action}','SlideshowController@ext');

		Route::resource('inbox', 'InboxController');
		Route::any('inbox/ext/{action}','InboxController@ext');

		// Route::get('manage-merchant/get_merchant_category/{par}', 'MerchantController@get_merchant_category');
		// Route::get('manage-merchant/get_merchant_location/{par}', 'MerchantController@get_merchant_location');
	});

	// Route::group(['prefix' => 'post'], function(){
	// 	Route::resource('manage-post', 'PostController');
	// 	Route::any('manage-post/ext/{action}','PostController@ext');

	// 	Route::resource('orders-customer', 'ReportCustomerController');
	// 	Route::any('orders-customer/ext/{action}','ReportCustomerController@ext');
	// });

	Route::group(['prefix' => 'report'], function(){
		Route::resource('stock-report', 'StockReportController');
		Route::any('stock-report/ext/{action}','StockReportController@ext');

		Route::resource('outlet-income-report', 'OutletIncomeReportController');
		Route::any('outlet-income-report/ext/{action}','OutletIncomeReportController@ext');
	});

	Route::group(['prefix' => 'administration'], function(){
		Route::resource('user-access', 'UseraccessController');
		Route::any('user-access/ext/{action}','UseraccessController@ext');

		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');

		Route::resource('customer', 'CustomerController');
		Route::any('customer/ext/{action}','CustomerController@ext');
		Route::get('customer/posting/{id}/{slug}', 'CustomerController@detail');
	});

	Route::group(['prefix' => 'preferences'], function(){
		Route::get('general-settings', 'ConfigController@index');
		Route::post('general-settings/update', 'ConfigController@update');
	});

	Route::group(['prefix' => 'sales'], function(){
		Route::resource('salesman-product', 'SalesmanProductController');
		Route::any('salesman-product/ext/{action}','SalesmanProductController@ext');

		Route::resource('sales-target', 'SalesTargetController');
		Route::any('sales-target/ext/{action}','SalesTargetController@ext');
	});

	//Main services for ajax outside builder
	Route::any('services','ServicesController@index');