@extends($view_path.'.layouts.master')
@push('css')
  <link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                @if($ticket->image != null && file_exists('components/both/images/movie/'.$ticket->image))
                                    <img src="{{asset('components/both/images/movie/'.$ticket->image)}}" class="img-responsive pic-bordered" alt="" />
                                @else
                                    <img src="{{asset('components/admin/image/default.jpg')}}" class="img-responsive pic-bordered" alt="" />
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-7 profile-info">
                                <h4 class="font-green sbold">{{$ticket->ticket_name}}</h4>
                                <p>
                                  {{$ticket->synopsis}}
                                  <br><br>
                                  {{$preorder->payment_methods->payment_method_name}} ({{$preorder->bank_info}})
                                  <br><br>
                                  @if($preorder->preorder_status == "1")
                                    <label class="label label-default">Waiting Payment</label>
                                  @elseif($preorder->preorder_status == "2")
                                    <label class="label label-info">Payment Confirmation</label>
                                  @elseif($preorder->preorder_status == "3")
                                    <label class="label label-primary">Ready for redeem</label>
                                  @elseif($preorder->preorder_status == "4")
                                    <label class="label label-success">Redeemed</label>
                                  @elseif($preorder->preorder_status == "11")
                                    <label class="label label-danger">Cancel</label>
                                  @endif
                                </p>
                                <ul class="list-inline">
                                    <li>
                                        <i class="fa fa-user"></i> {{$preorder->name}} </li>
                                    <li>
                                        <i class="fa fa-phone"></i> {{$preorder->phone}} </li>
                                    <li>
                                        <i class="fa fa-ticket"></i> {{number_format($preorder->total_ticket)}} </li>
                                    <li>
                                        <i class="fa fa-tags"></i> {{number_format($preorder->price)}} </li>
                                </ul>
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-5">
                              <div class="portlet sale-summary" style="box-shadow: none;">
                                  <div class="portlet-body">
                                      <ul class="list-unstyled">
                                          <li>
                                              <span class="sale-info"> Cinema
                                                  <i class="fa fa-img-down"></i>
                                              </span>
                                              <span class="sale-num"> {{$preorder->cinema_tickets->cinema->bioskop_name}} </span>
                                          </li>
                                          <li>
                                              <span class="sale-info"> Type
                                                  <i class="fa fa-img-down"></i>
                                              </span>
                                              <span class="sale-num"> {{$preorder->cinema_tickets->cinema_service->name}} </span>
                                          </li>
                                          <li>
                                              <span class="sale-info"> Showtime
                                                  <i class="fa fa-img-up"></i>
                                              </span>
                                              <span class="sale-num"> {{date("d M, H:i", strtotime($preorder->cinema_tickets->cinema_date." ".$preorder->cinema_tickets->cinema_time))}} </span>
                                          </li>
                                          <li>
                                              <span class="sale-info"> Preorder Time </span>
                                              <span class="sale-num"> {{date("d M, H:i", strtotime($preorder->preorder_date))}} </span>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
@endsection
@push('custom_scripts')

@endpush