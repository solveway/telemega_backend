@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$user->id}}" enctype="multipart/form-data">
	{{ method_field('PUT') }}
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.select',['name' => 'user_access_id','label' => 'User Access','value' => (old('user_access_id') ? old('user_access_id') : $user->user_access_id),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $user_access, 'class' => 'select2 user_access_id', 'onchange' => ''])!!}

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : $user->name),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'required' => 'y'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'username','label' => 'Username','value' => (old('username') ? old('username') : $user->username),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'required' => 'y'])!!}

				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password" value="">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>

				{!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $user->email),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				 <div class="col-md-6">
		          <div class="form-group form-md-line-input">
		            <input type="text" id="birth_date" class="form-control" name="birth_date" value="{{$user->birth_date != null ? date_format(date_create($user->birth_date),'d-m-Y') : ''}}" readonly="" placeholder="Valid To">
		            <label for="form_floating_Hqd">Birth Date <span class="" aria-required="true">*</span></label>
		            <small></small>
		          </div>
		        </div>

				{!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $user->description),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

			  	<div class="col-md-12">
	              <div class="form-group form-md-line-input">
	                <label>Outlet</label>
                    <select id="outlet" name="outlet" class="form-control select2">
						<optgroup label="Outlet">
						  @foreach($outlet as $o)
						  	<option value="{{$o->id}}" {{in_array($o->id, explode(";", $user->outlet_id)) ? "selected" : ""}}>{{$o->outlet_name}}</option>
						  @endforeach
						</optgroup>
                  	</select>
	              </div>
	            </div>

			  	<div class="form-group col-md-2">
				  	<div class="md-checkbox">
				  		<label>Login Web</label>
						<input type="checkbox" id="checkbox_form_1" class="md-check login_web" name="login_web" {{isset($user->login_backend) ? ($user->login_backend == 'y' ? 'checked' : '') : ''}} value="y" >
						<label for="checkbox_form_1">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div>

				<div class="form-group col-md-2">
					<div class="md-checkbox">
				  		<label>Login App</label>
						<input type="checkbox" id="checkbox_form_2" class="md-check login_app" name="login_app" {{isset($user->login_app) ? ($user->login_app == 'y' ? 'checked' : '') : ''}} value="y">
						<label for="checkbox_form_2">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div>

				<div class="form-group col-md-2">
					<div class="md-checkbox">
				  		<label>Sales Dompul</label>
						<input type="checkbox" id="checkbox_form_3" class="md-check sales_dompul" name="sales_dompul" {{isset($user->sales_dompul) ? ($user->sales_dompul == 'y' ? 'checked' : '') : ''}}  value="y">
						<label for="checkbox_form_3">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div>

				
			  	{!!view($view_path.'.builder.file',['name' => 'picture','label' => 'Picture','value' => $user->images,'type' => 'file','file_opt' => ['path' => $image_path.$user->id.'/'],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-12', 'required' => 'n'])!!}

  				<input type="hidden" id="root-url" value="{{$path}}" />
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");
			
			$('#generate-password').on('click', function(e){

	        	var randomstring = Math.random().toString(36).slice(-6);

		        password.val(randomstring);

		    });



		    $('#show-password').on('click', function(e){

		        if(password.attr("type") == "password"){

		            password.attr("type", "text");

		            $("#show-password").addClass("text-primary");

		            $("#show-password").removeClass("text-default");

		        }

		        else{

		            password.attr("type", "password");

		            $("#show-password").addClass("text-default");

		            $("#show-password").removeClass("text-primary");

		        }

		    });

		    $("#birth_date").datepicker({
		    	changeMonth: true,
		    	changeYear: true,
		    	dateFormat: 'dd-mm-yy',
		    	yearRange: "0:+90",
		    	showButtonPanel: true,

              	onSelect: function(dateText, inst) {

              	}

          	});

          	var user_access_id = $(".user_access_id");
          	if(user_access_id.val() == '2'){
          		$("#outlet").prop( "disabled", true );
          	}else{
          		$("#operator").removeAttr("disabled");
          	}

          	user_access_id.change(function(){
          		$("#outlet").prop( "disabled", true );
          		console.log(user_access_id.val());
          		if(user_access_id.val() != '2'){
	          		$("#outlet").removeAttr("disabled");
	          	}
          	});
		});
	</script>
@endpush
@endsection