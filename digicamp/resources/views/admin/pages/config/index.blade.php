@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>
        <div class="actions">
          <div class="actions">
            {!!view($view_path.'.builder.button',['type' => 'submit','label' => trans('general.submit')])!!}
          </div>
        </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#settings" data-toggle="tab" aria-expanded="true">{{trans('general.web-settings')}}</a>
          </li>
          <li>
            <a href="#dompul_reguler" data-toggle="tab" aria-expanded="true">Dompul Reguler</a>
          </li>  
          <!-- <li>
            <a href="#technical" data-toggle="tab" aria-expanded="true">{{trans('general.technical-settings')}}</a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="settings">
            <div class="row">
              {!!view($view_path.'.builder.text',['type' => 'email','name' => 'web_email','label' => 'Global Email','value' => (old('web_email') ? old('web_email') : $configs->web_email),'attribute' => 'required','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'web_name','label' => 'Web Name','value' => (old('web_name') ? old('web_name') : $configs->web_name),'attribute' => 'required','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.file',['name' => 'favicon','label' => 'Favicon','value' => $configs->favicon,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 30 x 30 px','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.file',['name' => 'web_logo','label' => 'Web Logo','value' => $configs->web_logo,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-6'])!!}              
            </div>
          </div>
          
          <!-- <div class="tab-pane" id="technical">
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'maintenance_mode','label' => 'Maintenance Mode','value' => (old('maintenance_mode') ? old('maintenance_mode') : $configs->maintenance_mode), 'form_class' => 'col-md-6 pad-left'])!!}
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Yes','n' => 'No'],'name' => 'clean_cache','label' => 'Clean cache','value' => 'n', 'form_class' => 'col-md-6 pad-right'])!!}
            <div class="clearfix"></div>
          </div> -->

          <div class="tab-pane active" id="dompul_reguler">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group form-md-line-input">
                  <input type="number" id="form_floating_mB0" class="form-control " name="discount" value="{{$dompul_reguler->discount}}" required="" placeholder="Discount Dompul" step="any" min="0" pattern="\d+(\.\d{2})?">
                  <label for="form_floating_mB0">Discount Dompul <span class="required" aria-required="true"></span></label>
                  <small></small>
                </div>
              </div>       
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  @if ($role->view == 'n')
    <script>
      $(document).ready(function(){
        // $('input,select,textarea').prop('disabled',true);
      });
    </script>
  @endif
@endpush
@endsection
