@extends($view_path.'.layouts.master')
@section('content')

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
  {{method_field('PUT')}}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
          <div class="col-md-6">
            <label for="tag">Salesman<span class="required no-margin-bottom" aria-required="true"></span></label>
            <div class="form-group form-md-line-input no-padding-top">
              <select class="select2" name="user_id" class="user_id" id="user_id" disabled="">
                  @foreach($user as $sc)
                      <option value="{{$sc->id}}" {{$data->user_id == $sc->id ? 'selected' : ''}}>{{$sc->name.' - '.$sc->outlet_name}}</option>
                  @endforeach
              </select>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="start_date" class="form-control" name="start_date" value="{{$data->start_date != null ? date_format(date_create($data->start_date),'d-m-Y') : ''}}" readonly="" placeholder="Start Date">
              <label for="form_floating_Hqd">Start Date<span class="" aria-required="true">*</span></label>
              <small></small>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="end_date" class="form-control" name="end_date" value="{{$data->end_date != null ? date_format(date_create($data->end_date),'d-m-Y') : ''}}" readonly="" placeholder="End Date">
              <label for="form_floating_Hqd">End Date<span class="" aria-required="true">*</span></label>
              <small></small>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
              <h4><b>Product Serial</b></h4>
            </div>
          </div>

          <div class="col-md-6">
            <label for="tag">Package / Product <span class="required no-margin-bottom" aria-required="true"></span></label>
            <div class="form-group form-md-line-input no-padding-top">
              <select class="select2" name="product[]" class="service_category" id="product" multiple="">
                  @foreach($serial_product as $sc)
                      @if(!in_array($sc->id, $productNoStock))
                        @php
                          if(count($serial_product_selected) > 0){
                            $flagSelected = 0;
                            foreach($serial_product_selected as $sp){
                              if(in_array($sc->id, explode(';', $sp))){
                                $flagSelected = 1;
                                break;
                              }
                            }
                          }
                        @endphp

                        @if($flagSelected == 0)
                          <option value="{{'kartu_perdana-'.$sc->id}}" {{in_array($sc->id, explode(';', $data->list_product)) ? 'selected' : ''}}>{{$sc->product_name}} {{'- barcode: '.$sc->barcode}}</option>
                        @endif
                      @endif
                  @endforeach

                  @foreach($package_small as $ps)
                      @php
                        $title = $ps->package_name.'- barcode: '.$ps->barcode.':&#13;';
                        if(count($product) > 0){
                          $flagProductExist = 0;
                          foreach($product as $key => $p){

                            if($p->package_id == $ps->id && !in_array($p->id, $productNoStock)){
                              $flagProductExist = 1;
                              $title .= '&nbsp;-'.$p->product_name.' - '.$p->package_id.'- barcode: '.$p->barcode.'&#13;';
                            }
                          }
                        }

                        if(count($serial_product_selected) > 0){
                          $flagSelected = 0;
                          foreach($package_selected as $sp){
                            if(in_array($ps->id, explode(';', $sp))){
                              $flagSelected = 1;
                              break;
                            }
                          }
                        }
                        
                      @endphp

                      @if($flagProductExist == 1 && $flagSelected == 0)
                        <option value="{{'package_small-'.$ps->id}}" title="{{$title}}" {{in_array($ps->id, explode(';', $data->list_package)) ? 'selected' : ''}}>{{$ps->package_name}} {{'- barcode: '.$ps->barcode}}</option>
                      @endif
                  @endforeach

                  @foreach($package_big as $pb)
                      @php
                        $flagProductExist = 0;
                        $title = $pb->package_name.'- barcode: '.$pb->barcode.':&#13;';
                        $packageList = '';
                        if(count($package_small2) > 0){
                          foreach($package_small2 as $key => $ps2){
                            if($pb->id == $ps2->package_id){
                              $title .= '&nbsp;-'.$ps2->package_name.'- barcode: '.$ps2->barcode.'&#13;';
                         
                              if(count($product) > 0){
                                $productList = ';';
                                foreach($product as $key2 => $p){
                                  if($p->package_id == $ps2->id && !in_array($p->id, $productNoStock)){
                                    $flagProductExist = 1;
                                    $title .= '&nbsp;&nbsp;--'.$p->product_name.'- barcode: '.$p->barcode.'&#13;';
                                  }
                                }
                              }
                            }
                          }
                        }
                        
                        if(count($serial_product_selected) > 0){
                          $flagSelected = 0;
                          foreach($package_selected as $sp){
                            if(in_array($ps->id, explode(';', $sp))){
                              $flagSelected = 1;
                              break;
                            }
                          }
                        }
                      @endphp

                      @if($flagProductExist == 1 && $flagSelected == 0)
                         <option value="{{'package_big-'.$pb->id}}" title="{{$title}}" {{in_array($ps->id, explode(';', $data->list_package)) ? 'selected' : ''}}>{{$pb->package_name}} {{'- barcode: '.$pb->barcode}}</option>
                      @endif
                  @endforeach
              </select>
              <br>
              <small>Note: Remove product / package will remove from stock</small>
            </div>
          </div>
      </div>

      <div class="row">
           

               <div class="col-md-12 actions">
                    {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}   
                </div>
          </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      // $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });

       // var price = $('.price');
       //  if(price.text() != '-'){
       //      console.log(price.text());
       //      price.text($.formatRupiah(price.text()));
       //  }

       //  var global_buying_price_average = $('.global_buying_price_average');
       //  // console.log($.formatRupiah(global_buying_price_average.val()));
       //  global_buying_price_average.val($.formatRupiah(global_buying_price_average.val()));

       $("#start_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

            });

            $("#end_date").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd-mm-yy',
          yearRange: "0:+90",
          showButtonPanel: true,

                onSelect: function(dateText, inst) {

                }

            });

    });

    $( ".buying_price" ).blur(function() {  
        // alert('test');
        //number-format the user input
        var val = $(this).val();
        var val2 = parseFloat(val.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val(val2);             
    });

    $( ".adjust" ).blur(function() {
        var val = $(this).val();
        console.log(val);
        if(val < 0){
          $(this).val(0);
        }
    });  

    $('.qty_product_transfer').keyup(function(){
        var id  = $(this).attr('id');
        id      = id.split('-');
        id      = id[1];
        var input = $('#qty_product_transfer-'+id).val();
        var outlet_stock_total = $('#outlet_stock_total-'+id).val();
        if(parseInt(input) > parseInt(outlet_stock_total)){
          $('#qty_product_transfer-'+id).val('');
        }else if(input == 0){
          $('#qty_product_transfer-'+id).val('');
        }
        // console.log(id+'-'+outlet_stock_total+'-'+input);
    })
  </script>
@endpush
