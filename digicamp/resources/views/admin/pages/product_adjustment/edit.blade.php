@extends($view_path.'.layouts.master')
@section('content')

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row"> 
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
              <h4><b>Product Serial</b></h4>
            </div>
          </div>

          <div class="col-md-6">
            <label for="tag">Package / Product <span class="required no-margin-bottom" aria-required="true"></span></label>
            <div class="form-group form-md-line-input no-padding-top">
              <select class="select2" name="product[]" class="service_category" id="product" multiple="">
                  @foreach($serial_product as $sc)
                      @if(!in_array($sc->id, $productNoStock))
                        <option value="{{'kartu_perdana-'.$sc->id}}" {{in_array($sc->id, $package_product_exist) ? "selected" : ""}}>{{$sc->product_name}} {{'- barcode: '.$sc->barcode}}</option>
                      @endif
                  @endforeach

                  @foreach($package_small as $ps)
                      @php
                        $flagProductExist = 0;
                        $title = $ps->package_name.'- barcode: '.$ps->barcode.':&#13;';
                        if(count($product) > 0){
                          foreach($product as $key => $p){

                            if($p->package_id == $ps->id && !in_array($p->id, $productNoStock)){
                              $flagProductExist = 1;
                              $title .= '&nbsp;-'.$p->product_name.' - '.$p->package_id.'- barcode: '.$p->barcode.'&#13;';
                            }
                          }
                        }
                        
                      @endphp

                      @if($flagProductExist == 1)
                        <option value="{{'package_small-'.$ps->id}}" title="{{$title}}" {{in_array($ps->id, $package_small_exist) ? "selected" : ""}}>{{$ps->package_name}} {{'- barcode: '.$ps->barcode}}</option>
                      @endif
                  @endforeach

                  @foreach($package_big as $pb)
                      @php
                        $flagProductExist = 0;
                        $title = $pb->package_name.'- barcode: '.$pb->barcode.':&#13;';
                        $packageList = '';
                        if(count($package_small2) > 0){
                          foreach($package_small2 as $key => $ps2){
                            if($pb->id == $ps2->package_id){
                              $title .= '&nbsp;-'.$ps2->package_name.'- barcode: '.$ps2->barcode.'&#13;';
                         
                              if(count($product) > 0){
                                $productList = ';';
                                foreach($product as $key2 => $p){
                                  if($p->package_id == $ps2->id && !in_array($p->id, $productNoStock)){
                                    $flagProductExist = 1;
                                    $title .= '&nbsp;&nbsp;--'.$p->product_name.'- barcode: '.$p->barcode.'&#13;';
                                  }
                                }
                              }
                            }
                          }
                        }
                       
                      @endphp

                      @if($flagProductExist == 1)
                         <option value="{{'package_big-'.$pb->id}}" title="{{$title}}" {{in_array($pb->id, $package_big_exist) ? "selected" : ""}}>{{$pb->package_name}} {{'- barcode: '.$pb->barcode}}</option>
                      @endif
                  @endforeach
              </select>
              <br>
              <small>Note: Remove product / package will remove from stock</small>
            </div>
          </div>
      </div>

      <div class="row">
          <div class="col-md-12">
            <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
              <h4><b>Product Quantity</b></h4>
            </div>
          </div>
          <div class="col-md-12">
              <table id="outlet_adjust" class="table table-bordered table-form">
                <thead>
                  <!-- <tr class="parent">
                    <td colspan="3" class="cinema_col" align="center"><b>Product Quantity</b></td>
                  </tr> -->
                  <tr class="">
                    <td class="" align=""><b>Product Name</b></td>
                    <td class="" align=""><b>{{trans('general.outlet')}} Stock Total</b></td>
                    <td class="" align=""><b>Adjust</b></td>
                  </tr>
                </thead>

                <tbody>
                  @foreach($qty_product as $c)
                    <tr id="qty_product_{{$c->id}}" valign="middle">
                      <td>{{$c->product_name}} <input type='hidden' name='qty_product_id[]' value='{{$c->id}}'></td>
                      @php
                        $qty = 0;
                        if(count($qty_product_outlet) > 0){
                          foreach($qty_product_outlet as $qpo){
                            if($qpo->id == $c->id){
                                $qty = $qpo->outlet_stock_total;
                            }
                          }
                        }
                      @endphp
                      <td>{{$qty}} <input type='hidden' name='qty_product_qty[]' value='{{$c->id}}'></td>
                      <td> <input class='form-control adjust' type='number' name='qty_product_adjust[]' value=''></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>  

               <div class="col-md-12 actions">
                    {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}   
                </div>
          </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      // $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });

       // var price = $('.price');
       //  if(price.text() != '-'){
       //      console.log(price.text());
       //      price.text($.formatRupiah(price.text()));
       //  }

       //  var global_buying_price_average = $('.global_buying_price_average');
       //  // console.log($.formatRupiah(global_buying_price_average.val()));
       //  global_buying_price_average.val($.formatRupiah(global_buying_price_average.val()));
    });

    $( ".buying_price" ).blur(function() {  
        // alert('test');
        //number-format the user input
        var val = $(this).val();
        var val2 = parseFloat(val.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val(val2);             
    });

    $( ".adjust" ).blur(function() {
        var val = $(this).val();
        console.log(val);
        if(val < 0){
          $(this).val(0);
        }
    });  
  </script>
@endpush
