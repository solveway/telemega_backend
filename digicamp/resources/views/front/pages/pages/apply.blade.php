@extends($view_path.'.layouts.master')
@section('content')
<div class="row ap_con">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row cus_con">
	  <div class="col-md-8 col-sm-8 col-xs-12 ap_pad_l">
	  	<div class="row">
	   	  <div class="col-md-12 col-sm-12 col-xs-12">
	   	    <div class="row">
	   	  	  <img src="{{ asset('components/admin/image/banner') }}/{{ $content->image }}" class="img-responsive img_center" />
	   	  	</div>
	   	  </div>

	   	  <div class="col-md-12 col-sm-12 col-xs-12 ap_des">
	   	  	{!! $content->description !!}
	   	  </div>
	  	</div>
	  </div>

	  <div class="col-md-4 col-sm-4 col-xs-12">
	  	<div class="row">
	  	  <div class="col-md-12 col-sm-12 col-xs-12 ap_ti_r">
	  	  	<h3>Apply now</h3>
	  	  </div>

	  	  <form action="apply" method="POST">
		  	{{ csrf_field() }}

		  	  <div class="col-md-12 col-sm-12 col-xs-12 ap_con_r">
		  	  	<div class="row">
		  	  	
		  	  	  @if(session()->has('message'))
				    <div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="alert alert-success alert-dismissable">
					     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					        {{ session()->get('message') }}
					    </div>
				    </div>
				  @endif

		  	  	  @if (count($errors) > 0)
				  	<div class="col-md-12 col-sm-12 col-xs-12">
					  <div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						        
						<ul>
						  @foreach ($errors->all() as $error)
						    <li>{{ $error }}</li>
						  @endforeach
						</ul>
					  </div>
				  	</div>
				  @endif


		  	  	  <div class="col-md-12 col-sm-12 col-xs-12 ap_input_con">
		  	  	  	<div class="row">
			  	  	  <div class="col-md-5 col-sm-5 col-xs-12 ap_label_r">
			  	  	  	<label>Nama Sesuai KTP</label>
			  	  	  	<label>(Tanpa Singkatan)</label>
			  	  	  </div>

			  	  	  <div class="col-md-7 col-sm-7 col-xs-12">
			  	  	  	<input type="text" class="form-control ap_cus_input" name="ap_name" required>		  	  	  
			  	  	  </div>	
		  	  	  	</div>
		  	  	  </div>

		  	  	  <div class="col-md-12 col-sm-12 col-xs-12 ap_input_con">
		  	  	  	<div class="row">
			  	  	  <div class="col-md-5 col-sm-5 col-xs-12 ap_label_r">
			  	  	  	<label>Nomor Telepon</label>
			  	  	  </div>

			  	  	  <div class="col-md-7 col-sm-7 col-xs-12">
			  	  	  	<input type="text" class="form-control ap_cus_input" name="ap_telp" required>		  	  	  
			  	  	  </div>	
		  	  	  	</div>
		  	  	  </div>

		  	  	  <div class="col-md-12 col-sm-12 col-xs-12 ap_input_con">
		  	  	  	<div class="row">
			  	  	  <div class="col-md-5 col-sm-5 col-xs-12 ap_label_r">
			  	  	  	<label>Alamat</label>
			  	  	  </div>

			  	  	  <div class="col-md-7 col-sm-7 col-xs-12">
			  	  	  	<textarea class="form-control" rows="3" name="ap_address" required></textarea>	  	  	  
			  	  	  </div>	
		  	  	  	</div>
		  	  	  </div>

		  	  	   <div class="col-md-12 col-sm-12 col-xs-12 ap_input_con">
		  	  	  	<div class="row">
			  	  	  <div class="col-md-5 col-sm-5 col-xs-12 ap_label_r">
			  	  	  	<label>Jenis Kartu</label>
			  	  	  	<label>yang didaftarkan</label>
			  	  	  </div>

			  	  	  <div class="col-md-7 col-sm-7 col-xs-12 ap_select_r">
			  	  	  	<select class="form-control" name="ap_card" required>
						  <option readonly>-- Select Your Card --</option>
						  @foreach($card as $cr)
						    <option value="{{ $cr->id }}">{{ $cr->card_category_name }}</option>
						  @endforeach
						</select>	  
			  	  	  </div>	
		  	  	  	</div>

		  	  	  	<div class="col-md-12 col-sm-12 col-xs-12 ap_des_r">
		  	  	  	  <div class="row">
		  	  	  	  	<p>Dengan ini Saya menyatakan setuju untuk memberikan informasi di atas kepada Card (Wahana Indonesia) dan setuju bahwa :</p><br>
						<p>Informasi Saya sebagaimana tersebut di atas dapat dipergunakan untuk keperluan pengajuan Kartu di Card Wahana).</p><br>
						<p>Saya telah membaca, mengerti dan memahami manfaat, biaya, dan resiko yang tercantum dalam penjelasan produk termasuk syarat dan ketentuan yang berlaku.</p>
		  	  	  	  </div>
		  	  	  	</div>

		  	  	  	<div class="col-md-12 col-sm-12 col-xs-12 ap_submit_con">
		  	  	  	  <div class="row row-eq-height">
		  	  	  	  	<div class="col-md-6 col-sm-6 col-xs-12">
		  	  	  	  	  <button type="submit" class="btn ap_submit_btn">SUBMIT</button>	
		  	  	  	  	</div>

		  	  	  	  	<div class="col-md-6 col-sm-6 col-xs-12">
		  	  	  	  	  <img src="{{ asset('components/front/images/other/lock.png') }}" class="img-responsive img_lock" />
		  	  	  	  	</div>
		  	  	  	  </div>
		  	  	  	</div>
		  	  	  </div>
		  	  	</div>
		  	  </div>
	  	  </form>
	  	</div>
	  </div>
    </div>
  </div>
</div>
@endsection