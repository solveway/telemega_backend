<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\User;
use digipos\models\Inbox;
// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use File;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class InboxController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Inbox';
		$this->root_link		= 'manage-inbox';
		$this->bulk_action		= true;
		$this->bulk_action_data = [3];
		$this->model			= new Inbox;
		$this->image_path 			= 'components/both/images/user/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
	}

	public function index(){
		// dd($this->get_user_sales());
		$this->field = [
			[
				'name' => 'inbox_name',
				'label' => 'Inbox Name',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'sales_id',
				'label' => 'Sales',
				'sorting' => 'y',
				'search' => 'text',
				'search_data' => $this->get_user_sales(),
				'belongto' => ['method' => 'user','field' => 'name']
			],
			[
				'name' => 'created_at',
				'label' => 'Created At',
				'sorting' => 'y',
				// 'search' => 'text'
			],
			[
				'name' => 'read',
				'label' => 'Read',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];
		return $this->build('index');
	}

	public function show($id){
		$this->data['data'] 		= $this->model->find($id);
		$this->data["title"] 		= 'View '.$this->title.' '.$this->data['data']->inbox_name;
		$this->data["user"] 		= User::where('id', $this->data['data']->sales_id)->first();
		Inbox::where('id', $this->data['data']->id)->update(['read' => 'y']);
		return $this->render_view('pages.inbox.view');
	}

	// public function detail($sender, $id){
	// 	if($sender == 1){
	// 		$query = $this->model->join('customer', 'inbox.user_id', 'customer.id')->where([['inbox.id', $id], ['inbox.sender', $sender]]);
	// 	}else{
	// 		$query = $this->model->join('mitra', 'inbox.user_id', 'mitra.id')->where([['inbox.id', $id], ['inbox.sender', $sender]]);
	// 	}

	// 	$this->data["title"] = "Detail Inbox";
	// 	$this->data['inbox'] = $query->first();
	// 	return $this->render_view('pages.inbox.detail');
	// }

	public function get_user_sales(){
		$q = $this->build_array(User::where('user_access_id',3)->get(),'id','name');
		return $q;
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
?>