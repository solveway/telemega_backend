<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Store;
use digipos\models\Useraccess;
use digipos\models\Slideshow;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Illuminate\Http\Request;

class SlideshowController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Slideshow Mobile';
		$this->root_link		= 'slide-mobile';
		$this->bulk_action_data = [2];
		$this->model			= new Slideshow;
		$this->bulk_action		= true;
		$this->image_path 		= 'components/both/images/slideshow/';
		$this->data['image_path'] = $this->image_path;
		// $this->hide_edit_button	= true;
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'slideshow_name',
				'label' 	=> 'Slideshow Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'image',
				'label' 	=> 'Image',
				'type' 		=> 'image',
				'file_opt' 	=> ['path' => $this->image_path]
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model;
		// dd($this->model->get());
		// dd($this->model->get());
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'slideshow_name',
				'label' => 'Slideshow Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
				'form_class' => 'col-md-12',
			],
			// [
			// 	'name' => 'description',
			// 	'label' => 'Description',
			// 	'type' => 'textarea',
			// 	'attribute' => 'required',
			// 	'validation' => 'required',
			// 	'tab' => 'general',
			// 	'form_class' => 'col-md-12',
			// ],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-6',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
			]
		];
		return $field;
	}

	public function field_edit($id){
		// $data = $this->model->find($id);	

		$field = [
			[
				'name' => 'slideshow_name',
				'label' => 'Slideshow Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
				'form_class' => 'col-md-12',
			],
			// [
			// 	'name' => 'description',
			// 	'label' => 'Description',
			// 	'type' => 'textarea',
			// 	'attribute' => 'required',
			// 	'validation' => 'required',
			// 	'tab' => 'general',
			// 	'form_class' => 'col-md-12',
			// 	// 'class' => 'editor',
			// ],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-12',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
			],
		];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'slideshow_name' => 'required|unique:slideshow,slideshow_name',
		]);
		
		$this->model->slideshow_name	= $request->slideshow_name;
		// $this->model->description		= $request->description;
		
		$this->model->status 			= 'y';
		$this->model->upd_by 			= auth()->guard($this->guard)->user()->id;

		if($request->input('remove-single-image-images') == 'y'){
			File::delete($this->image_path.$this->model->image);
			$this->model->images = '';
		}

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}
		
		$this->model->save();

		Alert::success('Successfully create new Slideshow');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_edit($id);
		return $this->build('view');
	}

	public function edit($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_edit($id);
		return $this->build('edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'slideshow_name' => 'required|unique:slideshow,slideshow_name,'.$id,
		]);
		
		$this->model 					= $this->model->find($id);
		$this->model->slideshow_name 	= $request->slideshow_name;
		
		$this->model->status 			= 'y';
		$this->model->upd_by 			= auth()->guard($this->guard)->user()->id;

		if($request->input('remove-single-image-image') == 'y'){
			if($this->model->image != NULL){
				File::delete($this->image_path.$this->model->image);
				$this->model->image = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($paSSHth.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}


		// $this->model->updated_at = date("Y-m-d H:i:s");
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully edit Banner');
		return redirect()->to($this->data['path']);
	}									

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		$uc->delete();
		Alert::success('Merchant has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		$store = $this->myStore();

		$list_member_id = Member::whereIn('store_id', $store)->groupBy('user_id')->pluck('user_id')->toArray();
		return $this->build_export($list_member_id);
	}
}
?>