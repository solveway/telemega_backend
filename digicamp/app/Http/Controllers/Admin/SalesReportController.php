<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use Carbon\Carbon;

use digipos\models\Outlet;
use digipos\models\Product_adjustment;
use digipos\models\Product;
use digipos\models\Orderhd;
use digipos\models\Orderdt;
use digipos\models\Customer;
use digipos\models\Subcategory_product_id;
use digipos\models\User;
use digipos\models\Promo;

use digipos\Libraries\Alert;
use digipos\Libraries\Timeelapsed;
use Illuminate\Http\Request;

class SalesReportController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Sales Report";
		$this->root_url			= "report/sales-report";
		$this->root_link 		= "sales-report";
		$this->model 			= new Orderhd;
		$this->outlet 			= new Outlet;
		$this->orderdt 			= new Orderdt;
		$this->user 			= new User;
		$this->promo 			= new Promo;
		$this->data['root_url']	= $this->root_url;

		$this->payment_type = [
			'1' => 'Cash',
			'2' => 'Transfer',
			'3' => 'TOP',
		];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->data['title'] 			= $this->title;
		// $this->data['data1'] 			= $this->model->get();
		$this->data['payment_type'] 	= $this->payment_type;
		$outlet_access 					= $this->myStore2();
		if($outlet_access != 'all'){
			$office 					= Outlet::where('id', $outlet_access)->get();
		}else{
			$office 					= Outlet::get();
		}
		$this->data['office'] 			= $office;
		$this->data['outlet_access'] 	= $outlet_access;
		// dd($this->data['office']);
		return $this->render_view('pages.reports.sales');
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function filter($request){
		// $outlet_access = $this->myStore2();

		$query = $this->model->leftJoin('user', 'user.id', 'orderhd.user_id')->leftJoin('outlet', 'outlet.id', 'orderhd.outlet_id')->leftJoin('promo', 'promo.id', 'orderhd.promo_id');
		
		// return $request->input('outlet');
		if($request->input('outlet') != ''){
			if(in_array('0', $request->input('outlet'))){
				
			}else{
				$query->whereIn('orderhd.outlet_id', $request->input('outlet'));
			}
		}else{
			return '<div style="text-align:center;color:red;">Please select Office !</div>';
		}
		
		$query2 = Orderdt::Join('product', 'product.id', 'orderdt.product_id')->leftJoin('subcategory_product', 'subcategory_product.id', 'orderdt.subcategory_product_id')->orderBy('orderhd_id')->select('orderdt.*', 'subcategory_product.subcategory_product_name', 'product.product_name');
		// $product 	= $this->product->where('status', 'y');
		// $outlet 	= $this->outlet->where('status', 'y')->get();

		if($request->input('search_date_from') != ""){
			// dd($request->input('search_date_from'));
			$search_date_from = date('d-m-Y', strtotime($request->input('search_date_from') . ' -1 day'));
			$search_from = $this->displayToSql($search_date_from);
			// dd($search_from);
			$query->whereDate('orderhd.created_at', '>=', $search_from);
		}

		if($request->input('search_date_to') != ""){
			$search_to = $this->displayToSql($request->input('search_date_to'));
			$query->whereDate('orderhd.created_at', '<=', $search_to);
		}

		$count_product = 0;
		// return $request->input('payment_type');
		if($request->input('payment_type') != ""){
			if(in_array('0', $request->input('payment_type'))){
				// $product2= $product->get();
				// $count_product = count($this->product->get()) * 3;
			}else{
				// $count_product = count($request->input('payment_type')) * 3;
				$query->whereIn('orderhd.payment_type', $request->input('payment_type'));
				// $product2 = $product->whereIn('product.id', $request->input('payment_type'))->get();
			}
		}else{
			return '<div style="text-align:center;color:red;">Please select Payment Type !</div>';
		}

		$display = "";
		$get_last = "";
		
		// $query2 = $query->select('product_adjustment.*', 'product.product_name as product_name', 'outlet.outlet_name as outlet_name')->get();
		// $query2 = $query->select('product_adjustment.*', 'product.product_name as product_name', 'outlet.outlet_name as outlet_name')->pluck('created_at', 'id')->toArray();

		// dd($query2);

		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Product Name</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Barcode</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Total Item</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Type</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Price (Rp)</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Subtotal (Rp)</th>
		                </tr>
		              </thead>
		              <tbody>';
		$query->select('orderhd.*', 'user.name as user_name', 'outlet.outlet_name', 'promo.discount_type', 'promo.discount_value')->orderBy('orderhd.id'); 
		// return dd($query2->get());
		foreach($query->get() as $odr){
			$display .= '<tr>
							<td colspan="6" class="bg-dark font-white">Office: '.$odr->outlet_name.' - Order Id: '.$odr->order_id.' - sales: '.$odr->user_name.'<br>'.date('d - F - Y', strtotime($odr->order_date)).' - cust: '.$odr->name.' - Payment Type: '.$odr->payment_type.'</td>
						</tr>';
			$curr_orderhd_id = $odr->id;
			$curr_subtotal = 0;
			$flagfind = 0;
			foreach($query2->get() as $odc){
				if($curr_orderhd_id == $odc->orderhd_id){
					$flagfind 		= 1;
					$subtotal 		= $odc->price;
					$curr_subtotal += $subtotal;
					$total_item   	= 1;
					if($odc->type == 'dompul'){
						$total_item 	= $odc->total_item;
						$subtotal 		= $odc->price * $odc->total_item;
					}
					$display .= '<tr>
									<td>'.$odc->name.'</td>
									<td>'.$odc->barcode.'</td>
									<td>'.$total_item.'</td>
									<td>'.$odc->type.'</td>	
									<td>'. number_format($odc->price).'</td>
									<td>'. number_format($subtotal).'</td>
								</tr>';
				}else{
					if($flagfind == 1){
						break;
					}
				}
			}

			$display .= '<tr><td colspan="5" style="text-align: right;">Subtotal (Rp.)</td><td>'.number_format($odr->subtotal).'</td></tr>
						<tr><td colspan="5" style="text-align: right;">Discount (Rp.)</td><td>'.number_format($odr->discount).'</td></tr>
						<tr><td colspan="5" style="text-align: right;">Total (Rp.)</td><td>'.number_format($odr->total_price)	.'</td></tr>';
		}

		$display .='</tbody>
					</table>
					</div>';

		return $display;
	}
}
