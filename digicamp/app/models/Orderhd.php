<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Orderhd extends Model{
    protected $table        = 'orderhd';
    protected $orderdt      = 'digipos\models\Orderdt';
    protected $customer     = 'digipos\models\Customer';

    public function orderdt(){
        return $this->hasMany($this->orderdt,'orderhd_id');
    }

    // public function customer(){
    //     return $this->hasMany($this->customer,'orderhd_id');
    // }
}
