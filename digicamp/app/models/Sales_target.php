<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Sales_target extends Model{
	protected $table 		= 'sales_target';
	protected $user     	= 'digipos\models\User';

    public function user(){
        return $this->belongsTo($this->user,'user_id');
    }
}
