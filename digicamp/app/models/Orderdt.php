<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Orderdt extends Model{
    protected $table        			= 'orderdt';
    protected $orderhd      			= 'digipos\models\Orderhd';
    protected $product     				= 'digipos\models\Product';
    protected $subcategory_product     	= 'digipos\models\Subcategory_product';

    public function orderhd(){
        return $this->belongsTo($this->orderhd,'orderdt_id');
    }

    public function product(){
        return $this->belongsTo($this->product,'product_id');
    }

    public function subcategory_product(){
        return $this->belongsTo($this->subcategory_product,'subcategory_product_id');
    }
}
